#!/bin/bash
export t=0
while [ $# -gt 0 ]
do
  f=`sed 's,^.*src/,,' <<< "${1}"`
  echo "${1} => ${f}"
  until sqlite3 bots.db "UPDATE log_prio SET prio = -( abs(prio) + 9 ) WHERE file = '${f}';"
  do
    sleep $[++t]
  done
  min=9
  for lev in trace debug info warn error
  do
    lines=` grep -n "${lev}!" "${1}" | cut -d ':' -f 1 | xargs echo | tr ' ' ',' `
    sqlite3 bots.db "SELECT * FROM log_prio WHERE file = '${f}' AND line in (${lines}) AND ABS(prio) < ${min};"
    sqlite3 bots.db "UPDATE log_prio SET prio = -( abs(prio) + 1 ) WHERE file = '${f}' AND line in (${lines}) AND ABS(prio) < ${min};"
    export min=`sqlite3 bots.db "SELECT MAX(ABS(prio)) FROM log_prio WHERE file = '${f}' AND line in (${lines});"`
    echo $[++min]
    export min
    echo "Highest priority for ${lev} is ${min}"
    if grep -n "${lev}\!" "${1}" && read -p 'Line for extra boost: ' lin && grep '[1-9]' <<< "${lin}"
    then
      sqlite3 bots.db "INSERT INTO log_prio (file, line, prio) VALUES ('${f}', ${lin}, 3210);" 2>/dev/null
      echo sqlite3 bots.db "UPDATE log_prio SET prio = -( abs(prio) + 99 ) WHERE file = '${f}' AND line = ${lin};"
      until sqlite3 bots.db "UPDATE log_prio SET prio = -( abs(prio) + 99 ) WHERE file = '${f}' AND line = ${lin};"
      do
        sleep $[++t]
      done
    fi
  done
  shift
done
