#!/bin/bash -x
for d in *.dot
do
    if dot -Tsvg "${d}" > "${d}.svg"
    then
        ( xdg-open "${d}.svg" >&- 2>&- & ) >&- 2>&- &
    else
        rm -v "${d}.svg"
        break
    fi
done
