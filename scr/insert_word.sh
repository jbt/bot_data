#!/bin/bash 
if [ $# -lt 2 ]
then
    echo usage $0 table value...
    exit 1
fi
if [ "$1" = words ]
then
  echo common words
elif [ "$1" = permatag ]
then
  echo permatagging
else
  echo are you sure you want to insert into "$1" '?'
  exit 2
fi
table=$1
shift
if [ "$1" = 'lower' ]
then
  export lower=true
  shift
else
  export lower=false
fi
ins() {
    export w="${1}"
	echo "${w}"
    if [ "${table}" = words ]
    then
	    sqlite3 bots.db "SELECT * FROM permatag WHERE word = '${w}';"
	    sqlite3 bots.db "DELETE   FROM permatag WHERE word = '${w}';"
    fi
    until [ ${e} -eq 19 ]
    do
        q="INSERT INTO ${table} (word) VALUES ('${w}');"
    	sqlite3 bots.db "${q}"
        export e=$?
    	echo "${e} -> ${q}"
        sleep 1
    done
    sleep 1
}
for v in "${@}"
do
    export e=0
    if ${lower}
    then
      ins "${v}"
    fi
    w=`tr '[:lower:]' '[:upper:]' <<< "${v}"`
    ins "${w}"
done
set -x
if [ "${table}" = permatag ]
then
  sqlite3 bots.db "DELETE FROM words WHERE EXISTS (SELECT 1 FROM permatag WHERE permatag.word = words.word);"
elif [ "${table}" = words ]
then
  until sqlite3 bots.db "DELETE FROM tags WHERE EXISTS (SELECT 1 FROM words WHERE word = tag);"
  do
	  sleep 1
  done
fi
