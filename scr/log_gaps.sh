#!/bin/bash

grep -v tick.tock log | grep '^[0-9][0-9]:[0-9][0-9]:[0-9][0-9] ' | sort | while read ts msg
do
    h=`cut -d : -f 1 <<< "${ts}" | sed 's,^0*,,'`
    m=`cut -d : -f 2 <<< "${ts}" | sed 's,^0*,,'`
    s=`cut -d : -f 3 <<< "${ts}" | sed 's,^0*,,'`
    if [ "$m" = '' ] || [ "$s" = '' ] || [ "$h" = '' ]
    then
        echo "MALFORMED:h='${h}' m='${m}' s='${s}' ts=${ts} msg=${msg}" >&2
        continue
    fi
    t=$[ $h * 60 + m ]
    t=$[ $t * 60 + s ]
    if [ "${t0}" != '' ] && [ ${t0} -gt 0 ]
    then
        d=$[ $t - ${t0} ]
        echo "${d} ${msg0} ...->... ${msg} @ ${ts0}->${ts}"
    fi
    export t0="${t}"
    export ts0="${ts}"
    export msg0="${msg}"
done | sort -n
