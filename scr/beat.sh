#!/bin/bash -e

d=`mktemp -d`
sql() {
    if [ "$1" = '-v' ]
    then
        shift
        echo "SQL: ${*} ; " >&2 
    fi
    sqlite3 bots.db " ${*} ; "
}
bot2tag() {
    while read w
    do
        sql SELECT tag FROM tags WHERE slug = "'${w}'" >> "${d}/${2}"
    done < "${d}/${1}"
}
query() {
    if ${3}
    then
        winner=slug
    else
        winner=opp
    fi
    cat > "${d}/q.sql" <<EOF
SELECT DISTINCT
        ( select count(1) from tags a where a.tag = winner.tag ) + ifnull(weights.weight,0) as n, 
        thirdparty.tag 
    FROM 
                matches loserhist
        INNER JOIN 
                tags thirdparty
            ON 
                thirdparty.slug = loserhist.opp 
        INNER JOIN 
                tags winner
            ON 
                    winner.tag = thirdparty.tag
                AND 
                    thirdparty.slug != winner.slug
        LEFT JOIN
                tag_weights weights
            ON
                winner.tag = weights.tag
    WHERE 
            (
                    weights.weight IS NULL
                OR
                    (
                            weights.weight > 0
                        AND 
                            weights.weight < 999
                    )
            )
        AND
            winner.slug = '${1}'
        AND 
            loserhist.slug = '${2}'
        AND 
            loserhist.winner = loserhist.${winner}
EOF
    #cat "${d}/q.sql" >&2 
    sqlite3 bots.db <"${d}/q.sql"  | sed 's,|, ,'
}

sql ' DELETE FROM tags WHERE EXISTS (SELECT 1 FROM words WHERE word = tag) '

# reduce tags new winner has in common with new loser's defeated foes
query ${1} ${2} true  | sort -nr > "${d}/dn"
# sql -v " ${prolog} me.slug = '${1}' AND m.slug = '${2}' AND m.winner = m.opp  " | sort -nr  > "${d}/dn"

# bump   tags new winner has in common with new loser's former losses
query ${1} ${2} false | sort -n  > "${d}/up"
# sql -v " ${prolog} me.slug = '${1}' AND m.slug = '${2}' AND m.winner = m.slug " | sort -n  > "${d}/up"

# bump tags common with loser and winner's former conquests
query ${2} ${1} true  | sort -n  > "${d}/up"
# sql -v " ${prolog} me.slug = '${2}' AND m.slug = '${1}' AND m.winner = m.slug " | sort -n  >> "${d}/up"

# drop tags common with loser and winner's former losses
query ${2} ${1} false | sort -nr > "${d}/dn"
# sql -v " ${prolog} me.slug = '${2}' AND m.slug = '${1}' AND m.winner = m.opp  " | sort -nr >> "${d}/dn"

set +x
wc "${d}/"* | sort -n
dns=0
while read n t
do
    if grep -q "${t}" "${d}/up"
    then
        true
    elif sql UPDATE tag_weights SET weight = 'MAX( weight - 1, 0 )' WHERE tag = "'${t}'" AND 'weight > 0'
    then
        echo "Lowered tag ${t}" >&2
        [ $((dns++)) -gt 8 ] && break
    elif sql "INSERT INTO tag_weights (tag, weight) VALUES ('${t}', 0)" 2>/dev/null 
    then
        echo "Inserted tag ${t} @ 0." >&2
        break
    else
        sleep 1
    fi
done < "${d}/dn"
ups=0
while read n t
do
    if grep -q "${t}" "${d}/dn"
    then
        true
    elif sql UPDATE tag_weights SET weight = 'MAX( weight + 1, 99 )' WHERE tag = "'${t}'" 
    then
        echo "Bumped tag ${t}" >&2
        [ $((ups++)) -gt 9 ] && break
    elif sql "INSERT INTO tag_weights (tag, weight) VALUES ('${t}', 9)" 2>/dev/null 
    then
        echo "Inserted tag ${t} @ 9." >&2
        break
    else
        sleep 1
    fi
done < "${d}/up"
sql " UPDATE tag_weights SET weight = 0 WHERE weight < 0 ; "
ls -lrth "${d}/"*
