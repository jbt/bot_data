#!/bin/bash
m=`stat --format=%Y "${0}"`
rm -v /tmp/*.running
rm -v log
export running=/tmp/$$.running
export RUST_BACKTRACE=full
date > "${running}"
(
    set -e
    while [ -d /proc/${PPID}/ ] && [ -f "${running}" ]
    do
        mod=$[ `date +%s` % 120 ]
        case ${mod} in
            29)
                sleep .1
                ;;
            0)
                echo `date '+%H:%M:%S'` tick tock
                sleep 120
                ;;
            *)
                sleep $[ 120 - ${mod} ]
                ;;
        esac
    done  | tee -a log
) &
if ! grep '[1-9]' ~/bot_data.runtime
then
    echo -n 1 > ~/bot_data.runtime
fi
t=9
until [ $[ `stat --format=%Y "${0}"` ] -ne ${m} ] || read -N 1 -t $[++t]
do
    export t=`cat ~/bot_data.runtime`
    echo "Pulling $t"
    timeout "${t}" git pull || sleep $[++t]
    echo "Building $t" | tee -a log
    ( cargo build --bins -Z unstable-options --out-dir=target/debug 2>&1 ) 2>&1 >> log || continue
    if ( fuser --verbose bots.db 2>&1 ) 2>&1 >> log
    then
        echo -e " \n \t Waiting for other procs to release the DB. $[++t]" | tee -a log
        for p in `fuser bots.db`
        do
            if [ -d /proc/${p}/ ]
            then
                pstree --arguments --show-parents --show-pids --highlight-pid ${p} | grep -vE 'pstree|grep' | grep -C 9 --color=always "${p}" >> log
                sleep 9 
            fi
        done
        sleep 9 
        echo -n $[ t + 1 ] > ~/bot_data.runtime
        continue
    fi
    echo "Starting up $t" | tee -a log
    ( timeout $[ t * 60 ] ./target/debug/bot_data 2>&1 ) 2>&1 >> log
    e=$?
    echo "Done e=$e $t" | tee -a log
    if [ $e -eq 124 ]
    then
        echo "May have timed out. Relaxing timeout to " $[t + 9] " min" | tee -a log
        echo -n $[ t + 9 ] > ~/bot_data.runtime
    elif [ $e -eq 0 ] && [ $t -gt 1 ]
    then
        echo -n $[ t - 1 ] > ~/bot_data.runtime
    fi
    for i in {9..0}
    do
      mv -v prio.${i}.txt prio.$[i + 1].txt
    done
    mv -v prio.txt prio.0.txt
    sqlite3 bots.db 'select * from log_prio order by file, line;' > prio.txt
done
rm -v /tmp/*.running
if [ $[ `stat --format=%Y "${0}"` ] -ne ${m} ]
then
    echo "Recursing " `date` | tee -a log
    "${0}" "${@}"
    exit
fi
