#!/bin/bash -ex
cd `dirname "${0}"`/..
echo 'pub(crate) const INIT : &[&str] = &[' > src/db/ddl.rs
sqlite3 bots.db .schema | sed -e 's,",\\",g' -e 's,^,    ",' -e 's/$/",/' >> src/db/ddl.rs
echo '];' >> src/db/ddl.rs
echo >> src/db/ddl.rs
git add src/db/ddl.rs

