#!/bin/bash -e
tf=`mktemp`
abot="${1}"
shift
while [ $# -gt 0 ]
do
    bbot="${1}"
    shift
    for w in {0..999}
    do
        echo -n "${abot} & ${bbot}: Weights > ${w} "
    #     if ! sqlite3 bots.db "select a.tag from tags a inner join tags b on b.tag = a.tag and a.slug = '${1}' and b.slug = '${2}' inner join tag_weights w where w.tag = a.tag and w.weight > ${w};" > "${tf}"
        if ! sqlite3 bots.db "select (select count(1) from tags c where c.tag = a.tag) as n, a.tag from tags a inner join tags b on b.tag = a.tag and a.slug = '${abot}' and b.slug = '${bbot}' inner join tag_weights w where w.tag = a.tag and w.weight > ${w} order by n desc;" > "${tf}.pre"
        then
            sleep ${w}
            continue
        fi
        sed 's,|, ,' "${tf}.pre" > "${tf}"
#         wc "${tf}"
        if [ ! -s "${tf}" ]
        then
            break 
        fi
        while read n tag
        do
            if sqlite3 bots.db "UPDATE tag_weights SET weight = weight - 1 WHERE tag = '${tag}';"
            then
                echo "n=${n} tag='${tag}'"
                break
            else
                sleep 1
            fi
        done < "${tf}"
    done
done
