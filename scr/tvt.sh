#!/bin/bash -e
if [ $# -lt 2 ]
then
    echo "Usage tag tag"
    exit 1
fi
a="${1}"
shift
b="${*}"

examples() {
    echo -n "There are a total of "
    sqlite3 bots.db "SELECT COUNT(1) FROM tags a WHERE a.tag = '${1}' AND NOT EXISTS (SELECT 1 FROM tags b WHERE a.slug = b.slug AND b.tag = '${2}');"
    echo " ... bots which are tagged ${1} but not ${2}. Some examples:"
    sqlite3 bots.db "SELECT a.slug FROM tags a WHERE a.tag = '${1}' AND NOT EXISTS (SELECT 1 FROM tags b WHERE a.slug = b.slug AND b.tag = '${2}');" | sort -R | head | sort
    echo
}
examples "${a}" "${b}"
examples "${b}" "${a}"
wins() {
    echo "How many times did ${1} beat ${2} ? "
    sqlite3 bots.db "SELECT COUNT(1) FROM tags a INNER JOIN matches m ON a.slug = m.winner INNER JOIN tags b ON m.opp = b.slug WHERE a.tag = '${1}' AND b.tag = '${2}' AND NOT EXISTS (SELECT 1 FROM tags aa WHERE aa.slug = a.slug AND aa.tag = '${2}') AND NOT EXISTS (SELECT 1 FROM tags bb WHERE bb.slug = b.slug AND bb.tag = '${1}');"
    echo 'Some examples of that:'
    sqlite3 bots.db "SELECT m.* FROM tags a INNER JOIN matches m ON a.slug = m.winner INNER JOIN tags b ON m.opp = b.slug WHERE a.tag = '${1}' AND b.tag = '${2}' AND NOT EXISTS (SELECT 1 FROM tags aa WHERE aa.slug = a.slug AND aa.tag = '${2}') AND NOT EXISTS (SELECT 1 FROM tags bb WHERE bb.slug = b.slug AND bb.tag = '${1}');" | sort -R | head | sort
    echo
}
wins "${a}" "${b}"
wins "${b}" "${a}"
