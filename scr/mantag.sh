#!/bin/bash 
t="${1}"
shift
sql() {
    while sleep .1
    do
        sqlite3 bots.db "${*}"
        case $? in
            0)
                return
                ;;
            5)
                sleep 2
                ;;
            19)
                return 19
                ;;
            *)
                echo "Bad SQL error." >&2
                exit 1
        esac
    done    
}
sql "INSERT INTO permatag (word) VALUES ( '${t}' );"
sql "INSERT INTO tag_weights (tag, weight) VALUES ( '${t}', 9 );"
while [ $# -gt 0 ]
do
    echo "Tagging ${1} with ${t}."
    sql "INSERT INTO tags (tag, slug) VALUES ( '${t}', '${1}' );"
    shift
done
