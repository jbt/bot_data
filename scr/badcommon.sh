#!/bin/bash 
if [ -f "${tmptxt}" ]
then
    truncate --size=0 "${tmptxt}"
else
    export tmptxt=`mktemp`
fi
against="${1}"
shift
while [ $# -gt 1 ]
do
    "${0}" "${against}" "${1}"
    shift
done
while fuser bots.db
do
  sleep $[++t]
done
echo "De-similarizing ${against} and ${1}"
sqlite3 bots.db "select a.tag, count(1) n from tags a inner join tags b on a.tag = b.tag inner join tags c on a.tag = c.tag where b.slug = '${against}' and c.slug = '${1}' and not a.tag like '%:%' and not exists (select 1 from permatag w where w.word = a.tag) group by a.tag order by n desc limit 9" | while read -d '|' t
do
    read n
    echo "There are ${n} bots tagged with '${t}'."
    echo "${t}" >> ${tmptxt}
done
sleep 1
while fuser bots.db
do
  sleep $[++t]
done
dict() {
    while [ $# -gt 0 ]
    do
        if grep -i --max-count=1 "^${1}$" /usr/share/dict/words
        then
            sleep .1
        else
#             echo "${1} not in dict"
            return 1
        fi
        shift
    done
#     echo 'yep dict'
    return 0
}
while read tag
do
    while fuser bots.db
    do
      sleep $[++t]
    done
    if dict ${tag} && sqlite3 bots.db "insert into words (word) values ( '${tag}' );"
    then
        echo "commonified '${tag}'"
        break
    else
        echo "skip '${tag}'"
        sleep 9
    fi
done < ${tmptxt}
sleep 1
while read tag
do
    e=5
    until [ $e -eq 0 ]
    do
        while fuser bots.db
        do
          sleep $[++t]
        done
        sqlite3 bots.db "DELETE FROM tags where tag = '${tag}';" 2>&1
        e=$?
#         echo e=${e}
        sleep 1
    done
    echo "deleted ${tag}"
done < ${tmptxt} | uniq -c
