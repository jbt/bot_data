#!/bin/bash -e
cd `dirname "${0}"`/..
l=`mktemp`
t=0
until grep "${1}.beats.${2}" "${l}"
do
    if [ $(( t % 100 )) -eq 99 ]
    then
        cargo run >/dev/null 2>/dev/null || sleep $((++t))
    fi
    ./scr/beat.sh "${1}" "${2}" || sleep $((++t))
    cargo run fight "${1}" "${2}" > "${l}" 2>&1 || sleep $((++t))
    tail "${l}"
    echo `date` ${t}
    sleep $((++t))
done
