#!/bin/bash -x
if [ "$1" = 'or' ]
then
	export op=2
	shift
elif [ "$1" = 'not' ]
then
	export op=3
	shift
else
	export op=1
fi
if [ $# -ne 3 ]
then
    echo "USAGE: ${0} lhs rhs out"
    exit 1
fi
if [ "${1}" '>' "${2}" ] && [ ${op} -lt 3 ]
then
    "${0}" "${2}" "${1}" "${3}"
    exit
fi
e=9
t=0
until [ ${e} -eq 19 ]
do
    f="${e}"
    sqlite3 bots.db "INSERT INTO tag_logic (lhs,rhs,op,result) VALUES ( '${1}' , '${2}' , ${op} , '${3}' );"
    export e=$?
    if [ ${f} -eq ${e} ]
    then
        sleep $[++t]
    fi
done
