#![feature(local_key_cell_methods)]

mod consts;
mod db;
mod ipfs;
mod mdl;
mod observation;
mod prio_log;
mod traits;
mod wiki;
mod error;
mod rel;

use crate::rel::graph::Polarity;

use itertools::Itertools;
use log::{info, trace};
use time::OffsetDateTime;

use std::{
    env,
    ops::{Div, Mul, Sub},
};

#[tokio::main]
async fn main() {
    if std::env::var("RUST_LOG").is_ok() {
        env_logger::init();
    } else {
        log::set_logger(&prio_log::LOGGER).expect("Trouble setting up logging.");
    }
    let mut worker = db::Worker::create().await;
    log::set_max_level(log::LevelFilter::Trace);
    let subcmd = env::args().skip(1).next();
    let mut exit_status = 0;
    match subcmd.as_ref().map(|s| s.as_str()) {
        None => worker.work().await,
        Some("show") => {
            for b in env::args().skip(2) {
                println!("Show {}...", &b);
                let html = {
                    let mut stmt = worker
                        .conn
                        .prepare("SELECT html FROM wiki_pages WHERE slug = ?1")
                        .unwrap();
                    match stmt.query_row([b.clone()], |row| row.get::<&str, String>("html")) {
                        Ok(h) => h,
                        Err(e) => {
                            println!("Failed to query html for {}: {}", &b, &e);
                            return;
                        }
                    }
                };
                if html.is_empty() {
                    println!("{} does not have html saved.", &b);
                    return;
                }
                let page = wiki::Page {
                    html,
                    slug: b.clone(),
                    typ: mdl::PageType::Bot,
                    updated: OffsetDateTime::now_utc(),
                };
                println!("{}:", &b);
                let (b, p) = wiki::bot::parse(&page, &worker.conn).unwrap();
                println!("  {:?}", &b);
                println!("  {:?}", &p);
                let comps = worker.conn.competitions().unwrap();
                let matches = wiki::matches::extract_from_page(&page, &b, &comps, &p.aliases);
                println!("Matches:");
                for m in &matches {
                    println!("\t{:?}", m);
                }
            }
        }
        Some("graph") => {
            let mut match_cache = db::matches::CachedLookup::default();
            let mut ger = get_ger(&worker, 1);
            prio_log::LOGGER.check();
            for b in env::args().skip(2) {
                for (ext, pol) in [("wins", Polarity::Strong), ("loss", Polarity::Weak)] {
                    prio_log::LOGGER.check();
                    let file = format!("{b}.{ext}.dot");
                    let mut g = rel::graph::BotGraph::new(b.clone(), pol);
                    while usize::from(g.expand(&worker.conn, &mut match_cache, &mut ger)) > g.size() / 10 {
                        let prio = g.expand(&worker.conn, &mut match_cache, &mut ger);
                        trace!("Expanding {b}'s graph... {}", prio);
                        prio_log::LOGGER.check();
                    }
                    g.draw(&file);
                    prio_log::LOGGER.check();
                }
                prio_log::LOGGER.check();
            }
            logs();
        }
        Some("save") => {
            for b in env::args().skip(2) {
                let html = {
                    let mut stmt = worker
                        .conn
                        .prepare("SELECT html FROM wiki_pages WHERE slug = ?1")
                        .unwrap();
                    match stmt.query_row([b.clone()], |row| row.get::<&str, String>("html")) {
                        Ok(h) => h,
                        Err(e) => {
                            println!("Failed to query html for {}: {}", &b, &e);
                            return;
                        }
                    }
                };
                if html.is_empty() {
                    println!("{} does not have html saved.", &b);
                    return;
                }
                let page = wiki::Page {
                    html,
                    slug: b.clone(),
                    typ: mdl::PageType::Bot,
                    updated: OffsetDateTime::now_utc(),
                };
                let (b, pi) = wiki::bot::parse(&page, &worker.conn).unwrap();
                db::bot::store(&b, &worker.conn);
                db::bot::record_participation(&pi, &worker.conn, &b.slug);
                println!("Saved: {:?}", &b);
            }
        }
        Some("near") => {
            let mut ger = get_ger(&worker, 3);
            for bot in env::args().skip(2) {
                logs();
                if let Some(neighbors) = ger.neighborhood(bot.clone(), &worker.conn) {
                    println!("Neighbors for {}:", &bot);
                    println!("|Similar| Name (Page) | Some shared patterns |");
                    println!("|-------|-------------|----------------------|");
                    for (score, slug, t, t0, t1, t2) in neighbors {
                        if score < 0.09 {
                            break;
                        }
                        println!("|  {:3}% | {:13}| {:?} {:?} {:?} {:?} |", (score * 100.) as i8, slug, t, t0, t1, t2);
                    }
                } else {
                    logs();
                    println!("Could not find *any* neighbors for {}", &bot);
                }
            }
        }
        Some("closer") => {
            let mut ger = get_ger(&worker, 99);
            let left = env::args().skip(2).next().expect("Usage provide bot args");
            for rite in env::args().skip(3) {
                ger.load_tags(&worker.conn, std::time::Duration::from_secs(9)).expect("db");
                let ltags = ger.get_tags4sure(&left, &worker.conn).expect("Failed to get tags on {left}");
                let rtags = ger.get_tags4sure(&rite, &worker.conn).expect("Failed to get tags on {rite}");
                for shared in ltags.intersection(&rtags) {
                    ger.increase_weight(shared, &worker.conn).expect("Database trouble");
                }
                let result = ger.tag_together(left.clone(), rite, &worker.conn);
                if result.is_empty() {
                    println!("Nothing remaining in common to tag.");
                    exit_status += 1;
                } else {
                    println!("Adding tags: {:?}", result);
                }
            }
        }
        Some("close") => {
            let mut ger = get_ger(&worker, 3);
            let left = env::args().skip(2).next().expect("Usage provide bot args");
            let rite = env::args()
                .skip(3)
                .next()
                .expect("Usage provide 2 bot args");
            let ltags = ger
                .get_tags4sure(&left, &worker.conn)
                .expect("tags of first bot not found");
            let rtags = ger
                .get_tags4sure(&rite, &worker.conn)
                .expect("tags of 2nd bot not found");
            let mut l = ltags.difference(&rtags).peekable();
            let mut r = rtags.difference(&ltags).peekable();
            let mut b = ltags.intersection(&rtags).peekable();
            println!(
                "   ### {} ### \t\t ### {} ### \t\t ### BOTH ###",
                &left, &rite
            );
            let na = " - ".to_string();
            while l.peek().is_some() || r.peek().is_some() || b.peek().is_some() {
                println!(
                    "{:33} {:33} {}",
                    l.next().unwrap_or(&na),
                    r.next().unwrap_or(&na),
                    b.next().unwrap_or(&na),
                );
            }
        }
        Some("tag_sw") => {
            let tag = env::args()
                .skip(2)
                .next()
                .expect("You need to specify a tag to check for strengths & weaknesses.");
            let mut ger = get_ger(&worker, 19);
            let matches = db::matches::get_1v1_wins(&worker.conn).expect("Unable to load matches");
            let mut result = observation::strong_weak(&tag, &mut ger, &matches, &worker.conn);
            println!("{} bots are tagged {tag}.", ger.get_bots(&tag).unwrap().len());
            println!("'{}' strengths:", &tag);
            for (tag, lo, hi, tot, samp) in result.iter().take(9) {
                println!("  {:26}:{:3}%-{:3}% ({} matches) {}", tag, lo, hi, tot, samp);
            }
            result.sort_unstable_by_key(|(tag, l, h, t, _)| (*h, *l, *t, tag.clone()));
            println!("'{}' weaknesses:", &tag);
            for (tag, lo, hi, tot, samp) in result.iter().take(9) {
                println!("  {:26}:{:3}%-{:3}% ({} matches) {}", tag, lo, hi, tot, samp);
            }
        }
        Some("dealias_opp") => {
            let result = db::matches::dealias_opp(&worker.conn);
            println!("Result: {:?}", result);
        }
        Some("team_tag") => {
            let ger = get_ger(&worker, 0);
            logs();
            let res = ger.gen_tags_from_teams(&worker.conn);
            logs();
            logs();
            println!("Result = {:?}", &res);
        }
        Some("tl") => {
            logs();
            let mut ger = get_ger(&worker, 1);
            logs();
            exit_status = 9;
            logs();
            for pat in env::args().skip(2) {
                logs();
                let done = db::tag_logic::for_pattern(&worker.conn, &mut ger, &pat).expect("Fail");
                if done > 0 {
                    info!("Applied tags to {done} bots for {pat}.");
                    println!("\n\t###\tApplied tags to {done} bots for {pat}.\t###\n");
                    exit_status = 0;
                }
                logs();
            }
            logs();
            db::tag_logic::apply(&worker.conn, &mut ger).expect("Failed to apply tags arbitrarily");
            logs();
        }
        Some("fight") => {
            let mut ger = get_ger(&worker, 1);
            logs();
            let mut match_cache = db::matches::CachedLookup::default();
            for (a, b) in env::args().skip(2).tuples() {
                match rel::fight::guess(&a, &b, &worker.conn, &mut match_cache, &mut ger) {
                    Ok(pct) => {
                        if pct > 50.01 {
                            println!("{a} beats {b} {}% of the time.", pct.mul(10.).round().div(10.));
                        } else if pct < 49.99 {
                            println!("{b} beats {a} {}% of the time.", 100.0.sub(pct).mul(10.).round().div(10.));
                        } else {
                            println!("{a} vs. {b} : Draw (dead heat)");
                        }
                    }
                    Err(e) => println!("Trouble predicting {a} vs. {b}: {:?}", e),
                }
                logs();
            }
        }
        Some("factors") => {
            let mut ger = get_ger(&worker, 1);
            let mut match_cache = db::matches::CachedLookup::default();
            if let Some((a, b)) = env::args().skip(2).tuples().next() {
                let mut result = observation::matchup_factors(&a, &b, &mut ger, &worker.conn, &mut match_cache);
                result.sort();
                result.reverse();
                println!("Factors favoring {a}:");
                for factor in result.iter().filter(|f|f.favors_a()).take(9) {
                    println!("  {:?}", factor.tags() );
                }
                println!("Factors favoring {b}:");
                for factor in result.iter().filter(|f|!f.favors_a()).take(9) {
                    println!("  {:?}", factor.tags() );
                }
            }
            logs();
        }
        Some("bot_sw") => {
            for a in env::args().skip(2) {
                match observation::bot_strengths_weaknesses(&worker.conn, &a) {
                    Ok(mut sw) => { 
                        println!("{a} weaknesses:");
                        for i in 0..20 {
                            let (n,t) = &sw[i];
                            println!("  {}% against {t}", (*n as f64) / 10.);
                        }
                        sw.reverse();
                        println!("{a} strengths:");
                        for i in 0..20 {
                            let (n,t) = &sw[i];
                            println!("  {}% against {t}", (*n as f64) / 10.);
                        }
                    },
                    Err(e) => { println!("Error analyzing {a} as a bot: {e:?}"); },
                }
            }
        }
        Some(x) => {
            println!("Argument error: {} not implemented.", x);
        }
    }
    logs();
    worker.conn.save_log_priorities();
    std::process::exit(exit_status);
}

fn logs() {
    for _ in 0..9 {
        if prio_log::LOGGER.check() {
            prio_log::LOGGER.speed_up();
        } else {
            std::thread::sleep(std::time::Duration::from_millis(1));
        }
    }
}

fn get_ger(worker: &db::Worker, stage: u8) -> db::tagger::Tagger {
    // let log = Rc::new(RefCell::new(log::Immediate::default()));
    let mut ger = db::tagger::Tagger::new();
    if stage > 0 {
        ger.load_bots(&worker.conn).expect("Failed to load bots");
    }
    if stage > 2 {
        ger.load_permatag(&worker.conn)
            .expect("failed to load permatag table");
        ger.load_common_words(&worker.conn)
            .expect("failed to load words table");
    }
    let mut delay: u64 = stage.into();
    delay *= stage as u64;
    ger.load_tags(&worker.conn, std::time::Duration::from_secs(delay)).ok();
    if stage > 9 {
        ger.load_tags(&worker.conn, std::time::Duration::from_secs(999)).ok();
        ger.collect_words();
    }
    ger
}
