use super::{
    db,
    db::matches::MatchLookup,
    error::GenErr,
    mdl::Match,
    prio_log,
};

use itertools::iproduct;
use log::{info, debug, trace, warn, error};

use std::{
    cmp::Ordering,
    collections::{BinaryHeap, BTreeMap, BTreeSet, HashMap, HashSet},
    ops::{Sub},
};


pub fn strong_weak(
    tag: &String,
    tagger: &mut db::tagger::Tagger,
    wins: &MatchLookup,
    db: &crate::db::Connection,
) -> Vec<(String, i8, i8, i16, String)> {
    let mut loss = MatchLookup::default();
    for m in wins.values().flat_map(|m| m.iter()) {
        if let Some(loser) = m.loser() {
            loss.entry(loser.clone())
                .or_insert_with(BTreeSet::default)
                .insert(m.clone());
        }
    }
    let bots = tagger.get_bots4sure(tag, &db).expect("Found no bots with that tag.");
    let empty = BTreeSet::default();
    let mut recs: HashMap<String, (i32, i32)> = HashMap::default();
    for bot in &bots {
        let win = wins.get(bot).unwrap_or(&empty);
        let los = loss.get(bot).unwrap_or(&empty);
        if let Some(otags) = tagger.get_tags4sure(bot, &db) {
            for otag in &otags {
                if tag == otag {
                    continue;
                }
                let rec = recs.entry(otag.clone()).or_default();
                if let Some(obots) = tagger.get_bots4sure(otag, &db) {
                    for obot in obots.difference(&bots) {
                        rec.0 += win.iter().filter(|m| m.involves(obot)).count() as i32;
                        rec.1 += los.iter().filter(|m| m.involves(obot)).count() as i32;
                    }
                }
            }
        }
    }
    let mut recs: Vec<(String, i8, i8, i16, String)> = recs
        .into_iter()
        .filter_map(|(tag, (win, los))| {
            let tot = win + los;
            if tot < 10 {
                None
            } else {
                let bots = tagger.get_bots4sure(&tag, db).unwrap_or_else(BTreeSet::default);
                let lo = win * 100 / (tot + 1);
                let hi = (win + 1) * 100 / (tot + 1);
                let tot: i16 = tot.try_into().unwrap();
                let lo: i8 = lo.try_into().unwrap();
                let hi: i8 = hi.try_into().unwrap();
                let mut bots: Vec<String> = bots.into_iter().collect();
                bots.sort_by_key(|b| (b.len(), b.clone()));
                let mut sample = bots[0].clone();
                for b in bots.iter().skip(1) {
                    if sample.len() + b.len() > 80 {
                        sample.push_str("...");
                        break;
                    }
                    sample.push(',');
                    sample.push_str(&b);
                }
                Some((tag, lo, hi, tot, sample))
            }
        })
        .collect();
    recs.sort_by_key(|(x, l, h, t, samp)| (50 - l, 50 - h, samp.clone(), x.clone(), *t));
    let mut i = 0;
    while i + 1 < recs.len() {
        let r = recs[i + 1].clone();
        let l = &mut recs[i];
        if l.4 == r.4 {
            if l.0.contains(&r.0) {
                ()
            } else if r.0.contains(&l.0) || l.0.split('/').any(|t| r.0.contains(t)) {
                l.0 = r.0.clone();
            } else {
                l.0.push('/');
                l.0.push_str(&r.0);
            }
            recs.remove(i + 1);
        } else {
            i += 1;
        }
    }
    recs
}

#[allow(dead_code)]
pub fn make(tagger: &db::tagger::Tagger, wins: &HashMap<String, BTreeSet<String>>) {
    let tags = tagger.all_tags();
    let mut it = tags.iter();
    let mut thresh = 0;
    println!("Example of wins: {:?}", wins.iter().next());
    assert_ne!(wins.len(), 0);
    while let Some(tag_a) = it.next() {
        let bots_a = tagger.get_bots(tag_a).unwrap_or_else(BTreeSet::default);
        for tag_b in it.clone() {
            let mut a_wins: i32 = 0;
            let mut b_wins: i32 = 0;
            let bots_b = tagger.get_bots(tag_b).unwrap_or_else(BTreeSet::default);
            let anotb = bots_a.difference(&bots_b);
            let bnota = bots_b.difference(&bots_a);
            /*
            println!("{tag_a} {} ^ {tag_b} {} -> {} {}",
                     bots_a.len(),
                     bots_b.len(),
                     anotb.clone().count(),
                     bnota.clone().count(),
            );
             */
            for (bot_a, bot_b) in iproduct!(anotb.clone(), bnota.clone()) {
                let avb = wins.get(bot_a);
                let bva = wins.get(bot_b);
                /*
                if avb.is_some() || bva.is_some() {
                    println!("{bot_a} vs {bot_b} | {:?} vs {:?}",
                             avb,
                             bva
                    );
                }

                 */
                if avb.map(|w| w.contains(bot_b)).unwrap_or(false) {
                    a_wins += 1;
                }
                if bva.map(|w| w.contains(bot_a)).unwrap_or(false) {
                    b_wins += 1;
                }
            }
            sho(a_wins, b_wins, tag_a, tag_b, &mut thresh);
        }
    }
}

fn sho(a: i32, b: i32, at: &str, bt: &str, thresh: &mut i32) {
    if a < b {
        sho(b, a, bt, at, thresh);
        return;
    }
    let diff = a - b;
    if diff > *thresh {
        *thresh += 1;
        let tot = a + b;
        println!(
            "'{at}' beats '{bt}' {a} out of {tot} times aka {}%.",
            (a * 100) / tot
        );
    }
}


#[derive(Copy, Clone, Hash, PartialEq, Eq, Debug, PartialOrd, Ord)]
enum Whose {
    A,
    B,
}

fn other(w: Whose) -> Whose {
    use Whose::*;
    match w {
        A => B,
        B => A,
    }
}

#[derive(Copy, Clone, Hash, PartialEq, Eq, Debug, PartialOrd, Ord)]
enum Polarity {
    Strong,
    Weak,
}

#[allow(dead_code)]
fn reverse(p: Polarity) -> Polarity {
    use Polarity::*;
    match p {
        Strong => Weak,
        Weak => Strong,
    }
}

#[derive(Clone, Debug)]
struct Task {
    bot: String,
    whose: Whose,
    polarity: Polarity,
    weight: f64,
}

impl Eq for Task {}

impl PartialEq for Task {
    fn eq(&self, other: &Self) -> bool {
        if self.bot == other.bot
            && self.whose == other.whose
            && self.polarity == other.polarity {
            (self.weight - other.weight).abs() < 1e-9
        } else {
            false
        }
    }
}

impl PartialOrd for Task {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        other.weight.partial_cmp(&self.weight)
    }
}

impl Ord for Task {
    fn cmp(&self, other: &Self) -> Ordering {
        if (self.weight - other.weight).abs() < 1e-9 {
            Ordering::Equal
        } else if self.weight < other.weight {
            Ordering::Greater
        } else {
            Ordering::Less
        }
    }
}

type State = HashMap<(Whose, Polarity), BTreeMap<String, f64>>;

#[allow(dead_code)]
pub fn fight(a: &String, b: &String, tagger: &mut db::tagger::Tagger, db: &crate::db::Connection, match_cache: &mut db::matches::CachedLookup) -> Result<i8, GenErr> {
    let mut last_persist = std::time::Instant::now();

    warn!("Simulating a fight between {a} ({}-{}) and {b} ({}-{})",
        match_cache.lookup(a,db).expect("botnotfound").wins.len(),
        match_cache.lookup(a,db).expect("botnotfound").loss.len(),
        match_cache.lookup(b,db).expect("botnotfound").wins.len(),
        match_cache.lookup(b,db).expect("botnotfound").loss.len(),
    );
    let mut todo = vec![
        Task { bot: a.clone(), whose: Whose::A, polarity: Polarity::Strong, weight: 1. },
        Task { bot: a.clone(), whose: Whose::A, polarity: Polarity::Weak, weight: 1. },
        Task { bot: b.clone(), whose: Whose::B, polarity: Polarity::Strong, weight: 1. },
        Task { bot: b.clone(), whose: Whose::B, polarity: Polarity::Weak, weight: 1. },
    ];
    let mut state: State = HashMap::default();
    let mut ascore = 1e-9;
    let mut bscore = 1e-9;
    let log_state = |ascore, bscore, state: &State, todo: &Vec<Task>| {
        debug!("Q len {}",todo.len());
        info!("A strengths {}",state.get(&(Whose::A,Polarity::Strong)).map(|m|m.len()).unwrap_or(0));
        info!("B strengths {}",state.get(&(Whose::B,Polarity::Strong)).map(|m|m.len()).unwrap_or(0));
        info!("A weaknesses {}",state.get(&(Whose::A,Polarity::Weak)).map(|m|m.len()).unwrap_or(0));
        info!("B weaknesses {}",state.get(&(Whose::B,Polarity::Weak)).map(|m|m.len()).unwrap_or(0));
        for ((w, p), m) in state {
            for (b, x) in m {
                debug!("State:{:?}/{:?}/{b}={x}",&w,&p);
            }
        }
        warn!("Scores: {a}=>{ascore} vs {b}=>{bscore}");
    };
    for round in 0..99 {
        if tagger.tag_a_bot(db).unwrap_or(1) > 0 {
            info!("Loading more tags, up-front! ");
            tagger.load_tags(db, std::time::Duration::from_secs(999)).ok();
        } else {
            warn!("All tags loaded!");
        }
        for _ in 0..99 {
            prio_log::LOGGER.check();
        }
        if last_persist.elapsed().as_secs() > 99 {
            eprintln!("Persisting log priorities, todo has {} tasks", todo.len());
            db.save_log_priorities();
            last_persist = std::time::Instant::now();
        }
        log_state(ascore, bscore, &state, &todo);
        let mut next: HashMap<(Whose, Polarity, String), f64> = HashMap::default();
        for t in &todo {
            let mut next_add = |w, p, b: &str, x| {
                let ent = next.entry((w, p, b.to_string())).or_default();
                *ent = prob_add(*ent, x);
            };
            let t = t.clone();
            trace!("pop {:?}, {} remain",&t, todo.len());
            for _ in 0..99 {
                prio_log::LOGGER.check();
            }
            if tagger.tag_a_bot(db).unwrap_or(1) > 0 {
                debug!("Loading more tags, on-the-fly!");
                prio_log::LOGGER.check();
                tagger.load_tags(db, std::time::Duration::from_secs(9)).ok();
                prio_log::LOGGER.check();
            } else {
                info!("All tags loaded!");
                prio_log::LOGGER.check();
            }
            let m = state.entry((t.whose, t.polarity)).or_default();
            if let Some(weight) = m.get_mut(&t.bot) {
                let notand = prob_add(*weight, t.weight);
                debug!("Increasing weight on {:?}/{:?}/{}: {} + {} = {notand}", t.whose,t.polarity,&t.bot, *weight, t.weight);
                *weight = notand;
            } else {
                warn!("Inserted into ({:?},{:?}):{}={}",t.whose,t.polarity,&t.bot,t.weight);
                m.insert(t.bot.clone(), t.weight);
            }
            log_state(ascore, bscore, &state, &todo);
            if let Some(tags) = tagger.get_tags(&t.bot) {
                let mut to_insert: HashMap<String, f64> = HashMap::default();
                for tag in tags {
                    let bots = tagger.get_bots(&tag).expect("should at least reflect");
                    let commonality = (bots.len() as f64).log2() + 1.;
                    for bot in bots {
                        let similarity = to_insert.entry(bot).or_default();
                        *similarity += 1. / commonality;
                    }
                }
                if let Some(normalize) = to_insert.get(&t.bot) {
                    let normalize = normalize.clone();
                    for (bot, similarity) in to_insert {
                        let similarity = similarity / normalize;
                        let weight = t.weight * similarity * similarity * similarity / 2.;
                        info!("{} is {}.{}% similar to {bot}, plan to add this to {:?}/{:?}",
                            t.bot,
                            (similarity * 100.) as i8,
                            (similarity * 1e3) as i8 % 10,
                            t.whose,
                            t.polarity,
                        );
                        next_add(t.whose, t.polarity, &bot, weight);
                    }
                } else {
                    error!("How does {} not share tags with itself‽",&t.bot);
                }
            }
            let matches = match_cache.lookup(&t.bot, db);
            if matches.is_none() {
                continue;
            }
            let sided_matches = match t.polarity {
                Polarity::Strong => matches.unwrap().wins,
                Polarity::Weak => matches.unwrap().loss,
            };
            for (opp, machs) in sided_matches {
                if opp.contains(' ') || opp.trim().is_empty() {
                    continue;
                }
                prio_log::LOGGER.check();
                let repeats = machs.len() as f64;
                let weight = t.weight * repeats / (repeats + 2.);
                info!("Adding {opp} to {:?}/{:?} ({weight}) due to {:?}",t.whose,t.polarity,&machs);
                next_add(t.whose, t.polarity, &opp, weight);
            }
        }
        if let Some((a_score, b_score)) = score(&state) {
            let old_lead = leading_bot(a, b, ascore, bscore);
            let lead = leading_bot(a, b, a_score, b_score);
            println!("After round {round} : leader = {:?} ; {a} = {a_score} vs {b_score} = {b} ; cumulative leader = {:?} .", &lead, &old_lead);
            if let (Some(l), Some(ol)) = (&lead, &old_lead) {
                if l.0 != ol.0 {
                    return Err(GenErr::Inconclusive(a.clone(), b.clone(), "Scope-dependent".to_owned()));
                }
                let shift: i16 = l.1.sub(ol.1).abs().into();
                if shift < round {
                    info!("Scoring seems to have stabilized enough.");
                    break;
                }
                update_cumulative(&mut ascore, &mut bscore, a_score, b_score, &l, &ol);
            } else if let Some(l) = &lead {
                info!("{:?} has pulled into the lead!",l);
                ascore = a_score;
                bscore = b_score;
            }
        }
        todo = next.into_iter().filter_map(|((whose, polarity, bot), weight)| {
            if weight > 0.001 {
                Some(Task { whose, polarity, bot, weight })
            } else {
                None
            }
        }).collect();
        log_state(ascore, bscore, &state, &todo);
    }
    for (k, sw) in state {
        let v: Vec<(f64, String)> = sw.into_iter().map(|(a, b)| (b, a)).collect();
        for e in v {
            warn!("Final state: {:?} {:?}",k,e);
        }
    }
    error!("Ending...");
    for _ in 0..99 {
        prio_log::LOGGER.check();
        prio_log::LOGGER.speed_up();
        prio_log::LOGGER.check();
    }
    let result: f64 = ascore * 100. / (ascore + bscore);
    println!("Ending with ascore={ascore} bscore={bscore}");
    Ok(result.round() as i8)
}

fn leading_bot(an: &str, bn: &str, ax: f64, bx: f64) -> Option<(String, i8)> {
    if ax > bx * 1.01 {
        Some((an.to_owned(), (ax * 100. / (ax + bx)).round() as i8))
    } else if bx > ax * 1.01 {
        Some((bn.to_owned(), (bx * 100. / (ax + bx)).round() as i8))
    } else {
        None
    }
}

fn score(st: &State) -> Option<(f64, f64)> {
    let check_side = |score: &mut f64, side: Whose| {
        let mut match_count = 0;
        for (k, v) in st.iter() {
            trace!("Key={:?} Val has {}", &k, v.len());
        }
        if let (Some(strongs), Some(weaks)) = (st.get(&(side, Polarity::Strong)), st.get(&(other(side), Polarity::Weak))) {
            info!("{:?} has {} strengths, {:?} has {} weaknesses", side, strongs.len(),other(side),weaks.len());
            for (bot, strong) in strongs {
                debug!("{bot} is a strength{strong} of {:?}",side);
                if let Some(weak) = weaks.get(bot) {
                    warn!("Side {:?}: scores on {bot}: {score}+= {strong}*{weak} ", &side);
                    *score += strong * weak;
                    match_count += 1;
                }
            }
        }
        match_count
    };
    let mut a = 0.;
    let mut b = 0.;
    let an = check_side(&mut a, Whose::A);
    let bn = check_side(&mut b, Whose::B);
    if an + bn > 0 && a > 1e-9 && b > 1e-9 {
        info!("Have discovered {an}+{bn}={} cross-overs.",an+bn);
        Some((a, b))
    } else {
        None
    }
}

fn prob_add(a: f64, b: f64) -> f64 {
    let l = 1. - a;
    let r = 1. - b;
    1. - (l * r)
}

fn update_cumulative(ascore: &mut f64, bscore: &mut f64, a_score: f64, b_score: f64, l: &(String, i8), ol: &(String, i8)) {
    let old_extrem = ol.1.sub(50).abs();
    let extrem = l.1.sub(50).abs();
    if extrem < old_extrem {
        trace!("Weight new result more heavily, because {a_score}/{b_score}=>{}=>{extrem} < {old_extrem}<={}<={ascore}/{bscore}.",l.1,ol.1);
        *ascore = (*ascore + a_score * 2.) / 3.;
        *bscore = (*bscore + b_score * 2.) / 3.;
    } else {
        trace!("Weight old result more heavily, because {ascore}/{bscore}=>{}=>{old_extrem} < {extrem}<={}<={a_score}/{b_score}.",ol.1,l.1);
        *ascore = (2. * *ascore + a_score) / 3.;
        *bscore = (2. * *bscore + b_score) / 3.;
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct TagMatchup {
    tag_a: String,
    tag_b: String,
    orig_a: usize,
    orig_b: usize,
    remain_a: Vec<Match>,
    remain_b: Vec<Match>,
}

impl TagMatchup {
    fn key(&self) -> (usize, usize, usize, usize) {
        let hi = self.remain_a.len().max(self.remain_b.len());
        let lo = self.remain_a.len().min(self.remain_b.len());
        let look = self.orig_a.max(self.orig_b) - self.orig_a.min(self.orig_b);
        let tag_len = self.tag_a.len()
            + self.tag_b.len()
            + self.tag_a.chars().filter(|c| !c.is_alphabetic()).count()
            + self.tag_a.chars().filter(|c| !c.is_alphabetic()).count()
            ;
        (hi - lo, hi, look, tag_len)
    }
    pub fn favors_a(&self) -> bool {
        if self.remain_a.len() > self.remain_b.len() {
            true
        } else if self.remain_b.len() > self.remain_a.len() {
            false
        } else {
            self.orig_a > self.orig_b
        }
    }
    pub fn tags(&self) -> [String;2] {
        [self.tag_a.clone(), self.tag_b.clone()]
    }
}

impl PartialOrd for TagMatchup {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.key().partial_cmp(&other.key())
    }
}

impl Ord for TagMatchup {
    fn cmp(&self, other: &Self) -> Ordering {
        self.key().cmp(&other.key())
    }
}

pub fn matchup_factors(bota: &String, botb: &String, tagger: &mut db::tagger::Tagger, db: &crate::db::Connection, match_cache: &mut db::matches::CachedLookup) -> Vec<TagMatchup> {
    let all_a = tagger.get_tags4sure(bota, db).expect("found no tags for left bot");
    let all_b = tagger.get_tags4sure(botb, db).expect("found no tags for right bot");
    let a_tags: Vec<String> = all_a.difference(&all_b).map(|s| s.clone()).collect();
    let b_tags: Vec<String> = all_b.difference(&all_a).map(|s| s.clone()).collect();
    let mut tvt = BinaryHeap::default();
    info!("{bota} has tags that {botb} does not: {:?}", &a_tags);
    info!("{botb} has tags that {bota} does not: {:?}", &b_tags);
    for a_tag in &a_tags {
        if a_tag.contains("WIN") || a_tag.contains("WON") || a_tag.contains("LOST") || a_tag.contains("LOSE") {
            continue;
        }
        let all_a_bots = tagger.get_bots4sure(a_tag, db).expect("found no bots for left tag");
        debug!("Looking at matches related to {bota}'s tag {a_tag} ... {} bots.",all_a_bots.len());
        for b_tag in &b_tags {
            if b_tag.contains("WIN") || b_tag.contains("WON") || b_tag.contains("LOST") || b_tag.contains("LOSE") {
                continue;
            }
            let mut tmu = TagMatchup {
                tag_a: a_tag.clone(),
                tag_b: b_tag.clone(),
                orig_a: 0,
                orig_b: 0,
                remain_a: Vec::default(),
                remain_b: Vec::default(),
            };
            let all_b_bots = tagger.get_bots4sure(b_tag, db).expect("found no bots for right tag");
            let a_bots = all_a_bots.difference(&all_b_bots);
            let b_bots: Vec<String> = all_b_bots.difference(&all_a_bots).map(|s| s.clone()).collect();
            for a_bot in a_bots {
                if let Some(matches) = match_cache.lookup(&a_bot, db) {
                    for b_bot in &b_bots {
                        if let Some(wins) = matches.wins.get(b_bot) {
                            tmu.orig_a += wins.len();
                            tmu.remain_a.extend(wins.iter().map(|m| m.clone()));
                        }
                        if let Some(loss) = matches.loss.get(b_bot) {
                            tmu.orig_b += loss.len();
                            tmu.remain_b.extend(loss.iter().map(|m| m.clone()));
                        }
                    }
                }
            }
            tvt.push(tmu);
        }
    }
    let mut seen: HashSet<String> = HashSet::default();
    let mut result = Vec::default();
    while let Some(mut tmu) = tvt.pop() {
        let len = (tmu.remain_a.len(), tmu.remain_b.len());
        tmu.remain_a.retain(|m| !seen.contains(&m.winner));
        tmu.remain_b.retain(|m| !seen.contains(&m.winner));
        if tmu.remain_b.len() + tmu.remain_a.len() == 0 {
            continue;
        } else if len == (tmu.remain_a.len(), tmu.remain_b.len()) {
            seen.extend(tmu.remain_a.iter().map(|m| m.winner.clone()));
            seen.extend(tmu.remain_b.iter().map(|m| m.winner.clone()));
            result.push(tmu);
        } else {
            tvt.push(tmu);
        }
    }
    result
}

pub fn bot_strengths_weaknesses(conn: &crate::db::Connection, bot: &str )  -> Result<Vec<(u32,String)>, GenErr> {
    let mut q = conn.prepare("SELECT a.tag, m.winner FROM tags a INNER JOIN matches m ON a.slug = m.opp AND m.slug = ?1")?;
    let mut rows = q.query([bot])?;
    let mut counts: BTreeMap<String,(u32,u32)> = BTreeMap::new();
    while let Ok(Some(row)) = rows.next() {
        let e = counts.entry(row.get(0)?).or_default();
        let winner: String = row.get(1)?;
        if winner == bot {
            e.0 += 1;
        } else {
            e.1 += 1;
        }
    }
    let mut result: Vec<(u32,String)> = counts.into_iter().filter(|(_,(a,b))|a+b >9).map(|(t,(w,l))| (w*1000/(w+l), t)).collect();
    result.sort();
    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;
    // use crate::*;

    #[test]
    fn yesthereisaleader() {
        let actual = leading_bot("Rusty", "Kraken", 46.255972334251105, 106.24169583952792);
        let expected = Some(("Kraken".to_owned(), 70));
        assert_eq!(actual, expected);
    }

    #[test]
    fn cumalitive() {
        let mut oa = 9.;
        let mut ob = 1.;
        let a = 7.;
        let b = 3.;
        let ol = leading_bot("a", "b", oa, ob).unwrap();
        assert_eq!(ol.0, "a");
        assert_eq!(ol.1, 90);
        let l = leading_bot("a", "b", a, b).unwrap();
        assert_eq!(l.0, "a");
        assert_eq!(l.1, 70);
        update_cumulative(&mut oa, &mut ob, a, b, &l, &ol);
        assert_eq!(oa, (9. + 7. * 2.) / 3.);
        assert_eq!(ob, (1. + 3. * 2.) / 3.);
    }
}
