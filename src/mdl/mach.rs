#[derive(Debug, Ord, PartialOrd, PartialEq, Eq, Default, Clone)]
pub struct Match {
    //a slug
    pub(crate) competition: String,

    // e.g. "Fight Night", "Desperado"... In case rematch in the same competition
    pub(crate) segment: String,

    //slugs. Typically has 2 entries, more for a rumble.
    pub(crate) bots: Vec<String>,
    //a slug, "" for a double-loss, or otherwise ambiguous result (e.g. their page says they lost a rumble but doesn't say who won)
    pub(crate) winner: String,
    pub(crate) note: String, //e.g. "by default", "by KO", "controversial", "rake smacked a drone"....
    pub(crate) sequence: u16,
}

impl Match {
    pub fn loser(&self) -> Option<&String> {
        self.other(&self.winner)
    }
    pub fn other(&self, bot: &str) -> Option<&String> {
        self.bots.iter().find(|b| *b != bot)
    }
    pub fn involves(&self, bot: &String) -> bool {
        self.bots.contains(bot)
    }
}
