#[derive(Default, Debug)]
pub struct Team {
    pub slug: String,
    pub name: String,
    pub members: Vec<String>,
    pub incarnation: String,
}

impl Team {
    pub fn new(incarnation: String) -> Self {
        Self {
            slug: String::default(),
            name: String::default(),
            members: Vec::default(),
            incarnation,
        }
    }
}
