use log::{info, trace, warn};
use num_enum::{IntoPrimitive, TryFromPrimitive};
use std::collections::BTreeSet;

#[derive(Debug, TryFromPrimitive, PartialEq, IntoPrimitive, Copy, Clone)]
#[repr(i8)]
pub enum Op {
    //Don't do anything, other than perhaps diagnostic
    Noop,
    //lhs && rhs, the intersection
    And,
    //lhs || rhs, the union
    Or,
    ButNot, //lhs && !rhs The only approach I have here for negation
}

#[derive(Debug)]
pub struct Rule {
    lhs: String,
    op: Op,
    rhs: String,
    out: String,
}

impl Rule {
    pub fn new(op: Op, lhs: String, rhs: String, out: String) -> Self {
        Self { lhs, rhs, op, out }
    }
    pub fn eval(
        &self,
        fetch_bots: &mut dyn FnMut(&String) -> Option<BTreeSet<String>>,
    ) -> Option<Vec<String>> {
        trace!("start:eval({:?})", &self);
        let lefts: BTreeSet<String> = fetch_bots(&self.lhs).unwrap_or_else(BTreeSet::default);
        let rites: BTreeSet<String> = fetch_bots(&self.rhs).unwrap_or_else(BTreeSet::default);
        let already = fetch_bots(&self.out).unwrap_or_else(BTreeSet::default);
        info!("lefts.len()={} rites.len()={} already.len()={} for {:?}", lefts.len(), rites.len(), already.len(), &self);
        let result = match self.op {
            Op::And => finish_eval(lefts.intersection(&rites), &already),
            Op::Or => finish_eval(lefts.union(&rites), &already),
            Op::ButNot => finish_eval(lefts.difference(&rites), &already),
            Op::Noop => {
                warn!(
                    "rule evaluated with a NOOP operator, this is likely a mistake. {:?}",
                    &self
                );
                vec![]
            }
        };
        trace!("end:eval({:?})", &self);
        Some(result)
    }
    pub fn out_tag(&self) -> &str {
        self.out.trim()
    }
}

fn finish_eval<'a, It: Iterator<Item=&'a String>>(
    it: It,
    already: &BTreeSet<String>,
) -> Vec<String> {
    it.filter(|b| !already.contains(b.as_str()))
        .map(|b| b.clone())
        .collect()
}
