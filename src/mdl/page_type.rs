use num_enum::{IntoPrimitive, TryFromPrimitive};
use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ToSqlOutput, ValueRef};
use rusqlite::ToSql;

#[derive(Debug, TryFromPrimitive, PartialEq, IntoPrimitive, Copy, Clone)]
#[repr(i8)]
pub enum PageType {
    Unknown = 0,
    Competition,
    Team,
    Bot,
    Updates,
    Competitions,
    Teams,
    Bots,
    Human,
    Term,
    Alias,
    Category,
    Other,
}

impl FromSql for PageType {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let big = value.as_i64()?;
        if let Ok(i) = i8::try_from(big) {
            Ok(PageType::try_from(i).or(Err(FromSqlError::OutOfRange(big)))?)
        } else {
            Err(FromSqlError::OutOfRange(big))
        }
    }
}

impl ToSql for PageType {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        Ok(ToSqlOutput::from(i8::from(*self)))
    }
}
