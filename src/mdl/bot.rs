use super::team::*;

use time::OffsetDateTime;

use std::collections::*;

#[derive(Debug, Default)]
pub struct Honor {
    pub name: String,
    pub competition: String,
}

#[derive(Debug)]
pub struct Bot {
    pub slug: String,
    pub name: String,
    pub updated: OffsetDateTime,
    pub desc: String,
    pub stats: BTreeMap<String, String>,
    pub cats: Vec<String>,
    pub trivia: Vec<String>,
    pub honors: Vec<Honor>,
    pub team: Team,
}

impl Bot {
    pub fn new(
        slug: String,
        name: String,
        updated: OffsetDateTime,
        desc: String,
        cats: Vec<String>,
        honors: Vec<Honor>,
    ) -> Self {
        Self {
            slug: slug.clone(),
            name,
            updated,
            desc,
            stats: BTreeMap::default(),
            cats,
            trivia: Vec::default(),
            honors,
            team: Team::new(slug),
        }
    }
}
