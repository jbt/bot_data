pub mod bot;
pub mod mach;
pub mod page_type;
pub(crate) mod tag_logic;
pub mod team;
pub mod page;

pub use mach::Match;
pub use page_type::PageType;
pub use page::Page;
