use time::OffsetDateTime;
use std::fmt::{Display, Formatter};
use rand::Rng;
use crate::mdl::PageType;
use crate::wiki::{cat};

#[derive(Debug, Clone)]
pub struct Page {
    pub(crate) slug: String,
    pub(crate) typ: PageType,
    pub(crate) html: String,
    pub(crate) updated: OffsetDateTime,
}

impl Page {
    pub(crate) fn unfetched(slug: String, typ: PageType) -> Self {
        Self {
            slug,
            typ,
            html: String::default(),
            updated: OffsetDateTime::UNIX_EPOCH,
        }
    }
    pub(crate) fn updates_page() -> Self {
        Self::unfetched(
            "Special:RecentChanges?hidebots=0&limit=500&days=70&enhanced=1&urlversion=2".to_owned(),
            PageType::Updates,
        )
    }
    fn category_page(cat: &str, pt: PageType) -> Vec<Self> {
        let mut result = Vec::new();
        result.push(Self::unfetched(format!("Category:{}", cat), pt));
        let mut rng = rand::thread_rng();
        let letter = rng.gen_range('A'..'[');
        result.push(Self::unfetched(
            format!("Category:{}?from={}", cat, letter),
            pt,
        ));
        result
    }
    pub(crate) fn bots_pages() -> Vec<Self> {
        cat::bot_cats()
            .iter()
            .flat_map(|c| Self::category_page(c, PageType::Bots))
            .collect()
    }
    pub(crate) fn competitions_page() -> Vec<Self> {
        Self::category_page("Competitions", PageType::Competitions)
    }
    pub(crate) fn with_fetched_text(self, txt: String) -> Self {
        Self {
            slug: self.slug,
            typ: self.typ,
            updated: OffsetDateTime::now_utc(),
            html: txt, // Some(Html::parse_document(txt.as_str())),
        }
    }
}

impl Display for Page {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "<Page slug={} type={:?} @={} ({}B)>",
            &self.slug,
            self.typ,
            &self.updated,
            self.html.len()
        )
    }
}
