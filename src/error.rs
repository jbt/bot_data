use std::num::TryFromIntError;

pub type Result<T> = std::result::Result<T, GenErr>;

#[derive(Debug, thiserror::Error)]
pub enum GenErr {
    #[error("Int out-of-bounds: {0}")]
    IntBounds(#[from] TryFromIntError),

    #[error("Error: {0}")]
    Whatever(String),

    #[error("Error: Sqlite: {0}")]
    Db(#[from] rusqlite::Error),

    #[error("{0} vs {1} : Inconclusive: {2}")]
    Inconclusive(String, String, String),
}

impl From<&str> for GenErr {
    fn from(value: &str) -> Self {
        GenErr::Whatever(value.to_owned())
    }
}

impl From<String> for GenErr {
    fn from(value: String) -> Self {
        GenErr::Whatever(value)
    }
}