use std::ops::Add;
use std::io::Write;
use log::{Metadata, Record};
use priority_queue::PriorityQueue;
use time::{Duration, Instant};
use crate::traits::log::*;

//used priorities
//1 2 3 4 5 6 7 9 A C E F 10 11 13 15 17 19 1B 1F 20 21 22 24 30 32 38 40 90 AB AD B0 B2 C0 C2 CD D0 D4 D6  DA DB   DF E1 E3 EB   EF   F6  F8  FA FC   FE FF

#[derive(Debug)]
pub struct BackLog {
    q: PriorityQueue<String, u8>,
    last: Instant,
    thresh: i16,
}

impl Default for BackLog {
    fn default() -> Self {
        Self {
            q: PriorityQueue::default(),
            last: Instant::now(),
            thresh: 0,
        }
    }
}

const PERIOD: Duration = Duration::milliseconds(1234);

impl MutatingLogger for BackLog {
    fn log(&mut self, line: String, priority: u8) {
        self.q.push(line, priority);
    }
    fn log_str(&mut self, line: &str, priority: u8) {
        self.log(line.to_string(), priority);
    }
}

impl BackLog {
    pub async fn show(&mut self) -> bool {
        if let Some((l, p)) = self.q.peek() {
            let past = std::time::Duration::from_millis(*p as u64);
            let next_time = self.last.add(PERIOD);
            let now = Instant::now();
            let d: Duration = (next_time - now) / 2;
            let sleepy = d.unsigned_abs() + past;
            let result = *p as i16 > self.thresh;
            if result {
                self.thresh += 9;
            } else {
                self.thresh -= 1;
            }
            if now >= next_time {
                println!(".{:#02X} {}", p, l);
                self.last = Instant::now();
                self.q.pop();
            } else if result {
                tokio::time::sleep(sleepy / 3).await;
            }
            result
        } else {
            false
        }
    }
}

#[derive(Default)]
pub struct Immediate {}

impl Logger for Immediate {
    fn log(&self, line: String, _: u8) {
        std::io::stderr().write(line.trim().as_bytes()).ok();
        std::io::stderr().write(&[10]).ok();
    }

    fn log_str(&self, line: &str, _: u8) {
        std::io::stderr().write(line.trim().as_bytes()).ok();
        std::io::stderr().write(&[10]).ok();
    }
}
