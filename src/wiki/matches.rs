use super::util::*;

use crate::db::Bot;
use crate::mdl::*;
use crate::traits;
use regex::Regex;
use scraper::{ElementRef, Html};
use std::collections::BTreeSet;
use traits::*;

use itertools::Itertools;
use log::{error, info, trace};

struct Parms<'a> {
    bot_source: &'a Bot,
    html: &'a Html,
    // log: &'a dyn Logger,
    aliases: &'a BTreeSet<String>,
}

type Approach = (
    &'static str,
    fn(&Parms, &dyn traits::lu::Competition) -> Option<Vec<Match>>,
);

//TODO: currently all of these only get linked bots. i.e. Alakran only fights Icewave and Pharoh
const APPROACHES: &[Approach] = &[
    ("delux 'Results' section", results_with_subheads),
    ("named 'Results' section", new_results),
    ("named 'Wins/Losses' section", old_winslosses),
    ("unnamed 'Wins/Losses' table", competition_wins_losses_table),
];

pub fn extract_from_page(
    page: &super::Page,
    parsed_bot: &Bot,
    comps: &dyn traits::lu::Competition,
    aliases: &BTreeSet<String>,
) -> Vec<Match> {
    let h = Html::parse_document(page.html.trim());
    info!(
        "Matches: parsed {} bytes, now calling extract for {}",
        page.html.len(),
        &page.slug
    );
    let mut result = extract(parsed_bot, &h, comps, &aliases);
    result.sort();
    for pair in result.windows(2) {
        let a = &pair[0];
        let b = &pair[1];
        if a.competition == b.competition
            && a.bots == b.bots
            && a.segment == b.segment
            && a.winner == b.winner
            && a.note == b.note
        {
            error!("ERROR: TODO : need to implement sub-competition segment for {}: {:?} vs {:?}",
                page.slug, a, b
            );
        }
    }
    result
}

pub(crate) fn extract(
    bot_source: &Bot,
    h: &Html,
    comps: &dyn traits::lu::Competition,
    aliases: &BTreeSet<String>,
) -> Vec<Match> {
    trace!("fn extract {}", &bot_source.slug);
    let parms = Parms {
        bot_source,
        html: h,
        aliases,
    };
    for approach in APPROACHES {
        info!(
            "About to attempt approach {:?} on {}",
            &approach.0, &bot_source.slug
        );
        if let Some(mut result) = approach.1(&parms, comps) {
            info!(
                "Got {} results for {} using approach: {}",
                result.len(),
                &bot_source.slug,
                &approach.0
            );
            if !result.is_empty() {
                for i in 0..(result.len() as u16) {
                    result[i as usize].sequence = i;
                }
                return result;
            }
        }
    }
    Vec::new()
}

fn new_results(parms: &Parms, comps: &dyn traits::lu::Competition) -> Option<Vec<Match>> {
    let mut result = None;
    if let Some(mut results_header) = parms.html.select(&tor("#Results")).next() {
        while results_header.value().name() != "h2" && results_header.value().name() != "h3" {
            if let Some(rent) = results_header.parent() {
                if let Some(e) = ElementRef::wrap(rent) {
                    results_header = e;
                } else {
                    println!(
                        "Parent of 'Results' header is not an element, for {}? {:?}",
                        parms.bot_source.slug,
                        rent.value()
                    );
                    break;
                }
            } else {
                println!("Got to the top of the HTML tree, ascending from 'Results', without ever hitting an h2! {}", parms.bot_source.slug);
                break;
            }
        }
        if let Some(results_table) = after(&results_header, &"table") {
            let mut competition = String::default();
            let note_re = Regex::new(r"\((.*)\)").unwrap();
            for n in child_element(&results_table, "tbody")
                .expect("table with no tbody")
                .children()
            {
                if let Some(e) = ElementRef::wrap(n) {
                    if e.value().name() != "tr" {
                        continue;
                    }
                    let find_name = |t: &str| {
                        //TODO index looping
                        if comps.competition_lookup(t) != CompRef::Zero {
                            competition = t.to_string();
                            true
                        } else {
                            false
                        }
                    };
                    if e.text().any(find_name) {
                        // println!("Results exist for {} in competition {}", from, competition.trim());
                    } else {
                        if let Some(a) = e.select(&tor("td > a")).next() {
                            let to = a.value().attr("href").unwrap();
                            if !to.starts_with("/wiki/") {
                                continue;
                            }
                            let to = &to[6..];
                            if let Some(b) =
                            after(&ElementRef::wrap(a.parent().unwrap()).unwrap(), "b")
                            {
                                let outcome = b.text().collect::<String>();
                                let winner = if outcome.contains("Won") {
                                    parms.bot_source.slug.to_owned()
                                } else if outcome.contains("Lost") {
                                    to.to_owned()
                                } else {
                                    String::new()
                                };
                                let note = if let Some(cap) = note_re.captures(outcome.trim()) {
                                    cap[1].to_string()
                                } else {
                                    String::default()
                                };
                                if result.is_none() {
                                    result = Some(Vec::new());
                                }
                                let bots = vec![parms.bot_source.slug.to_owned(), to.to_owned()];
                                // bots.sort();
                                result.as_mut().unwrap().push(Match {
                                    bots,
                                    competition: competition.clone(),
                                    winner,
                                    note,
                                    segment: String::new(),
                                    sequence: 0,
                                });
                            } else {
                                //parms.//log.log(format!("ERROR: no bolded outcome for {} vs {}!!", parms.bot_source.slug, to), 0xDA);
                            }
                        }
                    }
                }
            }
        }
    }
    result
}

fn old_winslosses(parms: &Parms, comps: &dyn traits::lu::Competition) -> Option<Vec<Match>> {
    let mut result = None;
    let h = parms.html;
    let from = parms.bot_source.slug.as_str();
    if let Some(heading) = h.select(&tor("#Wins\\/Losses")).next() {
        let h3 = ElementRef::wrap(heading.parent()?)?;
        let table = after(&h3, "table")?;
        for (selector, won) in [
            ("tr > td:nth-child(2)", true),
            ("tr > td:nth-child(3)", false),
        ] {
            for wlcell in table.select(&tor(selector)) {
                let tr = ElementRef::wrap(wlcell.parent()?)?;
                let comp_name = tr
                    .select(&tor("td"))
                    .next()?
                    .text()
                    .find(|t| !t.trim().is_empty())?
                    .trim();
                //TODO: handle unlinked opponents (generally don't have pages). See Bear_Tooth for an example. Should attempt to look them up by name & season combo
                for opp_a in wlcell.select(&tor("a")) {
                    let note = opp_a
                        .parent()
                        .map(|p| {
                            p.children()
                                .filter_map(|c| {
                                    c.value().as_text().map(|t| t.text.trim().to_owned())
                                })
                                .next()
                                .unwrap_or_else(String::new)
                        })
                        .expect("how does it not have a parent");
                    let opp = opp_a.value().attr("href")?;
                    if !opp.starts_with("/wiki/") {
                        continue;
                    }
                    let opp = &opp[6..];
                    let winner = if won { from } else { opp }.to_owned();
                    let bots = vec![from.to_owned(), opp.to_owned()];
                    // bots.sort();
                    if result.is_none() {
                        result = Some(Vec::new());
                    }
                    let competition = match comps.competition_lookup(comp_name) {
                        CompRef::One(c) => c,
                        _ => "TODO old_winslosses: failed to look up ".to_owned() + comp_name,
                    };
                    result.as_mut().unwrap().push(Match {
                        bots,
                        competition, //TODO index looping
                        winner,
                        note,
                        segment: String::new(),
                        sequence: 0,
                    });
                }
            }
        }
        result
    } else {
        None
    }
}

fn competition_wins_losses_table(
    parms: &Parms,
    comps: &dyn traits::lu::Competition,
) -> Option<Vec<Match>> {
    let mut headings = std::collections::HashMap::new();
    let h = parms.html;
    let from = parms.bot_source.slug.as_str();
    // let log = parms.log;
    for th in h.select(&tor("th,td")) {
        let hdr_txt: String = th.text().map(str::trim).filter(|t| !t.is_empty()).collect();
        let presumed_row = th.parent().expect("How is th a root node?");
        let rent_id = presumed_row.id();
        if hdr_txt.trim() == "Losses" {
            if (headings.get("Competition") == Some(&rent_id) || headings.get("") == Some(&rent_id))
                && headings.get("Wins") == Some(&rent_id)
            {
                let parent_row = ElementRef::wrap(presumed_row).expect("tr not an element");
                return Some(competition_wins_losses_row(from, parent_row, comps));
            }
        } else {
            headings.insert(hdr_txt, rent_id);
        }
    }
    None
}

fn competition_wins_losses_row(
    bot: &str,
    mut prev_row: ElementRef,
    comp_lu: &dyn traits::lu::Competition,
) -> Vec<Match> {
    let mut result: Vec<Match> = Vec::new();
    let mut cell_foo = |comp, cell: ElementRef, wl| {
        for a in cell.select(&tor("a")) {
            if let Some(url) = a.value().attr("href") {
                if url.starts_with("/wiki/") {
                    let foe = &url[6..];
                    let winner = if wl { bot } else { foe }.to_owned();
                    let bots = vec![bot.to_owned(), foe.to_owned()];
                    // bots.sort();
                    //TODO index looping
                    let compurl = match comp_lu.competition_lookup(comp) {
                        CompRef::One(u) => u,
                        _ => {
                            "TODO competition_wins_losses_row: failed to look up ".to_owned() + comp
                        }
                    };
                    result.push(Match {
                        bots,
                        competition: compurl,
                        winner,
                        note: String::new(),
                        segment: String::new(), //TODO
                        sequence: 0,
                    });
                }
            }
        }
    };
    while let Some(tr) = after(&prev_row, "tr") {
        let td_tor = tor("td");
        let mut tds = tr.select(&td_tor);
        let competition = if let Some(competition_td) = tds.next() {
            one_txt(competition_td)
        } else {
            continue;
        };
        if let Some(wins_td) = tds.next() {
            cell_foo(competition, wins_td, true);
        } else {
            continue;
        }
        if let Some(losses_td) = tds.next() {
            cell_foo(competition, losses_td, false);
        } else {
            continue;
        }
        prev_row = tr;
    }
    // //log.log(format!("{} matches found for {} ", result.len(), bot), result.len().clamp(1, 250) as u8);
    result
}

fn results_with_subheads(parms: &Parms, comps: &dyn traits::lu::Competition) -> Option<Vec<Match>> {
    let results_header = parms.html.select(&tor("#Results")).next()?;
    let h2 = ElementRef::wrap(results_header.parent()?)?;
    if h2.value().name() != "h2" {
        //parms.//log.log(format!("For {}, parent element is {} rather than h2?", &parms.bot_source.slug, &h2.value().name()), 0xF0);
    }
    let table = after(&h2, "table")?;
    let main_name = parms.bot_source.name.as_str();
    let mut _cur_name = main_name;
    let mut cur_comp = String::new();
    let mut found = Vec::new();
    let mut subsubheads = String::new();
    for row in table.select(&tor("tr")) {
        // //parms.log.log_str(row.text().map(|t| t.trim()).collect::<String>().as_str(), 0x11);
        if row.text().filter(|t| !t.trim().is_empty()).count() == 1 {
            let text_label = one_txt(row);
            let style = row
                .select(&tor("td"))
                .filter_map(|td| td.value().attr("style"))
                .next()
                .unwrap_or("");
            if text_label == main_name.to_uppercase() {
                _cur_name = main_name;
            } else if let Some(a) = parms
                .aliases
                .iter()
                .find(|a| a.to_uppercase() == text_label)
            {
                _cur_name = a;
            } else if let CompRef::One(c) = comps.competition_lookup(text_label) {
                cur_comp = c;
                subsubheads.clear();
            } else if style.contains("a9a9a9") || style.contains("767676") {
                subsubheads.push_str(text_label.trim());
            } else {
                //parms.//log.log(format!("{}: unhandled simple text label row in Results table: {}", &parms.bot_source.slug, &text_label), 0x13);
            }
        } else if row.select(&tor("td")).count() == 1 {
            let td = row.select(&tor("td")).next()?;
            let style = td.value().attr("style").unwrap_or("");
            #[allow(unstable_name_collisions)]
                let txt = td
                .text()
                .map(|t| t.trim())
                .filter(|t| !t.is_empty())
                .intersperse("/")
                .collect::<String>();
            if style.contains("a9a9a9") || style.contains("767676") {
                // //parms.//log.log(format!("{} : Intentionally ignoring gray row: {}", &parms.bot_source.slug, &txt), 0x10);
                subsubheads.push_str(txt.trim());
            } else if !txt.contains("NOTE") {
                //parms.//log.log(format!("{} : unhandled complex text label row in Results table: {}", &parms.bot_source.slug, &txt), 0x12);
            }
        } else if row.select(&tor("td")).count() == 3 {
            let tdtor = tor("td");
            let mut tds = row.select(&tdtor);
            let segment = one_txt(tds.next()?).to_string();
            let opp_td = tds.next()?;
            let outcome_td = tds.next()?;
            let mut opps = if outcome_td.text().any(|t| t.contains("Bye")) {
                vec!["Bye".to_string()]
            } else {
                let mut opps = opp_td
                    .select(&tor("a"))
                    .filter_map(|a| Some(a.value().attr("href")?[6..].to_string()))
                    .collect::<Vec<String>>();
                if opps.is_empty() {
                    opps = opp_td
                        .text()
                        .filter_map(|tok| {
                            if tok.is_empty() {
                                None
                            } else {
                                Some(tok.replace(" ", "_"))
                            }
                        })
                        .collect::<Vec<String>>();
                }
                opps.sort();
                opps
            };
            let won = outcome_td.text().any(|t| t.contains("Won"));
            let lost = outcome_td.text().any(|t| t.contains("Lost"));
            let winner = if won {
                parms.bot_source.slug.clone()
            } else if lost && opps.len() == 1 {
                opps[0].clone()
            } else {
                String::default()
            };
            let txt = outcome_td.text().collect::<String>();
            let note = txt
                .split("(")
                .skip(1)
                .next()
                .map(|t| t.split(")").next())
                .unwrap_or(None)
                .unwrap_or(subsubheads.as_str())
                .to_string();
            opps.insert(0, parms.bot_source.slug.clone());
            found.push(Match {
                competition: cur_comp.to_owned(),
                segment,
                bots: opps,
                winner,
                note,
                sequence: 0,
            });
        } else {
            //parms.//log.log(format!("\n\t TODO: \t ###\tUNHANDLED: {}\t###\n", row.text().map(|t| t.trim()).collect::<String>()), 0x11);
        }
    }
    if found.is_empty() {
        None
    } else {
        Some(found)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::db::prefetched_competition_lookup::*;
    use crate::ipfs::read::*;
    use crate::mdl::team::Team;
    use crate::wiki::matches::Parms;
    use scraper::Html;
    use std::collections::{BTreeMap, HashMap, HashSet};

    #[tokio::test]
    async fn lock_jaw() {
        let raw = cat("Qmb6KbckoHNyEY3xoTLA4PT2y2QtG8vQK4tU2mdDhdq3PY").await;
        let txt = std::str::from_utf8(raw.as_slice()).expect("It was text when I added it.");
        let html = &Html::parse_document(txt);
        let mut competition_names: HashMap<String, String> = HashMap::new();
        let events = &[
            "ABC Season 1",
            "ABC Season 2",
            "Discovery Season 3",
            "Discovery Season 4",
            "Discovery Season 5",
            "Discovery Season 6",
            "World Championship VII",
            "re:MARS all:STARS BattleBots Challenge (2019)",
            "BattleBots: Bounty Hunters",
            "BattleBots: Champions",
        ];
        for n in events {
            competition_names.insert(n.to_string(), n.replace(" ", "_"));
        }
        let lu = PrefetchedCompetitionLookup::new(competition_names);
        let bot = Bot {
            slug: "Lock-Jaw".to_owned(),
            name: "Lock-Jaw".to_string(),
            updated: time::OffsetDateTime::now_utc(),
            desc: "Lock-Jaw is a heavyweight robot built by Donald Hutson of Mutant Robots, which has competed in every season of the BattleBots reboot.".to_string(),
            team: Team::default(),
            stats: BTreeMap::default(),
            cats: Vec::default(),
            trivia: Vec::default(),
            honors: Vec::default(),
        };
        let aliases = BTreeSet::new();
        let parms = Parms {
            bot_source: &bot,
            html,
            // log: &crate::log::Immediate {},
            aliases: &aliases,
        };
        let result = results_with_subheads(&parms, &lu);
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.len(), 35);
        let mut keys: HashSet<String> = HashSet::default();
        for r in result {
            assert!(keys.insert(r.competition + r.segment.trim() + r.bots.join(" ").as_str()));
        }
    }
}

#[cfg(test)]
mod tests {}
