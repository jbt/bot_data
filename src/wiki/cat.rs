use super::util::*;
use crate::mdl::PageType;
use log::error;
use phf::phf_map;
use scraper::Html;

static CAT2TYPE: phf::Map<&'static str, PageType> = phf_map! {
    "Antweight_Robots" => PageType::Bot,
    "Articles_in_need_of_rewriting" => PageType::Other,
    // "BattleBots_Competitors" => PageType::Bot, BattleBots is in BattleBots_Competitors somehow
    "Awards" => PageType::Other,
    "BattleBots_Merchandise" => PageType::Other,
    "BattleBots_Personalities" => PageType::Human,
    "BattleBots_Spinoff_Series" => PageType::Competition,
    "BattleBots_Teams" => PageType::Team,
    "BattleBox_Hazards" => PageType::Other,
    "Beetleweight_Robots" => PageType::Bot,
    "Blog_posts" => PageType::Other,
    "Browse" => PageType::Other,
    "Competitions" => PageType::Competition,
    "Demonstration_Robots" => PageType::Bot,
    "Dogeweights" => PageType::Bot,
    "Featherweight_Robots" => PageType::Bot,
    "Heavyweight_Robots" => PageType::Bot,
    "Hobbyweight_Robots" => PageType::Bot,
    "Invertible_Robots" => PageType::Bot,
    "International_Dubs" => PageType::Other,
    "Lightweight_Robots" => PageType::Bot,
    "Middleweight_Robots" => PageType::Bot,
    "Policy" => PageType::Other,
    "Robots_armed_with_Hammers/Axes" => PageType::Bot,
    "Robots_armed_with_Spinning_Flywheels" => PageType::Bot,
    "Robots_from_California" => PageType::Bot,
    "Robots_from_Florida" => PageType::Bot,
    "Robots_in_BattleBots_(PS2/GameCube)" => PageType::Other,
    "Robots_in_the_Mechadon_Handheld_Game" => PageType::Other,
    "Robots_that_did_not_compete" => PageType::Bot,
    "Robots_that_fought_on_television" => PageType::Bot,
    "Robots_which_debuted_in_World_Championship_VII" => PageType::Bot,
    "Robots_with_more_Wins_than_Losses" => PageType::Bot,
    "Robots_with_puns_for_names" => PageType::Bot,
    "ShowBots" => PageType::Other,
    "Site_administration" => PageType::Other,
    "Superheavyweight_Robots" => PageType::Bot,
    "Templates" => PageType::Other,
    "Terminology" => PageType::Term,
    "TV_Channels_and_Companies" => PageType::Other,
    "Walkerbots" => PageType::Bot,
    "Weaponless_Robots" => PageType::Bot,
    "Weapons" => PageType::Other,
};

pub fn categories(h: &Html) -> Vec<String> {
    let mut result = vec![];
    if let Some(cat_div) = h.select(&tor("div.page-header__categories")).next() {
        for a in cat_div.select(&tor("a")) {
            if let Some(url) = a.value().attr("href") {
                if url.starts_with("/wiki/Category:") {
                    result.push(url[15..].to_string());
                }
            }
        }
    }
    result
}

pub(crate) fn infer_type(s: &String) -> PageType {
    let h = Html::parse_document(s);
    let cats = categories(&h);
    for c in &cats {
        if let Some(t) = CAT2TYPE.get(c.as_str()) {
            return *t;
        }
    }
    if h.select(&tor("div.redirectMsg > ul.redirectText > li > a"))
        .next()
        .is_some()
    {
        PageType::Alias
    } else if cats.is_empty() {
        PageType::Other
    } else {
        error!("TODO: handle categories: {:?}", cats);
        PageType::Unknown
    }
}

pub(crate) fn bot_cats() -> Vec<&'static str> {
    CAT2TYPE
        .entries()
        .filter_map(|(k, v)| if *v == PageType::Bot { Some(*k) } else { None })
        .collect()
}
