use crate::mdl;

pub mod bot;
pub(crate) mod cat;
mod category;
mod fetcher;
pub mod matches;
pub(crate) mod page;
mod updates;
pub(crate) mod util;

pub type Fetcher = fetcher::Fetcher;
pub type Page = mdl::page::Page;
