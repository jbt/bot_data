use super::util::*;
use crate::db::Worker;
use crate::mdl::PageType;
use log::{debug, error, trace};
use rusqlite::ErrorCode;
use scraper::Html;

pub(crate) fn process(
    cat_slug: &str,
    h: &Html,
    w: &mut Worker,
    _into_table: &str,
    pt: PageType,
) -> Option<()> {
    let genins = "INSERT INTO wiki_pages(slug,page_type,html) VALUES(?1,?2,'')";
    let name = cat_slug[9..].split('?').next().expect("split ?");
    {
        let mut desc = String::default();
        for t in h
            .select(&tor("#mw-content-text > div.mw-parser-output > p"))
            .flat_map(|p| p.text())
        {
            trace!("t={}", &t);
            if let Some(c) = desc.chars().last() {
                if c != ' ' {
                    desc.push(' ');
                }
            }
            if desc.len() + t.len() >= 128 {
                desc.push_str(&t[..128 - desc.len()]);
                break;
            }
            desc.push_str(t);
        }
        let mut stmt = w.conn
            .prepare("INSERT INTO categories ( name, desc ) VALUES ( ?1 , ?2 ) ON CONFLICT ( name ) DO UPDATE SET desc = ?2")
            .expect("upsert failed to prepare");
        while let Err(rusqlite::Error::SqliteFailure(e, l)) = stmt.execute([name, &desc]) {
            error!("Error upserting into categories: {:?} ({:?})",&l,&e);
            if e.code == ErrorCode::DatabaseBusy {
                std::thread::sleep(std::time::Duration::from_secs(1));
            } else {
                return None;
            }
        }
        let mut genstmt = w.conn.prepare(genins).expect(genins);
        for a in h.select(&tor(
            "li.category-page__member > a.category-page__member-link",
        )) {
            let slug = &a.value().attr("href")?[6..];
            if ignored_page(slug) || slug.starts_with("Category:") {
                continue;
            }
            if genstmt.execute((slug, pt)).is_ok() {
                debug!(
                    "Acknowledge the existence of page {} of type {:?}",
                    slug, pt
                );
            }
        }
    }
    Some(())
}
