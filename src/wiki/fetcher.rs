use super::util::*;

use crate::mdl::{page::Page, PageType};

use time::OffsetDateTime;
use tokio::{sync::mpsc, task};

use std::{
    env,
    time::Duration,
};

pub struct Fetcher {
    tx_in: tokio::sync::mpsc::Sender<Page>,
    rx_out: tokio::sync::mpsc::Receiver<Page>,
    // fut: JoinHandle<()>,
}

impl Fetcher {
    pub fn startup() -> Self {
        let (tx_in, rx_in) = mpsc::channel(32);
        let (tx_out, rx_out) = mpsc::channel(32);
        let closure = move || proc(rx_in, tx_out);
        task::spawn(closure());
        // let fut = task::spawn(closure());
        // Self { tx_in, rx_out, fut }
        Self { tx_in, rx_out }
    }
    pub async fn fetch(&mut self, slug: String, typ: PageType) {
        self.tx_in
            .send(Page {
                slug,
                typ,
                html: String::default(),
                updated: OffsetDateTime::UNIX_EPOCH,
            })
            .await
            .expect("Sending side should hang up first.");
    }
    pub fn ready_response(&mut self) -> Option<Page> {
        let waker = futures::task::noop_waker();
        let mut cx = std::task::Context::from_waker(&waker);
        match self.rx_out.poll_recv(&mut cx) {
            std::task::Poll::Ready(x) => x,
            _ => None,
        }
    }
}

async fn proc(mut rx: tokio::sync::mpsc::Receiver<Page>, tx: tokio::sync::mpsc::Sender<Page>) {
    while let Some(t) = rx.recv().await {
        if ignored_page(t.slug.trim()) {
            continue;
        }
        let wiki_domain = env::var("WIKI_DOMAIN").expect("You need to set the env var WIKI_DOMAIN.");
        let outcome = reqwest::get(format!("https://{wiki_domain}/wiki/{}", t.slug)).await;
        match outcome {
            Ok(resp) => {
                let txt = resp.text().await;
                tx.send(t.with_fetched_text(
                    txt.expect("Expected an HTML document to contain valid text"),
                ))
                    .await
                    .unwrap();
            }
            Err(e) => {
                println!("Error fetching {}: {}", t.slug, e);
            }
        }
        tokio::time::sleep(Duration::from_millis(4096)).await;
    }
}
