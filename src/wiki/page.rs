extern crate rand;

use super::{bot, cat, category};

use crate::db::competition::Competition;
use crate::mdl::{Page, PageType};

use log::{info, warn};
use rand::Rng;
use scraper::Html;

use std::collections::HashSet;

pub(crate) fn process(pg: &Page, worker: &mut crate::db::Worker) -> Option<()> {
    let mut known_cats: HashSet<String> = {
        worker
            .conn
            .prepare("SELECT name FROM categories")
            .expect("prep")
            .query_map([], |r| r.get::<usize, String>(0))
            .expect("query")
            .map(|r| r.unwrap())
            .collect()
    };
    let document = Html::parse_document(&pg.html.as_str());
    let mut rng = rand::thread_rng();
    for c in cat::categories(&document) {
        if known_cats.contains(&c) {
            if rng.gen_bool(0.01) {
                let slug = "Category:".to_string() + c.trim();
                worker.enqueue(Page::unfetched(slug, PageType::Category));
                // worker.log_str("refresh cat", 0x2);
            }
        } else {
            // worker.log(format!("Newly-discovered category: {}", c), 0xF1);
            known_cats.insert(c);
        }
    }
    match pg.typ {
        PageType::Updates => crate::wiki::updates::process(&document, worker),
        PageType::Competitions => crate::wiki::category::process(
            pg.slug.as_str(),
            &document,
            worker,
            "competitions",
            PageType::Competition,
        ),
        PageType::Bots => crate::wiki::category::process(
            pg.slug.as_str(),
            &document,
            worker,
            "bots",
            PageType::Bot,
        ),
        PageType::Competition => Some(Competition::from_page(pg).store(&worker.conn)),
        PageType::Bot => {
            if let Ok((b, pi)) = bot::parse(&pg, &worker.conn) {
                crate::db::bot::store(&b, &worker.conn);
                crate::db::bot::record_participation(&pi, &worker.conn, &b.slug);
            }
            Some(())
        }
        PageType::Human => {
            warn!("TODO: Human::process()");
            Some(())
        }
        PageType::Team => {
            warn!("TODO: Team::process()");
            Some(())
        }
        PageType::Teams => {
            warn!("TODO: Teams::process()");
            Some(())
        }
        PageType::Alias => {
            warn!("TODO: Alias::process()");
            Some(())
        }
        PageType::Term => {
            info!("Shall I ever bother to implement Term::process()?");
            Some(())
        }
        PageType::Category => {
            warn!("Processing abstract category: {}", &pg.slug);
            category::process(
                pg.slug.as_str(),
                &document,
                worker,
                "bots",
                PageType::Unknown,
            );
            Some(())
        }
        PageType::Unknown => Some(()), //Silently ignore
        PageType::Other => Some(()),   //Silently ignore
    }
}
