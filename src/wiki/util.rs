use ego_tree::NodeRef;
use scraper::{ElementRef, Node, Selector};

pub(crate) fn tor(s: &str) -> Selector {
    Selector::parse(s).expect("bad selector")
}

pub(crate) fn ignored_page(s: &str) -> bool {
    for meta in ["User_talk:", "User:", "Template:"] {
        if s.starts_with(meta) {
            return true;
        }
    }
    s == "Wiki_Rules"
}

pub(crate) fn after<'a>(e: &ElementRef<'a>, tag: &str) -> Option<ElementRef<'a>> {
    let mut n: NodeRef<Node> = **e;
    while let Some(x) = n.next_sibling() {
        n = x;
        if let Some(e) = ElementRef::wrap(n) {
            // println!("sibling: {}", e.value().name());
            if let Some(result) = or_below(&e, tag) {
                return Some(result);
            }
        }
    }
    None
}

fn or_below<'a>(e: &ElementRef<'a>, tag: &str) -> Option<ElementRef<'a>> {
    if e.value().name() == tag {
        return Some(*e);
    }
    for c in e.children() {
        if let Some(x) = ElementRef::wrap(c) {
            if let Some(result) = or_below(&x, tag) {
                return Some(result);
            }
        }
    }
    None
}

pub(crate) fn one_txt(e: ElementRef) -> &str {
    e.text()
        .map(|t| t.trim())
        .find(|t| !t.is_empty())
        .unwrap_or("")
}

pub(crate) fn child_element<'a>(e: &'a ElementRef, tag: &'static str) -> Option<ElementRef<'a>> {
    e.children()
        .filter_map(|n| ElementRef::wrap(n))
        .find(|e| e.value().name() == tag)
}
