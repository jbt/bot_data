use crate::db::Worker;
use crate::mdl::PageType;
use crate::wiki::{util::tor, Page};
use scraper::{ElementRef, Html};
use time::{format_description::FormatItem, macros::format_description, PrimitiveDateTime};
// use crate::traits::Logger;
use crate::wiki::util::ignored_page;

const DATE_FORMAT: &[FormatItem] = format_description!("[day] [month repr:long] [year]");
const TIME_FORMAT: &[FormatItem] = format_description!("[hour]:[minute]:[second]");

pub(crate) fn process(h: &Html, w: &mut Worker) -> Option<()> {
    let mut log_prio = 0x10;
    for date in h.select(&tor("#mw-content-text > div.mw-changeslist > h4")) {
        let date_str: String = date.text().collect();
        let date_value = time::Date::parse(date_str.trim(), DATE_FORMAT);
        if date_value.is_err() {
            continue;
        }
        let date_value = date_value.unwrap();
        // w.log(format!("Wiki special: changes occurred on date {} aka {}", date_str, date_value), log_prio);
        let mut node = date.next_sibling();
        assert!(node.is_some());
        while node.is_some() {
            if let Some(e) = node.unwrap().value().as_element() {
                // w.log(format!("Wiki special: node following date {}: {}", date_str, e.name()), log_prio);
                if e.name() == "div" {
                    break;
                } else {
                    println!("Skipping element {}", e.name());
                }
            } else {
                // w.log(format!("Wiki special: non-element following date {}", date_str), log_prio);
            }
            node = node.unwrap().next_sibling();
        }
        let div = ElementRef::wrap(node.unwrap()).unwrap();
        for node in div.children() {
            if let Some(change_table) = ElementRef::wrap(node) {
                // w.log(format!("element under div after {}: {}", date_value, change_table.value().name()), log_prio);
                for time in change_table.select(&tor("td.mw-enhanced-rc")) {
                    let time_str = time.text().collect::<String>();
                    // w.log(format!("Wiki special: changes occurred on date {} at time {}", date_value, time_str), log_prio / 2 + 1);
                    if time_str.contains(":") {
                        let time_str = time_str.trim().to_owned() + ":59";
                        let time_value = time::Time::parse(time_str.trim(), TIME_FORMAT);
                        if time_value.is_err() {
                            continue;
                        }
                        let time_value = time_value.unwrap();
                        // println!(" & Time={:?}", time_value);
                        for a in ElementRef::wrap(time.parent().unwrap())
                            .unwrap()
                            .select(&tor("td.mw-changeslist-line-inner > span.mw-title > a"))
                        {
                            let url = a.value().attr("href").unwrap();
                            if url.starts_with("/wiki/") {
                                let slug = &url.trim()[6..];
                                // w.log(format!("According to wiki special, {} was updated at {} T {}", slug, date_value, time_value), log_prio);
                                if log_prio > 1 {
                                    log_prio -= 1;
                                }
                                if ignored_page(slug) {
                                    continue;
                                }
                                let timestamp =
                                    PrimitiveDateTime::new(date_value, time_value).assume_utc();
                                let result = Page {
                                    slug: slug.to_owned(),
                                    typ: PageType::Unknown,
                                    html: "".to_owned(),
                                    updated: timestamp,
                                };
                                w.store_page(&result).ok()?;
                            }
                        }
                        break;
                    }
                }
            } else {
                // w.log(format!("Non-element child node of div after {}", date_value), log_prio);
            }
        }
    }
    Some(())
}
