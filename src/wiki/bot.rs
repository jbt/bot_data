use super::util::*;

use crate::db::Connection;
use crate::mdl::{bot::*, Match};
use crate::traits::*;
use itertools::Itertools;
use std::collections::{BTreeMap, BTreeSet};
use std::ops::Deref;

use crate::mdl::team::Team;
use ego_tree::NodeRef;
use log::{error, info, trace, warn};
use lu::Competition;
use scraper::{ElementRef, Html, Node};

#[derive(Default, Debug)]
pub struct Participation {
    pub season2name: BTreeMap<String, String>,
    pub aliases: BTreeSet<String>,
    pub matches: Vec<Match>,
}

impl Participation {
    //Return something which might pass as the slug of a recently-mentioned competition.
    fn comp_name(&mut self, conn: &Connection, comp: &str, name: &str) -> String {
        use crate::traits::lu::CompRef::*;
        match conn.competitions().unwrap().competition_lookup(comp) {
            One(slug) => {
                self.season2name.insert(slug.clone(), name.to_owned());
                slug //Pretty clear in this case what to return
            }
            Zero => {
                if comp.contains("-") {
                    error!("TODO: handle multi-competition references like {}", &comp);
                } else {
                    error!(
                        "A bot is supposedly known as {} in '{}' but I don't know that competition",
                        name, &comp
                    );
                    self.season2name.insert(comp.to_string(), name.to_owned());
                }
                comp.to_owned() // There is no legit answer, but going through the DB by hand, seeing the name might be helpful finding the shortcomings
            }
            More(first, rest) => {
                self.season2name.insert(first, name.to_owned());
                self.comp_name(conn, rest.trim(), name) // TODO ? Not sure I care, but if the value is used it's... likely to give a less than ideal result.
            }
        }
    }
}

pub fn parse_bot<CompLu: lu::Competition, BotLu: lu::Bots>(
    p: &super::Page,
    comps: &CompLu,
    bots: &BotLu,
) -> rusqlite::Result<Bot> {
    let h = Html::parse_document(p.html.as_str());
    let mut cats = super::cat::categories(&h);
    cats.sort();
    let name = if let Some(h1) = h.select(&tor("#firstHeading")).next() {
        h1.first_child()
            .expect("expected text")
            .value()
            .as_text()
            .expect("should be text")
            .trim()
            .to_string()
    } else {
        String::default()
    };
    let name_allcaps = name.to_uppercase();
    let desc = parse_description(&h, bots);
    trace!("Looking for #Honors in the sidebar of {}", &p.slug);
    let honors = h.select(&tor("#Honors")).next();
    let h2 = honors
        .and_then(|e| e.parent())
        .and_then(|n| ElementRef::wrap(n));
    let table = h2.and_then(|h2| after(&h2, "table"));
    let mut honors = Vec::default();
    if let Some(table) = table {
        let mut honor_comp = String::default();
        for td in table.select(&tor("td")) {
            for t in td.text().filter_map(|t| {
                let t = t.trim();
                if t.is_empty() {
                    None
                } else {
                    Some(t)
                }
            }) {
                if t == name_allcaps {
                    break;
                }
                match comps.competition_lookup(t) {
                    CompRef::One(c) => honor_comp = c,
                    CompRef::Zero => {
                        honors.push(Honor {
                            name: t.to_owned(),
                            competition: honor_comp.clone(),
                        });
                    }
                    CompRef::More(x, y) => {
                        todo!("{:?}/{:?}", x, y)
                    }
                }
            }
        }
    }
    Ok(Bot::new(
        p.slug.clone(),
        name,
        p.updated,
        desc,
        cats,
        honors,
    ))
}

pub fn parse(p: &super::Page, conn: &crate::db::Connection) -> rusqlite::Result<(Bot, Participation)> {
    info!(
        "Parsing {}, which has valid html. Will do a parse of {} bytes of html...",
        &p.slug,
        p.html.len()
    );
    let h = Html::parse_document(p.html.as_str());
    info!(
        "OK, did the raw parse, now iterate through the dom and get stuff...{}",
        &p.slug
    );
    let mut bot = parse_bot(p, &conn.competitions().unwrap(), &conn.bot_lookup())?;
    info!("Got the basic essentials: {:?}", &bot);
    let mut partinf = Participation::default();
    if let Some(quick_fact_sidebar) = h.select(&tor("aside.portable-infobox")).next() {
        for child in quick_fact_sidebar.children() {
            if let Some(e) = ElementRef::wrap(child) {
                if e.value().name() != "section" {
                    continue;
                }
                let section_name: String;
                if let Some(h2) = e.select(&tor("h2")).next() {
                    section_name = h2.text().collect();
                } else {
                    continue;
                }
                match section_name.trim() {
                    "Team Information" => {
                        parse_team_info(&mut bot.team, e);
                    }
                    "Participation Information" => {
                        match parse_participation(&conn, e, &mut partinf) {
                            Ok(()) => (),
                            Err(e) => println!(
                                "ERROR!! Problem parsing participation info for {}: {:?}",
                                p.slug, e
                            ),
                        }
                    }
                    "Robot Statistics" => {
                        for h3 in e.select(&tor("h3.pi-data-label")) {
                            let key = h3.text().collect::<String>();
                            trace!("h3 key: {}", &key);
                            if let Some(Some(up)) = h3.parent().map(|n| ElementRef::wrap(n)) {
                                let mut val = String::default();
                                for val_el in up.select(&tor(".pi-data-value")) {
                                    if !val.is_empty() {
                                        val.push(' ');
                                    }
                                    let mut v = val_el.text().join(" ");
                                    while let Some(left) = v.find('(') {
                                        if let Some(right) = v[left..].find(')') {
                                            v.replace_range(left..left + right + 1, "...");
                                        } else {
                                            break;
                                        }
                                    }
                                    while let Some(spaces) = v.find("  ") {
                                        v.replace_range(spaces..spaces + 2, " ");
                                    }
                                    val.push_str(v.trim());
                                }
                                bot.stats.insert(key, val);
                            }
                        }
                    }
                    _ => println!(
                        "Bot {} has unhandled sidebar section {}",
                        p.slug, section_name
                    ),
                }
            }
        }
    } else {
        // w.log(format!("{} appears to have no quick-facts right-sidebar", p.slug), 9);
    }
    info!(
        "Parsed the quick facts sidebar, now time for trivia. {} {:?}",
        &p.slug, &bot
    );
    if let Some(trivia_header) = h.select(&tor("#Trivia")).next() {
        get_trivia(&mut bot, trivia_header.deref().clone());
    }
    info!("Got trivia, now matches. {} {:?}", &p.slug, &bot);
    let lu = conn
        .competitions()
        .ok_or(rusqlite::Error::QueryReturnedNoRows)?;
    partinf.matches = crate::wiki::matches::extract_from_page(p, &bot, &lu, &partinf.aliases);
    info!("Got {} matches for {}", partinf.matches.len(), &bot.slug);
    Ok((bot, partinf))
}

fn parse_team_info(team: &mut Team, e: ElementRef) {
    // bot.team.slug = get_team_slug(e.select()).unwrap_or(String::default());
    for h3 in e.select(&tor("h3.pi-data-label")) {
        match one_txt(h3) {
            "Team" => {
                if let Some(a) = after(&h3, "a") {
                    let url = a.value().attr("href").expect("a without href");
                    if url.starts_with("/wiki/") {
                        team.slug = url[6..].to_string();
                        team.name = a.text().collect::<String>();
                    }
                }
            }
            "Team Members" => {
                if let Some(div) = after(&h3, "div") {
                    for t in div.text() {
                        let t = t.trim();
                        if t.is_empty() || t.contains("(") || t.contains(")") || t.contains('[') {
                            continue;
                        }
                        team.members.push(t.to_string());
                    }
                }
            }
            x => warn!("Unhandled team info section {x}"),
        }
    }
}

fn text(e: ElementRef) -> &str {
    e.text()
        .map(|t| t.trim())
        .find(|t| t.len() > 0)
        .unwrap_or("")
}

//"h2[data-source=\"robot_name\"]"
fn parse_participation(
    conn: &Connection,
    panel: ElementRef,
    out: &mut Participation,
) -> Result<(), String> {
    let mut name = "";
    for h2 in ElementRef::wrap(panel.parent().unwrap())
        .unwrap()
        .select(&tor("h2[data-source=\"robot_name\"]"))
    {
        name = text(h2);
        break;
    }
    assert_ne!(name, ""); //robot_name should be there
    let label_tor = tor("h3.pi-data-label");
    let labels = panel.select(&label_tor);
    for label in labels {
        match text(label) {
            "Other Names" => {
                if let Some(div) = after(&label, "div") {
                    let mut prev: Option<&str> = None;
                    for t in div.text() {
                        if t.is_empty() || t.trim().starts_with("[") {
                            continue;
                        }
                        let n = t.trim();
                        if n.starts_with("(") && n.len() > 2 {
                            if !n.contains("Not ")
                                && !n.contains("orking")
                                && !n.contains("To be")
                                && !n.contains("riginal")
                            {
                                let nm = prev.ok_or_else(|| {
                                    "How did we get a parenthetical without a name?".to_owned() + t
                                })?;
                                out.comp_name(conn, &n[1..n.len() - 1], nm.into());
                            }
                            continue;
                        }
                        out.aliases.insert(n.to_string());
                        prev = Some(n);
                    }
                }
            }
            "1999 Events" => note_seasons(conn, after(&label, "div"), name, out),
            "BotsIQ" => note_seasons(conn, after(&label, "div"), name, out),
            "Comedy Central Seasons" => note_seasons(conn, after(&label, "div"), name, out),
            "Reboot Seasons" => note_seasons(conn, after(&label, "div"), name, out),
            "Live" => note_seasons(conn, after(&label, "div"), name, out),
            "Other Events" => note_seasons(conn, after(&label, "div"), name, out),
            "Unaired Events" => note_seasons(conn, after(&label, "div"), name, out),
            x => error!("TODO: handle pariticipation section {}", x),
        }
    }
    Ok(())
}

fn note_seasons(conn: &Connection, div: Option<ElementRef>, name: &str, out: &mut Participation) {
    let div = div.expect("Couldn't find a div, eh?");
    let mut last = String::new();
    let mut in_aka_paren: Option<String> = None;
    for t in div.text().map(|t| t.trim()).filter(|t| !t.is_empty()) {
        if let Some(mut so_far) = in_aka_paren {
            so_far.push_str(t.trim());
            if so_far.contains(")") {
                out.season2name.insert(
                    last.to_owned(),
                    so_far.split(")").next().expect("Weird parens").to_owned(),
                );
                in_aka_paren = None;
            } else {
                in_aka_paren = Some(so_far);
            }
            continue;
        }
        if t.contains("ithdrew")
            || t.contains("(reserve)")
            || t.contains("ot selected")
            || t.contains("lternate")
            || t.contains("ending")
            || t.contains("id not comp")
            || t.contains("ejected")
            || t.contains("ot Accepted")
            || t.contains("eserve")
            || t.contains("amera")
            || t.contains("Not entered")
            || t.contains("No Show")
            || t.contains("Did Not Compete")
            || t.contains("isqualified")
            || t.contains("Forfeited")
            || t.contains("Forefeited")
        {
            out.season2name.remove(&last);
        } else if t == "(as" {
            in_aka_paren = Some(String::new());
        } else if t.starts_with("(as ") && t.ends_with(")") {
            let l = t.len() - 1;
            out.season2name.insert(last.to_owned(), t[4..l].to_owned());
        } else if t.contains("entered as")
            || t.contains("vs")
            || t.contains("name")
            || t.contains("only")
            || t.contains("division")
            || t.contains("Unfinished")
            || t.contains("egistered")
            || t.contains("eight")
            || t.contains("Exhibition")
        {
            ()
        } else if t.starts_with("(") {
            assert_eq!(
                name.to_owned() + ":" + t,
                name.to_owned() + ":(immatterial explanation)"
            );
        } else if !out.season2name.contains_key(t) {
            last = out.comp_name(conn, t, name);
        }
    }
}

fn get_trivia(bot: &mut Bot, from: NodeRef<Node>) {
    let mut prv = from.clone();
    while let Some(n) = prv.next_sibling() {
        if let Some(e) = ElementRef::wrap(n) {
            if e.value().name() == "ul" {
                for li in e.select(&tor("li")) {
                    bot.trivia.push(li.text().join(" "));
                }
                return;
            }
        }
        prv = n;
    }
    if let Some(p) = from.parent() {
        get_trivia(bot, p.clone());
    }
}


fn parse_description<BotLu: lu::Bots>(h: &Html, _bots: &BotLu) -> String {
    let mut desc = String::default();
    if let Some(body) = h.select(&tor("#mw-content-text > div.mw-parser-output")).next() {
        for n in body.children() {
            if let Some(el) = ElementRef::wrap(n) {
                // eprintln!("id={:?}", el.value().id());
                if el.value().id() == Some("toc") {
                    break;
                }
                if el.value().name() == "p" {
                    desc.push_str(&el.text().map(|t| t.trim().to_string() + " ").collect::<String>());
                }
            }
        }
    };
    desc = desc.split('.').map(sanitize).join(".");
    desc
}

fn sanitize<'a>(sent: &'a str) -> &'a str {
    let mut sent = sent;
    for drop_after in ["does not", "opposing robot's"] {
        if let Some(pos) = sent.find(drop_after) {
            sent = &sent[0..pos];
        }
    }
    if let Some(named) = sent.find(" named") {
        if let Some(a) = sent.find(" a ") {
            if a < named {
                sent = &sent[0..a];
            }
        }
    }
    sent
}

#[cfg(test)]
mod tests {
    // use time::OffsetDateTime;
    // use super::super::page::*;
    use super::*;
    use crate::ipfs::read::*;
    // use crate::mdl::*;

    struct NoComps {}

    impl lu::Competition for NoComps {
        fn competition_lookup(&self, _name: &str) -> CompRef {
            CompRef::Zero
        }
    }

    struct NoBots {}

    impl lu::Bots for NoBots {
        fn is_bot(&self, _slug: &str) -> bool {
            false
        }
    }

    #[tokio::test]
    async fn defender_has_no_spinning() {
        let raw = cat("QmfQnbUh811fdozyRFyZ5YBuGpi4DAGZkAcEqSugmUB88G").await;
        let txt = std::str::from_utf8(raw.as_slice()).expect("It was text when I added it.");
        let html = Html::parse_document(txt);
        let desc = parse_description(&html, &NoBots {});
        assert!(desc.contains("lifting"));
        assert!(!desc.contains("spinning"));
    }

    #[tokio::test]
    async fn witch_doctor_is_not_a_shell_spinner() {
        let raw = cat("QmeXuW6G54SooBvDQaPVQVCXhiGDFWa36PKdWto1o9p9jp").await;
        let txt = std::str::from_utf8(raw.as_slice()).expect("It was text when I added it.");
        let html = Html::parse_document(txt);
        let desc = parse_description(&html, &NoBots {});
        assert!(desc.contains("Gellatly"), "{}", desc);
        assert!(!desc.contains("shell spinner"), "{}", desc);
    }

    #[tokio::test]
    async fn tombstone_has_no_wedge() {
        let raw = cat("QmebiZu5JtjRDcUVV2TZatiohgrVmy4i76V1quG23XoPkf").await;
        let txt = std::str::from_utf8(raw.as_slice()).expect("It was text when I added it.");
        let html = Html::parse_document(txt);
        let desc = parse_description(&html, &NoBots {});
        assert!(desc.contains("Last Rites"), "{}", desc);
        assert!(!desc.contains("wedge"), "{}", desc);
    }
}
