use crate::db::{Connection, matches, tagger::Tagger};
use crate::mdl::{Match};

use log::{debug, error, info, trace};
use petgraph::{dot::Dot, Directed, graphmap::GraphMap};
use priority_queue::PriorityQueue;

use std::{
    collections::hash_map::{DefaultHasher, HashMap},
    fmt::Formatter,
    fs::File,
    io::Write,
    hash::Hasher,
    ops::Mul,
};

type BotNameHash = u64;

#[derive(Debug)]
struct Edge {
    tag_weight: f64,
    matches: Vec<Match>,
}

impl Edge {
    fn weight(&self) -> f64 {
        let not_tag = 1. - self.tag_weight;
        let not = not_tag * 0.66f64.powf(self.matches.len() as f64);
        1. - not
    }
    fn display_label(&self) -> String {
        if self.matches.len() == 1 {
            self.matches[0].competition.clone()
        } else if self.matches.len() > 1 {
            format!("{} wins", self.matches.len())
        } else if self.tag_weight > 0.01 {
            format!("{}% similar", self.tag_weight.mul(100.) as u8)
        } else if self.tag_weight > 0.001 {
            format!("{}.{}% similar", self.tag_weight.mul(100.) as u8, self.tag_weight.mul(1000.) as u8 % 10)
        } else {
            String::default()
        }
    }
}

impl std::fmt::Display for Edge {
    fn fmt(&self, _f: &mut Formatter<'_>) -> std::fmt::Result {
        // f.write_str("TODO: edge description for graph");
        Ok(())
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Polarity {
    Strong,
    Weak,
}

#[derive(Hash, PartialEq, Eq, Ord, PartialOrd)]
pub enum ActionType {
    ExpandByMatches,
    ExpandByTags,
}

#[derive(Hash, PartialEq, Eq, Ord, PartialOrd)]
struct Action {
    typ: ActionType,
    bot: BotNameHash,
}

pub struct BotGraph {
    graph: GraphMap<BotNameHash, Edge, Directed>,
    todo: PriorityQueue<Action, u8>,
    root: BotNameHash,
    polarity: Polarity,
    hash2name: HashMap<BotNameHash, String>,
}

impl BotGraph {
    pub fn new(root: String, polarity: Polarity) -> Self {
        let root_hash = hash_bot(&root);
        let mut hash2name = HashMap::default();
        hash2name.insert(root_hash, root);
        let mut todo = PriorityQueue::default();
        todo.push(Action { typ: ActionType::ExpandByMatches, bot: root_hash }, 255);
        todo.push(Action { typ: ActionType::ExpandByTags, bot: root_hash }, 254);
        Self {
            graph: GraphMap::default(),
            todo,
            root: root_hash,
            polarity,
            hash2name,
        }
    }
    pub fn expand(&mut self, db: &Connection, match_lookup: &mut matches::CachedLookup, tagger: &mut Tagger) -> u8 {
        info!("Expanding graph for {:?}/{:?} beyond {} bots.", self.hash2name.get(&self.root), self.polarity, self.hash2name.len());
        match self.todo.pop() {
            Some((Action { typ: ActionType::ExpandByMatches, bot: b }, w)) => {
                self.add_matches(b, w as f64 / 256., db, match_lookup);
                w
            }
            Some((Action { typ: ActionType::ExpandByTags, bot: b }, w)) => {
                self.add_tags(b, w, tagger, db);
                w
            }
            None => {
                trace!("Done expanding.");
                0
            }
        }
    }
    pub fn size(&self) -> usize {
        self.hash2name.len()
    }
    pub fn next_priority(&self) -> Option<u8> {
        self.todo.peek().map(|top| top.1.clone())
    }
    pub fn membership(&self, into: &mut HashMap<String, f64>) -> f64 {
        let tb = TraceBack { here: BotNameHash::default(), prev: None };
        into.insert( self.hash2name.get(&self.root).unwrap().clone(), 1. );
        self.find_members(self.root, 1., into, &tb)
    }
    pub fn draw(&self, file_name: &str) {
        let lu_node = |_, (idx, _)| format!("label=\"{}\"", &self.hash2name.get(&idx).expect("hash not found"));
        let lu_edge = |_, (_, _, edge)| {
            let edge: &Edge = edge;
            format!("label=\"{}\"", edge.display_label())
        };
        let dot = Dot::with_attr_getters(&self.graph, &[], &lu_edge, &lu_node);
        let text = format!("{}", dot);
        File::create(file_name).unwrap().write_all(text.as_bytes()).expect("Failed to write to .dot file.");
    }

    fn add_tags(&mut self, bot: BotNameHash, todo_prio: u8, tagger: &mut Tagger, db: &Connection) {
        let name = self.hash2name.get(&bot).expect("hash not found");
        trace!("add_tags({name})");
        let prio: f64 = todo_prio.into();
        if let Some(neighbors) = tagger.neighborhood(name.clone(), db) {
            for (similarity, rhs_name, _, _, _, _) in neighbors.into_iter().take(9) {
                let (rhs, is_new) = self.get(&rhs_name);
                let mut weight = prio / 256.;
                if let Some(edge) = self.graph.edge_weight_mut(bot, rhs) {
                    edge.tag_weight = similarity;
                    weight *= edge.weight();
                } else {
                    let edge = Edge {
                        tag_weight: similarity,
                        matches: vec![],
                    };
                    trace!("Add edge from {bot}->{rhs} due to similarity");
                    weight *= edge.weight();
                    self.graph.add_edge(bot, rhs, edge);
                }
                if is_new {
                    debug!("Considering {rhs_name} ({rhs}) due to tag similarity. Will consider expanding upon it later.");
                    self.add_todo(rhs, weight * similarity);
                }
                if similarity < 0.011 {
                    break;
                }
            }
        }
    }
    fn add_matches(&mut self, idx: BotNameHash, p: f64, db: &Connection, match_lookup: &mut matches::CachedLookup) {
        let bot = &self.hash2name.get(&idx).expect("hashnotfound");
        trace!("add_matches({bot})");
        let matches = match_lookup.lookup(bot, db);
        if let Some(matches) = matches {
            let matches = match self.polarity {
                Polarity::Weak => &matches.loss,
                Polarity::Strong => &matches.wins,
            };
            for (opp, multiple) in matches {
                let (opp_idx, was_new) = self.get(&opp);
                let mut todo_weight = p;
                if let Some(edge) = self.graph.edge_weight_mut(idx, opp_idx) {
                    for match_ in multiple {
                        edge.matches.push(match_.clone());
                    }
                    todo_weight *= edge.weight();
                } else {
                    let edge = Edge {
                        tag_weight: 0.,
                        matches: multiple.iter().map(|m| m.clone()).collect(),
                    };
                    debug!("Add edge from {idx}->{opp_idx}");
                    todo_weight *= edge.weight();
                    self.graph.add_edge(idx, opp_idx, edge);
                }
                if was_new {
                    self.add_todo(opp_idx, todo_weight);
                }
            }
        }
    }
    fn get(&mut self, bot: &str) -> (BotNameHash, bool) {
        let idx = hash_bot(bot);
        if self.graph.contains_node(idx) {
            (idx, false)
        } else {
            self.graph.add_node(idx);
            self.hash2name.insert(idx, bot.to_owned());
            (idx, true)
        }
    }
    fn add_todo(&mut self, bot: BotNameHash, weight: f64) {
        self.todo.push(Action { typ: ActionType::ExpandByMatches, bot }, (weight * 255.).round() as u8);
        self.todo.push(Action { typ: ActionType::ExpandByTags, bot }, (weight * 85.).round() as u8);
    }
    fn find_members(&self, n: BotNameHash, w: f64, into: &mut HashMap<String, f64>, ptb: &TraceBack) -> f64 {
        if ptb.cycled(n) {
            debug!("Found a cycle on {n}");
            return 0.;
        }
        let tb = TraceBack { here: n, prev: Some(ptb) };
        let mut sum = 0.;
        for (from, to, edge) in self.graph.edges(n) {
            assert_eq!(from, n);
            let w2 = w * edge.weight();
            if w2 < 1e-5 {
            }
            else if let Some(bot) = self.hash2name.get(&n) {
                if let Some(k) = into.get_mut(bot) {
                    debug!("Increasing {bot} by {w2}");
                    *k = 1. - (1. - *k) * (1. - w2);
                } else {
                    debug!("Found {bot} with weight {w2}");
                    into.insert(bot.clone(), w2);
                }
                sum += w2;
                sum += self.find_members(to, w2, into, &tb);
            } else {
                error!("{n} not found in hash2name!!");
            }
        }
        sum
    }
}

fn hash_bot(slug: &str) -> BotNameHash {
    let mut hasher = DefaultHasher::default();
    hasher.write(slug.as_bytes());
    hasher.finish()
}

struct TraceBack<'a> {
    here: BotNameHash,
    prev: Option<&'a TraceBack<'a>>,
}

impl<'a> TraceBack<'a> {
    fn cycled(&self, b: BotNameHash) -> bool {
        if self.here == b {
            true
        } else if let Some(p) = self.prev {
            p.cycled(b)
        } else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn appropriate_search() {
        use petgraph::*;
        use petgraph::prelude::*;
        let mut graph: Graph::<(), ()> = Graph::default();

        graph.extend_with_edges(&[
            (0, 1), (0, 2), (1, 3), (2, 3), (3, 1)
        ]);

        let mut x = Bfs::new(&graph, 0.into());
        while let Some(visited) = x.next(&graph) {
            print!(" {}", visited.index());
        }
    }

    #[test]
    fn hash_rusty() {
        assert_eq!(708429571129657925, hash_bot(&"Rusty"));
    }
}
