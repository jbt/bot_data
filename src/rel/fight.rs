use super::graph::{BotGraph, Polarity};

use crate::{
    db::{Connection, matches, tagger::Tagger},
    error::{
        GenErr::Inconclusive,
        Result,
    },
    prio_log,
};

use log::{warn, debug, info};

use std::{
    collections::HashMap,
    ops::Sub,
};

pub fn guess(bot_a: &str, bot_b: &str, db: &Connection, match_lookup: &mut matches::CachedLookup, tagger: &mut Tagger) -> Result<f64> {
    let mut graphs = [
        BotGraph::new(bot_a.to_owned(), Polarity::Strong),
        BotGraph::new(bot_a.to_owned(), Polarity::Weak),
        BotGraph::new(bot_b.to_owned(), Polarity::Strong),
        BotGraph::new(bot_b.to_owned(), Polarity::Weak),
    ];
    const ASTR: usize = 0;
    const AWEK: usize = 1;
    const BSTR: usize = 2;
    const BWEK: usize = 3;
    let mut numer: f64 = 0.;
    let mut denom = 0.;
    let mut scratch = [HashMap::default(), HashMap::default()];
    for round in 0..99 {
        if prio_log::LOGGER.check() {
            prio_log::LOGGER.check();
        } else {
            db.save_log_priorities();
        }
        //Each graph should expand at least a little bit in each round
        for graph in graphs.iter_mut() {
            if graph.expand(db, match_lookup, tagger) > 9 {
                graph.expand(db, match_lookup, tagger);
            }
        }
        //The one with the most impactful things to do should do more stuff.
        graphs.iter_mut()
            .max_by_key(|g| g.next_priority())
            .expect("[_;4] can't be empty.")
            .expand(db, match_lookup, tagger);
        let scores = [
            score_on(bot_a, &graphs[ASTR], &graphs[BWEK], &mut scratch),
            score_on(bot_b, &graphs[BSTR], &graphs[AWEK], &mut scratch),
        ];
        debug!("Scores: {:?}",&scores);
        //let mut real_data = if scores[0] < 1e-9 {
          //  info!("Round {round} : No bot ({bot_a}) should be completely unable to score against another bot: {:?}", &scores);
            // graphs[ASTR].expand(db, match_lookup, tagger);
            // graphs[BWEK].expand(db, match_lookup, tagger);
            //false
//        } else {
  //          true
    //    };
        let pct: f64 = 100. * scores[0] / (scores.iter().sum::<f64>() + 1e-9);
        if scores[0] < 1e-9 || scores[1] < 1e-9 {
            info!("Round {round} : No bot should be completely unable to score against another bot: {:?}", &scores);
            println!("Round {round} : {bot_a} vs {bot_b} {pct}% ({}-{}, skipping... )", scores[0], scores[1]);
            // graphs[BSTR].expand(db, match_lookup, tagger);
            // graphs[AWEK].expand(db, match_lookup, tagger);
            //real_data = false;
            continue;
        }
        //if scores.iter().all(|f| *f < 1e-9) {
        //    info!("Round {round} : Not Enough Info yet. Scores this round: {:?}", &scores);
        //    continue;
        //}
        numer += pct;
        denom += 1.;
        let cum = numer / denom;
        println!("Round {round} : {bot_a} vs {bot_b} {pct}% ({}-{}, cumulative: {cum}% )", scores[0], scores[1]);
        // if pct > 51. && cum < 49. || pct < 49. && cum > 51. {
        //     return Err(Inconclusive(bot_a.to_owned(), bot_b.to_owned(), "scope-dependent".to_owned()));
        // }
        if scores.iter().sum::<f64>() > 5. && pct.sub(cum).abs() < 1.3 * round as f64 {
            break;
        }
    }
    let pct = numer / denom;
    if pct > 49. && pct < 51. {
        Err(Inconclusive(bot_a.into(), bot_b.into(), "Too close to call".into()))
    } else {
        Ok(pct)
    }
}

fn score_on(score_for: &str, strong: &BotGraph, weak: &BotGraph, scratch: &mut [HashMap<String, f64>]) -> f64 {
    for s in scratch.iter_mut() {
        for (_, v) in s {
            *v = 0.;
        }
    }
    strong.membership(&mut scratch[0]);
    weak.membership(&mut scratch[1]);
    let strong = &scratch[0];
    let weak = &scratch[1];
    let mut result = 0.;
    for (bot, weight) in strong {
        //if *weight < 1e-9 {
          //  continue;
//        }
        warn!("{score_for} strong against {bot} ({weight})");
        if let Some(w2) = weak.get(bot) {
            let w = w2 * *weight;
            result += w;
            if w > 1e-9 {
                info!("{score_for} scores on {bot}: {w} -> {result}");
            }
        }
    }
    result
}
