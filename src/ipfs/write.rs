use ipfs_api_backend_hyper::*;
use std::io::Cursor;

#[allow(dead_code)]
pub async fn add(data: Vec<u8>, pin: bool) -> Result<String, Error> {
    let api_client = IpfsClient::default();
    let curse = Cursor::new(data);
    let hash = api_client.add(curse).await?.hash;
    if !pin {
        Ok(hash)
    } else if let Err(e) = api_client.pin_add(hash.as_str(), false).await {
        if let Error::Api(ApiError { message, code }) = e {
            if code == 0 {
                return Ok(hash);
            } else {
                Err(Error::Api(ApiError { message, code }))
            }
        } else {
            Err(e)
        }
    } else {
        Ok(hash)
    }
}
