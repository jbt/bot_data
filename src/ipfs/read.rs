use dirs;
use futures::TryStreamExt;
use ipfs_api_backend_hyper::*;
use multiaddr::{Multiaddr, Protocol};
use std::fs;
use std::path::{Path, PathBuf};
use std::string::String;

pub async fn down(cid: &str, save_to: &Path) -> bool {
    let data = cat(cid).await;
    if data.is_empty() {
        false
    } else {
        fs::write(save_to, data).is_ok()
    }
}

pub async fn cat(cid: &str) -> Vec<u8> {
    //TODO consider cycling through options async
    let mut path = dirs::home_dir().unwrap();
    path.push(".ipfs");
    if let Some(mut url) = from_client_config(path.clone()) {
        let result = get(&mut url, cid).await;
        if !result.is_empty() {
            return result;
        }
    }
    if let Some(mut url) = from_node_config(path) {
        let result = get(&mut url, cid).await;
        if !result.is_empty() {
            return result;
        }
    }
    for url in ["http://localhost:8080", "https://ipfs.io"] {
        let result = get(&mut url.to_string(), cid).await;
        if !result.is_empty() {
            return result;
        }
    }
    let api_client = IpfsClient::default();
    println!("Trying via API");
    if let Ok(res) = api_client
        .cat(cid)
        .map_ok(|c| c.to_vec())
        .try_concat()
        .await
    {
        res
    } else {
        println!("Couldn't download {}.", &cid);
        Vec::new()
    }
}

async fn get(url: &mut String, cid: &str) -> Vec<u8> {
    url.push_str("/ipfs/");
    url.push_str(cid);
    println!("Attempting {}", url.as_str());
    if let Ok(resp) = reqwest::get(url.as_str()).await {
        if let Ok(txt) = resp.bytes().await {
            if let Ok(str) = std::str::from_utf8(&txt) {
                if str.contains("<title>504 Gateway Time-out</title>") {
                    println!("Gateway {} couldn't find {}", url, cid);
                    return Vec::new();
                }
            }
            if txt.len() > 0 {
                txt.to_vec()
            } else {
                Vec::new()
            }
        } else {
            Vec::new()
        }
    } else {
        Vec::new()
    }
}

fn from_client_config(mut path: PathBuf) -> Option<String> {
    path.push("gateway");
    let ma = std::fs::read_to_string(path.as_path()).ok()?;
    println!("{:?} contained {}", &path, ma);
    match Multiaddr::try_from(ma.trim()) {
        Ok(ma) => multiaddress_to_url(&ma),
        Err(e) => {
            println!("{} is not a valid multiaddr: {}", ma, e);
            None
        }
    }
}

fn from_node_config(mut d: PathBuf) -> Option<std::string::String> {
    d.push("config");
    let cfg = std::fs::read_to_string(d.as_path()).ok()?;
    let cfg = json::parse(cfg.trim()).ok()?;
    let addrs = &cfg["Addresses"];
    if !addrs.is_object() {
        return None;
    }
    let gateway = &addrs["Gateway"];
    if !gateway.is_string() {
        return None;
    }
    let ma = Multiaddr::try_from(gateway.as_str()?).ok()?;
    multiaddress_to_url(&ma)
}

fn multiaddress_to_url(ma: &Multiaddr) -> Option<String> {
    let mut proto = "http".to_owned();
    let mut host = String::default();
    let mut port = 0u16;
    for protocol in ma.iter() {
        match protocol {
            Protocol::Ip6(ip) => {
                host = ip.to_string();
            }
            Protocol::Ip4(ip) => {
                host = ip.to_string();
            }
            Protocol::Dns(dns) => {
                host = dns.to_string();
            }
            Protocol::Dns4(dns) => {
                host = dns.to_string();
            }
            Protocol::Dns6(dns) => {
                host = dns.to_string();
            }
            Protocol::Dnsaddr(dns) => {
                host = dns.to_string();
            }
            Protocol::Tcp(p) => {
                port = p;
            }
            Protocol::Http => {
                proto = "http".to_string();
            }
            Protocol::Https => {
                proto = "https".to_string();
            }
            _ => {}
        }
    }
    if port > 0 && !proto.is_empty() && !host.is_empty() {
        let result = format!("{}://{}:{}", proto, host, port);
        println!("{}->{}", ma, result);
        Some(result)
    } else {
        println!("{} has no obvious URL equivalent", ma);
        None
    }
}
