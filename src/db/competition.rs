use super::Connection;
use crate::wiki;
use crate::wiki::util::{after, one_txt, tor};
use chrono::NaiveDate;
use scraper::Html;
use std::collections::HashMap;
use time::{Date, Duration, OffsetDateTime};

pub struct Competition {
    slug: String,
    name: String,
    updated: OffsetDateTime,
    when: Option<[chrono::NaiveDate; 2]>,
}

impl Competition {
    pub(crate) fn from_page(pg: &wiki::Page) -> Self {
        let h = Html::parse_document(pg.html.as_str());

        let name = if let Some(heading) = h.select(&tor("#firstHeading")).next() {
            heading.text().collect()
        } else {
            println!(
                "The competition page {} has no heading in which to find the name‽",
                &pg
            );
            String::default()
        };
        return Self {
            slug: pg.slug.clone(),
            name: name.trim().to_string(),
            updated: pg.updated + Duration::MINUTE,
            when: find_dates(&h),
        };
    }
    pub(crate) fn store(&self, conn: &Connection) {
        conn.execute("INSERT INTO competitions(slug,name,updated) VALUES(?1,?2,?3) ON CONFLICT(slug) DO UPDATE SET updated = MAX(updated, excluded.updated), name = MAX(name, excluded.name)", (&self.slug, &self.name, self.updated)).expect("insert failed");
        if let Some(when) = self.when {
            conn.execute(
                "UPDATE competitions SET start_date = ?1 WHERE slug = ?2",
                (when[0], &self.slug),
            )
                .expect("Can't do an update to set dates?");
        };
    }
    pub fn names(conn: &Connection) -> rusqlite::Result<HashMap<String, String>> {
        let mut direct = conn.prepare("SELECT name, slug FROM competitions UNION SELECT a.alias as name, c.slug as slug FROM aliases a INNER JOIN competitions c ON a.canon_slug = c.slug")?;
        let mut result: HashMap<String, String> = HashMap::new();
        for (k, v) in direct
            .query_map([], |r| {
                Ok((
                    r.get::<&str, String>("name")?,
                    r.get::<&str, String>("slug")?,
                ))
            })?
            .filter_map(|r| r.ok())
        {
            result.insert(k.clone(), v.clone());
            let toks = k.split(" ");
            if toks.last().unwrap().parse::<u16>().is_ok() {
                let mut toks: Vec<&str> = k.split(" ").collect();
                let number = toks.remove(toks.len() - 1);
                toks.insert(0, number);
                let alias = toks.join(" ");
                // println!("Adding {} as an auto-alias of {}", &alias, &k);
                result.insert(alias, v);
            }
        }
        Ok(result)
    }
}

fn find_dates(h: &Html) -> Option<[NaiveDate; 2]> {
    for label in h.select(&tor(".pi-data-label")) {
        if one_txt(label) == "Filming Dates" {
            if let Some(div) = after(&label, "div") {
                return parse_dates(one_txt(div));
            }
        }
    }
    None
}

fn parse_dates(rep: &str) -> Option<[NaiveDate; 2]> {
    let mut by_hyphen = rep.split("-").map(|s| s.trim().to_owned());
    let start = by_hyphen.next();
    let fin = by_hyphen.next();
    if start.is_none() || fin.is_none() || by_hyphen.next().is_some() {
        return None;
    }
    let mut start = start.unwrap();
    let fin = fin.unwrap();
    if fin.contains(",") && !start.contains(",") {
        if let Some(comma) = fin.find(",") {
            let year = fin[comma..].to_string();
            start.push_str(year.as_str());
        }
    }
    let format =
        time::format_description::parse("[month repr:long] [day padding:none], [year]").ok()?;
    let start = Date::parse(start.as_str(), &format).ok()?;
    let finish = Date::parse(fin.as_str(), &format).ok()?;
    Some([naivify(start), naivify(finish)])
}

fn naivify(d: Date) -> NaiveDate {
    let ord: u8 = d.month().into();
    NaiveDate::from_ymd_opt(d.year(), ord.into(), d.day().into())
        .expect("It is a prerequisite of naivify that the argument must already be a valid date.")
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ipfs::read::*;
    use scraper::Html;

    #[tokio::test]
    async fn discovery_season_6_dates() {
        let when = parse_dates("August 25 - September 5, 2021 ");
        assert!(when.is_some());
        let when = when.unwrap();
        assert_eq!(when[0], NaiveDate::from_ymd_opt(2021, 8, 25).unwrap());
        assert_eq!(when[1], NaiveDate::from_ymd_opt(2021, 9, 5).unwrap());
    }

    #[tokio::test]
    async fn discovery_season_6_dates_from_html() {
        let raw = cat("QmcnkXMarD4ysFYG7SRqxWqDBRLYfJdiGmXGWB4LEiQQJk").await;
        let txt = std::str::from_utf8(raw.as_slice()).expect("It was text when I added it.");
        let html = &Html::parse_document(txt);
        let when = find_dates(&html);
        assert!(when.is_some());
        let when = when.unwrap();
        assert_eq!(when[0], NaiveDate::from_ymd_opt(2021, 8, 25).unwrap());
        assert_eq!(when[1], NaiveDate::from_ymd_opt(2021, 9, 5).unwrap());
    }
}
