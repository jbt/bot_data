use crate::mdl::PageType;
use crate::wiki;

pub(crate) fn row2page(row: &rusqlite::Row) -> rusqlite::Result<wiki::Page> {
    let html: String = row.get("html")?;
    let typ = row.get("page_type").unwrap_or(PageType::Bot);
    Ok(wiki::Page {
        slug: row.get("slug")?,
        updated: row.get("updated")?,
        html,
        typ,
    })
}
