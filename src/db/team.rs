use super::Connection;

use crate::mdl::team::*;

use log::{info, trace, warn};
use rusqlite::Result;

pub fn store(conn: &Connection, team: &Team) -> Result<usize> {
    let slug = if team.slug.is_empty() {
        team.name.replace(' ', "_")
    } else {
        team.slug.clone()
    };
    warn!("TODO: wrap in a db transaction so we don't get a team half-saved");
    let mut ins_mem = conn.prepare(
        "INSERT INTO team_membership (team_slug,incarnation,member_name) VALUES ( ?1 , ?2 , ?3 )",
    )?;
    let rmed = conn.execute(
        "DELETE FROM team_membership WHERE team_slug = ?1 AND incarnation = ?2",
        (&slug, &team.incarnation),
    )?;
    info!("Removed {rmed} members from {}/{}", &team.slug, &team.incarnation);
    for mem in &team.members {
        trace!(
            "Inserting team_membership: {}/{} <- {}",
            &slug,
            &team.incarnation,
            &mem
        );
        ins_mem.execute([&slug, &team.incarnation, mem]).ok();
    }
    conn.execute(
        "INSERT INTO teams (slug, name, incarnation ) VALUES ( ?1 , ?2 , ?3 )",
        (slug, &team.name, &team.incarnation),
    )
}
