use crate::traits::CompRef;

use itertools::Itertools;
use log::{debug, error, trace};
use regex::Regex;

use std::{collections::HashMap, str::FromStr};

pub struct PrefetchedCompetitionLookup {
    competition_names: HashMap<String, String>,
}

impl PrefetchedCompetitionLookup {
    pub fn new(known: HashMap<String, String>) -> Self {
        Self {
            competition_names: known,
        }
    }
}

impl crate::traits::lu::Competition for PrefetchedCompetitionLookup {
    fn competition_lookup(&self, name: &str) -> CompRef {
        if let Some(exact) = self.competition_names.get(name) {
            return CompRef::One(exact.clone());
        }
        if name.contains("Comedy Central ") {
            let bare = name.replace("Comedy Central ", "");
            debug!("competition_lookup: Trying {}->{}", name, bare);
            match self.competition_lookup(bare.trim()) {
                CompRef::Zero => (),
                hit => {
                    return hit;
                }
            }
        }
        if !name.contains("BattleBots") {
            let key = "BattleBots ".to_string() + name;
            match self.competition_lookup(key.as_str()) {
                CompRef::Zero => (),
                hit => {
                    return hit;
                }
            }
            let key = "BattleBots: ".to_string() + name;
            match self.competition_lookup(key.as_str()) {
                CompRef::Zero => (),
                hit => {
                    return hit;
                }
            }
        }
        if name.contains(",") {
            let mut toks = name.split(",").map(|t| t.trim().to_owned());
            match self.competition_lookup(toks.next().unwrap().as_str()) {
                #[allow(unstable_name_collisions)]
                CompRef::One(f) => {
                    return CompRef::More(f, toks.intersperse(",".to_owned()).collect())
                }
                CompRef::More(_, _) => todo!(),
                CompRef::Zero => (),
            }
        }
        let described_number_range = Regex::new(r"(.*?)\s*([1-9]\d*)(\.?0?)\s*\-\s*([1-9]\d*)")
            .expect("regex hard-coded wrong");
        if let Some(caps) = described_number_range.captures(name) {
            let prefix = caps.get(1).map(|m| m.as_str());
            let start = caps.get(2).map(|m| m.as_str());
            let point_oh = caps.get(3).map(|m| m.as_str());
            let finish = caps.get(4).map(|m| m.as_str());
            if let (Some(p), Some(s), Some(o), Some(f)) = (prefix, start, point_oh, finish) {
                if let (Ok(s), Ok(f)) = (s.parse::<u16>(), f.parse::<u16>()) {
                    let mut presumed =
                        (s..f + 1).map(|i| p.to_owned() + " " + i.to_string().as_str() + o);
                    #[allow(unstable_name_collisions)]
                    match self.competition_lookup(presumed.next().expect("range order").as_str()) {
                        CompRef::One(h) => {
                            return CompRef::More(h, presumed.intersperse(",".to_owned()).collect())
                        }
                        CompRef::More(_, _) => todo!(),
                        CompRef::Zero => (),
                    }
                } else {
                    error!("ERROR: can't parse out season integers in '{}' : expect u16 for '{}' and '{}", &name, &s, &f);
                }
            }
        }
        let mut toks = name.split_whitespace();
        if let Some(tok0) = toks.next() {
            if u16::from_str(tok0).is_ok() && toks.clone().next().is_some() {
                let year_at_end = toks.chain(std::iter::once(tok0)).map(str::trim).join(" ");
                trace!(
                    "Lookup competition {} which begins with {} so trying it as {}",
                    name,
                    tok0,
                    year_at_end
                );
                match self.competition_lookup(year_at_end.as_str()) {
                    CompRef::Zero => (),
                    hit => return hit,
                }
            }
        }
        if name.contains("BB") {
            let s = Regex::new(r"BB\s*")
                .unwrap()
                .replace_all(name, "BattleBots ");
            match self.competition_lookup(s.to_string().trim()) {
                CompRef::Zero => (),
                hit => return hit,
            }
        }
        let s_is_for_season = Regex::new(r"S([1-9])").unwrap();
        if s_is_for_season.is_match(name) {
            self.competition_lookup(&s_is_for_season.replace_all(name, r"Season $1").to_string())
        } else {
            CompRef::Zero
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::traits::lu::Competition;

    #[tokio::test]
    async fn season_range() {
        let known = [
            "Comedy Central Season 3.0",
            "Comedy Central Season 4.0",
            "Comedy Central Season 5.0",
        ]
        .map(|n| (n.to_owned(), n.replace(" ", "_")));
        let testee = PrefetchedCompetitionLookup::new(known.into_iter().collect());
        let actual = testee.competition_lookup("Season 3.0-5.0");
        let with_space = testee.competition_lookup("Season 3.0 - 5.0");
        assert_eq!(actual, with_space);
        match actual {
            CompRef::More(here, next) => {
                assert_eq!(here, "Comedy_Central_Season_3.0");
                assert_eq!(next, "Comedy Central Season 4.0,Comedy Central Season 5.0");
            }
            CompRef::Zero => assert_eq!("Should be more", "is zero"),
            CompRef::One(n) => assert_eq!("Should be more, but is one=", n),
        }
    }
}
