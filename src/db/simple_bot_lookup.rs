use std::collections::HashSet;

pub(crate) struct SimpleBotLookup {
    known: HashSet<String>,
}

impl SimpleBotLookup {
    pub fn new(db: &super::Connection) -> Self {
        let mut known = HashSet::default();
        let mut stmt = db.prepare("SELECT slug FROM bots").unwrap();
        let mut curs = stmt.query([]).unwrap();
        while let Ok(Some(row)) = curs.next() {
            known.insert(row.get("slug").unwrap());
        }
        Self {
            known
        }
    }
}

impl crate::traits::lu::Bots for SimpleBotLookup {
    fn is_bot(&self, slug: &str) -> bool {
        self.known.contains(slug)
    }
}
