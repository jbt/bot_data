use super::refresher::Refresher;
use super::steps::Step;

use crate::mdl::PageType;
use crate::wiki;

use log::{info, warn};
use std::collections::{HashSet, VecDeque};
use time::{Duration, OffsetDateTime};

pub struct Worker {
    pub(crate) conn: super::Connection,
    wiki: wiki::Fetcher,
    todo: VecDeque<wiki::Page>,
    pub(crate) plan: Step,
    seen: HashSet<String>,
    pub(crate) tagger: super::tagger::Tagger,
}

impl Worker {
    pub async fn work(&mut self) {
        let mut refresher = Refresher::default();
        let mut outstanding = 0;
        self.enqueue(wiki::Page::updates_page());
        while outstanding >= -1 {
            if let Some(page) = self.wiki.ready_response() {
                info!("Received page {}", &page.slug);
                self.store_page(&page).expect("Failed to store");
                wiki::page::process(&page, self);
                outstanding -= 1;
            } else if let Some(t) = self.todo.pop_front() {
                info!("Requesting {}", &t.slug);
                self.wiki.fetch(t.slug, t.typ).await;
                outstanding += 1;
            } else if self.log_prog().await {
                ()
            } else if refresher.refresh(self) {
                info!("{} refresh pages requested.", refresher.requested());
            } else if self.log_prog().await {
                ()
            } else if Step::check(self).await {
                info!("{} pages outstanding", &outstanding);
            } else if self.log_prog().await {
                ()
            } else if refresher.requested() > 3 {
                outstanding -= 1;
            } else if 0 == self.tagger.tag_a_bot(&self.conn).unwrap_or(0) {
                info!("Bored. save_log_priorities.");
                self.conn.save_log_priorities();
            }
        }
        warn!("Wrapping up...");
        self.conn.save_db_snapshot().await;
        println!("\n\t   ###   DONE!   ###\n");
    }

    async fn log_prog(&mut self) -> bool {
        log::logger().enabled(&log::Metadata::builder().build())
    }
    pub(crate) fn enqueue(&mut self, page: wiki::Page) {
        if self.seen.insert(page.slug.clone()) {
            self.todo.push_back(page);
        }
    }

    pub fn store_page(&mut self, incoming: &wiki::Page) -> rusqlite::Result<usize> {
        loop {
            match self.store_page_inner(incoming) {
                Err(rusqlite::Error::SqliteFailure(rusqlite::ffi::Error { code: rusqlite::ErrorCode::DatabaseBusy, .. }, optstrmsg)) => {
                    info!("Database busy ({:?})...", &optstrmsg);
                    std::thread::sleep(std::time::Duration::from_secs(1));
                }
                x => return x,
            }
        }
    }
    pub fn store_page_inner(&mut self, incoming: &wiki::Page) -> rusqlite::Result<usize> {
        let mut stmt = self
            .conn
            .prepare("SELECT page_type, updated FROM wiki_pages WHERE slug = ?")
            .unwrap();
        let mut rows = stmt.query([incoming.slug.clone()]).expect("query problem");
        let mut typi: i8 = incoming.typ.into();
        if let Some(row) = rows.next().unwrap() {
            let pti: i8 = row.get("page_type").unwrap();
            let pt = PageType::try_from(pti);
            if incoming.typ == PageType::Unknown && pt.is_ok() {
                typi = pt.unwrap().into();
            }
            let up: OffsetDateTime = row.get("updated").unwrap();
            if !incoming.html.is_empty()
                || incoming.updated > up.checked_add(Duration::DAY).unwrap()
            {
                // println!("Updating: {}", &incoming);
                self.conn.execute(
                    "UPDATE wiki_pages SET page_type = ?1, updated = ?2, html = ?3 WHERE slug = ?4",
                    (typi, incoming.updated, &incoming.html, &incoming.slug),
                )
            } else if pt == Ok(PageType::Unknown) {
                self.conn.execute(
                    "UPDATE wiki_pages SET page_type = ?1 WHERE slug = ?2",
                    (typi, &incoming.slug),
                )
            } else {
                Ok(0) //Our data is up-to-date
            }
        } else {
            // println!("inserting typi={},in={}", typi, incoming);
            self.conn.execute(
                "INSERT INTO wiki_pages(slug,page_type,updated,html) VALUES( ?1 , ?2 , ?3 , ?4 )",
                (&incoming.slug, typi, incoming.updated, &incoming.html),
            )
        }
    }

    pub async fn create() -> Worker {
        Worker {
            conn: super::Connection::create().await,
            wiki: wiki::Fetcher::startup(),
            todo: VecDeque::new(),
            plan: Step::Begin,
            seen: HashSet::default(),
            tagger: super::tagger::Tagger::new(),
        }
    }
}
