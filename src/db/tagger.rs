use super::Connection;

use itertools::Itertools;
use log::{debug, error, info, trace, warn};
use rand::Rng;
use rusqlite::{ErrorCode, Row};

use std::{
    collections::*,
    fs::read_dir,
    iter::Iterator,
    time::{Duration, Instant},
};

#[derive(Debug)]
pub enum Error {
    DbErr(rusqlite::Error),
    DataMissing(&'static str),
    OutOfOrder(&'static str),
    IoErr(std::io::Error),
}

impl From<rusqlite::Error> for Error {
    fn from(e: rusqlite::Error) -> Self {
        Error::DbErr(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::IoErr(e)
    }
}

pub type Result = std::result::Result<(), Error>;

fn word_to_tag(word: &str) -> String {
    word.chars()
        .filter_map(|c| {
            if c.is_alphabetic() || c.is_numeric() {
                Some(c.to_uppercase().next().unwrap_or('_'))
            } else {
                None
            }
        })
        .collect()
}

#[derive(Debug)]
struct TagData {
    bots: BTreeSet<String>,
    // creat: OffsetDateTime,
}

impl Default for TagData {
    fn default() -> Self {
        Self {
            bots: BTreeSet::default(),
            // creat: OffsetDateTime::now_utc(),
        }
    }
}

#[derive(Default, Debug)]
struct BotData {
    desc: String,
    tags: BTreeSet<String>,
    wc7: bool,
}

impl BotData {
    fn new(desc: String) -> Self {
        Self {
            desc,
            tags: BTreeSet::default(),
            wc7: false,
        }
    }
    fn tag_rate(&self) -> usize {
        self.desc.len() / (self.tags.len() + 1)
    }
}

#[derive(Debug)]
pub struct Tagger {
    common_words: BTreeSet<String>,
    cand2bot: BTreeMap<String, BTreeSet<String>>,
    bot2cand: BTreeMap<String, BTreeSet<String>>,
    permatag: BTreeSet<String>,
    tags: BTreeMap<String, TagData>,
    bots: BTreeMap<String, BotData>,
    limit: usize,
    to_add: Vec<(String, String)>,
    tagged_to: String,
    complete_tags: HashSet<String>,
    weights: HashMap<String, u32>,
}

impl Tagger {
    // pub fn new(logger: Rc<RefCell<dyn MutatingLogger>>) -> Self {
    pub fn new() -> Self {
        Self {
            common_words: BTreeSet::default(),
            cand2bot: BTreeMap::default(),
            bot2cand: BTreeMap::default(),
            permatag: BTreeSet::default(),
            tags: BTreeMap::default(),
            bots: BTreeMap::default(),
            limit: 0,
            to_add: Vec::default(),
            tagged_to: String::default(),
            complete_tags: HashSet::default(),
            weights: HashMap::default(),
        }
    }

    #[allow(dead_code)]
    pub(crate) fn all_tags(&self) -> BTreeSet<String> {
        self.tags
            .iter()
            .filter_map(|(k, v)| {
                if v.bots.len() > 22 {
                    Some(k.clone())
                } else {
                    None
                }
            })
            .collect()
    }
    pub fn get_weight(&self, tag: &str) -> u32 {
        *self.weights.get(tag).unwrap_or(&1)
    }
    pub fn increase_weight(&mut self, tag: &str, db: &Connection) -> crate::error::Result<u32> {
        if self.weights.contains_key(tag) {
            let val = self.weights.get_mut(tag).unwrap();
            *val += 1;
            info!("Increased the weight of {tag} to {val}");
            db.execute("UPDATE tag_weights SET weight = ?1 WHERE tag = ?2", (*val, tag))?;
            Ok(*val)
        } else {
            self.weights.insert(tag.to_owned(), 1);
            if db.execute("INSERT INTO tag_weights(tag,weight) VALUES ( ?1 , 1 );", [tag]).is_err() {
                db.execute("UPDATE tag_weights SET weight = weight + 1 WHERE tag = ?1", &[tag])?;
            }
            Ok(1)
        }
    }

    pub(crate) fn get_tags(&self, bot: &String) -> Option<BTreeSet<String>> {
        Some(self.bots.get(bot)?.tags.clone())
    }
    pub(crate) fn get_bots(&self, tag: &String) -> Option<BTreeSet<String>> {
        Some(self.tags.get(tag)?.bots.clone())
    }
    pub(crate) fn get_tags4sure(
        &mut self,
        bot: &String,
        db: &Connection,
    ) -> Option<BTreeSet<String>> {
        if self.tag_a_bot(db).unwrap_or(1) > 0 {
            self.load_tags(db, Duration::from_millis(1)).ok();
        }
        if self.tagged_to < *bot {
            self.load_tags(db, Duration::from_secs(1)).ok();
            while let Err(e) = self.fetch_this_bots_tags(bot.clone(), db) {
                match &e {
                    Error::DbErr(rusqlite::Error::SqliteFailure(e, _)) => {
                        if e.code == ErrorCode::DatabaseBusy {
                            std::thread::sleep(std::time::Duration::from_millis(1));
                        } else {
                            error!("Low-level Error fetching tags for bot {bot}: {:?}",&e);
                            break;
                        }
                    }
                    x => {
                        error!("Error fetching tags for bot {bot}: {:?}",&x);
                        break;
                    }
                }
                self.load_tags(db, Duration::from_secs(1)).ok();
            }
        }
        self.get_tags(bot)
    }
    pub(crate) fn get_bots4sure(
        &mut self,
        tag: &String,
        db: &Connection,
    ) -> Option<BTreeSet<String>> {
        trace!("get_bots4sure({tag})");
        if self.tag_a_bot(db).unwrap_or(1) == 0 {
            return self.get_bots(tag);
        } else {
            trace!("Tags are not fully-loaded. Can't be sure all bots correpsonding to every tag are in the tag-data.");
            self.load_tags(db, Duration::from_millis(1)).ok();
        }
        if self.complete_tags.contains(tag) {
            trace!("Tag {tag} was already explicitly fetched. So it is reliable.");
        } else if self.tag_a_bot(db).unwrap_or(1) > 0 {
            debug!("Tags are not fully-loaded. Can't be sure all bots correpsonding to {tag} are in the tag-data. So fetch it explicitly.");
            let mut stmt = db
                .prepare("SELECT slug FROM tags WHERE tag = ?1 ")
                .expect("prep");
            let mut curs = stmt.query([tag]).expect("select");
            loop {
                trace!("looping");
                match curs.next() {
                    Ok(Some(row)) => {
                        // trace!("Got a row for by-tag query");
                        let bot: String = row.get("slug").expect("row slug");
                        info!("Found bot={bot} for tag={tag}");
                        if let Some(td) = self.tags.get_mut(tag) {
                            td.bots.insert(bot.clone());
                        } else {
                            let mut td = TagData::default();
                            td.bots.insert(bot.clone());
                            self.tags.insert(tag.clone(), td);
                        }
                        if let Some(bd) = self.bots.get_mut(&bot) {
                            bd.tags.insert(tag.clone());
                        } else {
                            let mut bd = BotData::default();
                            bd.tags.insert(tag.clone());
                            self.bots.insert(bot.clone(), bd);
                        }
                    }
                    Ok(None) => {
                        trace!("Done fetching {tag} from DB.");
                        self.complete_tags.insert(tag.clone());
                        break;
                    }
                    Err(e) => {
                        error!("Error fetching bots tagged with '{tag}': {:?}!", &e);
                        return self.get_bots4sure(tag, db);
                    }
                }
            }
        }
        trace!("And now get_bots({tag})");
        self.get_bots(tag)
    }
    pub fn log_stats(&self) {
        info!("log::Ger common_words: {}", self.common_words.len());
        info!("log::Ger cand2bot: {}", self.cand2bot.len());
        info!("log::Ger bot2cand: {}", self.bot2cand.len());
        info!("log::Ger permatag: {}", self.permatag.len());
        info!("log::Ger tags: {}", self.tags.len());
        info!("log::Ger bots: {}", self.bots.len());
        info!("log::Ger limit: {}", self.limit);
    }
    pub fn load_common_words(&mut self, db: &Connection) -> Result {
        let mut stmt = db.prepare("SELECT word, updated FROM words")?;
        let mut curs = stmt.query([])?;
        while let Ok(Some(r)) = curs.next() {
            let word: String = r.get("word")?;
            self.common_words.insert(word);
        }
        if self.common_words.is_empty() {
            Err(Error::DataMissing(
                "Trouble loading common words from 'words' table. Are we starting a fresh DB?",
            ))
        } else {
            Ok(())
        }
    }
    pub fn load_permatag(&mut self, db: &Connection) -> Result {
        let mut stmt = db.prepare("SELECT word FROM permatag")?;
        let curs = stmt.query_map([], |r| r.get::<usize, String>(0))?;
        self.permatag = curs.flat_map(|r| r.ok()).collect();
        if self.permatag.is_empty() {
            Err(Error::DataMissing(
                "Trouble loading common words from 'permatag' table. Are we starting a fresh DB?",
            ))
        } else {
            Ok(())
        }
    }
    pub fn load_bots(&mut self, db: &Connection) -> Result {
        let mut stmt = db.prepare("SELECT slug, desc, trivia FROM bots")?;
        let f = |r: &Row| {
            let slug: String = r.get("slug")?;
            let mut desc: String = r.get("desc")?;
            let trivia: String = r.get("trivia")?;
            desc.push(' ');
            desc.push_str(trivia.trim());
            let bd = BotData::new(desc);
            Ok((slug, bd))
        };
        self.bots = stmt.query_map([], f)?.filter_map(|r| r.ok()).collect();
        if self.bots.is_empty() {
            Err(Error::DataMissing("no bots found‽"))
        } else {
            Ok(())
        }
    }
    pub fn load_cats(&mut self, db: &Connection) -> Result {
        let mut stmt = db.prepare("SELECT bot, name, desc FROM bot_cats INNER JOIN categories ON bot_cats.cat = categories.name")?;
        let mut curs = stmt.query([])?;
        let mut tag_ins = db.prepare("INSERT INTO tags (tag,slug) VALUES ( ?1 , ?2 )")?;
        let mut hits = 0;
        while let Ok(Some(r)) = curs.next() {
            let bot: String = r.get("bot")?;
            if let Some(bd) = self.bots.get_mut(&bot) {
                if hits > 99 {
                    break;
                }
                let mut name: String = r.get("name")?;
                if 1 == tag_ins.execute((&name, &bot)).unwrap_or(0) {
                    info!("Inserted category-based tag '{name}' for {bot}'");
                    hits += 1;
                }
                if !self.permatag.contains(&name) {
                    self.permatag.insert(name.clone());
                    match db.execute("INSERT INTO permatag (word) VALUES ( ?1 )", [&name]) {
                        Ok(n) => debug!("Inserted {} categories into permatag.", &n),
                        Err(e) => error!("ERROR: Inserting category name into permatag: {:?}", &e),
                    }
                }
                name = name
                    .chars()
                    .map(|c| if c.is_alphabetic() { c } else { ' ' })
                    .collect();
                for tok in name.split_whitespace() {
                    let w = tok.trim();
                    if w.is_empty() {
                        continue;
                    }
                    bd.desc.push(' ');
                    bd.desc.push_str(w);
                }
                let desc: String = r.get("desc")?;
                if !desc.is_empty() {
                    bd.desc.push(' ');
                    bd.desc.push_str(&desc);
                }
                trace!("loading cats for {}", &bot);
            }
        }
        Ok(())
    }
    pub fn mark_wc7_bots(&mut self, db: &Connection) -> Result {
        if self.bots.is_empty() {
            return Err(Error::DataMissing(
                "loading bots is a prerequisite to marking the ones in WC7",
            ));
        }
        let mut stmt = db.prepare("SELECT bot FROM lookup WHERE competition = 'BattleBots_World_Championship_VII' OR competition = 'World Championship VII'")?;
        let mut curs = stmt.query([])?;
        while let Ok(Some(bot)) = curs.next() {
            let bot: String = bot.get(0)?;
            if let Some(bd) = self.bots.get_mut(&bot) {
                bd.wc7 = true;
            } else {
                error!(
                    "ERROR: Can't find a bot entry for an WC7 competitor: {}",
                    &bot
                );
            }
        }
        Ok(())
    }
    pub fn fetch_this_bots_tags(&mut self, bot: String, db: &Connection) -> std::result::Result<usize, Error> {
        let mut stmt = db.prepare("SELECT tag, updated FROM tags WHERE slug = ?1 ")?;
        let mut curs = stmt.query([&bot])?;
        let mut hits = 0;
        while let Ok(Some(row)) = curs.next() {
            hits += 1;
            let tag: String = row.get("tag")?;
            if let Some(td) = self.tags.get_mut(&tag) {
                td.bots.insert(bot.clone());
            } else {
                let mut td = TagData::default();
                td.bots.insert(bot.clone());
                self.tags.insert(tag.clone(), td);
            }
            if let Some(bd) = self.bots.get_mut(&bot) {
                bd.tags.insert(tag.clone());
            } else {
                let mut bd = BotData::default();
                bd.tags.insert(tag.clone());
                self.bots.insert(bot.clone(), bd);
            }
            if let Some(bots) = self.cand2bot.get(&tag) {
                for bot in bots {
                    if let Some(cands) = self.bot2cand.get_mut(bot) {
                        cands.remove(&tag);
                    }
                }
            }
            self.cand2bot.remove(&tag);
        }
        debug!("Fetched {} tags for {}", hits, &bot);
        Ok(hits + 1)
    }
    pub fn tag_a_bot(&mut self, db: &Connection) -> std::result::Result<usize, Error> {
        if self.bots.is_empty() {
            return Err(Error::OutOfOrder("Haven't loaded bots yet."));
        }
        let bot = {
            let bot = self.bots.range(self.tagged_to.clone()..).next();
            if bot.is_none() {
                return Ok(0);
            }
            bot.unwrap().0.clone()
        };
        self.tagged_to = bot.clone();
        self.tagged_to.push(' ');
        self.fetch_this_bots_tags(bot, db)
    }
    pub fn load_tags(&mut self, db: &Connection, to: Duration) -> Result {
        if self.weights.is_empty() {
            let mut stmt = db.prepare("SELECT * FROM tag_weights")?;
            let mut curs = stmt.query([])?;
            while let Ok(Some(row)) = curs.next() {
                self.weights.insert(row.get("tag")?, row.get("weight")?);
            }
            if self.weights.is_empty() {
                warn!("No tag weights found.");
                self.weights.insert("We fetched".to_owned(), 0);
            }
        }
        let end_t = Instant::now() + to;
        while end_t > Instant::now() {
            if 0 == self.tag_a_bot(db)? {
                return Ok(());
            }
        }
        info!("Currently have {} unique tags, fully handling bots up through {}.",
            self.tags.len(),
            &self.tagged_to
        );
        Err(Error::DataMissing("Need to tag more!"))
    }

    pub fn gen_tags_from_honors(&self, db: &Connection) -> Result {
        let mut sel = db.prepare("SELECT DISTINCT slug, name FROM honors ORDER BY updated DESC")?;
        let mut ins = db.prepare("INSERT INTO tags (slug,tag) VALUES ( ?1 , ?2 )")?;
        let mut curs = sel.query([])?;
        let mut hits = 0;
        while let Ok(Some(row)) = curs.next() {
            let bot: String = row.get("slug")?;
            let honor: String = row.get("name")?;
            let it = honor.split_whitespace();
            let n = it.clone().count();
            for i in 0..n {
                for j in 1..9.min(n - i + 1) {
                    let label: String =
                        "Honor: ".to_owned() + it.clone().skip(i).take(j).join(" ").trim();
                    if 1 == ins.execute((&bot, &label)).unwrap_or(0) {
                        info!("Applied honor-based tag '{}' to '{}'.", &label, &bot);
                        hits += 1;
                    }
                }
            }
            if hits > 99 {
                break;
            }
        }
        Ok(())
    }
    pub fn gen_tags_from_bot_stats(&mut self, db: &Connection) -> Result {
        let mut sel = db.prepare("SELECT slug, key, val FROM bot_stats")?;
        let mut curs = sel.query([])?;
        let mut ins_stmt = db.prepare("INSERT INTO tags (slug, tag) VALUES ( ?1 , ?2 )")?;
        while let Ok(Some(row)) = curs.next() {
            let slug: String = row.get("slug")?;
            let mut ins = |t: &String| {
                if !self.common_words.contains(t) {
                    ins_stmt.execute((&slug, t)).ok();
                }
            };
            let key: String = row.get("key")?;
            let val: String = row.get("val")?;
            let it = val.split_whitespace().filter(|tok| *tok != "...");
            let n = it.clone().count();
            let maxwin = 9.min(n);
            for win in 1..maxwin {
                for start in 0..(n - win) {
                    let mut tag = key.clone();
                    tag.push(':');
                    for tok in it.clone().skip(start).take(win) {
                        tag.push(' ');
                        tag.push_str(tok);
                    }
                    ins(&tag);
                }
            }
            let tags: Vec<String> = it
                .filter_map(|tok| {
                    let tag = word_to_tag(tok);
                    if tag.is_empty() {
                        None
                    } else {
                        Some(tag)
                    }
                })
                .collect();
            for (i, t) in tags.iter().enumerate() {
                let mut tag = t.clone();
                ins(&(key.clone() + ": " + tag.as_str()));
                if let Some(n) = tags.get(i + 1) {
                    tag.push(' ');
                    tag.push_str(&n);
                    ins(&(key.clone() + ": " + tag.as_str()));
                }
                if let Some(n) = tags.get(i + 2) {
                    tag.push(' ');
                    tag.push_str(&n);
                    ins(&(key.clone() + ": " + tag.as_str()));
                }
            }
        }
        Ok(())
    }

    pub fn gen_tags_from_teams(&self, db: &Connection) -> Result {
        let mut ins_stmt = db.prepare("INSERT INTO tags (slug, tag) VALUES ( ?1 , ?2 )")?;
        let mut stmt = db.prepare("SELECT m.member_name, m.incarnation FROM team_membership m LEFT JOIN tags t ON t.slug = m.incarnation AND t.tag = m.member_name WHERE t.tag IS NULL")?;
        let mut curs = stmt.query([])?;
        let mut hits = 0;
        while let Some(row) = curs.next()? {
            let human: String = row.get(0)?;
            let bot: String = row.get(1)?;
            info!("The human {human} is/was on the crew of {bot}.");
            ins_stmt.execute([&bot, &human])?;
            debug!("Tagged {bot} with {human} : team. hits={hits}.");
            if hits > 9 {
                break;
            }
            hits += 1;
        }
        Ok(())
    }

    pub fn retire_common_word(&mut self, db: &Connection) -> Result {
        let n = self
            .common_words
            .len()
            .checked_sub(2 * self.tags.len())
            .unwrap_or(1)
            .clamp(1, 9)
            ;
        let mut q = db.prepare("SELECT word FROM words ORDER BY updated ASC")?;
        let mut d = db.prepare("DELETE FROM words WHERE word = ?")?;
        let kill_list: Vec<String> = q
            .query_map([], |r| r.get(0))?
            .filter_map(|s| s.ok())
            .take(n)
            .collect();
        info!("Re-candidatify formerlly-common words {:?}", &kill_list);
        for w in kill_list {
            d.execute([&w])?;
        }
        Ok(())
    }
    pub fn collect_words(&mut self) {
        let mut add_cand = |bot: &str, bot_data: &BotData, cand: &String| {
            if cand.is_empty() || self.common_words.contains(cand.trim()) {
                return;
            }
            if bot_data.tags.contains(cand.trim()) {
                trace!("{} is already tagged with {}", &bot, &cand);
                return;
            }
            if self.tags.contains_key(cand.trim()) || self.permatag.contains(cand) {
                self.to_add.push((bot.to_owned(), cand.clone()));
            }
            let c2b = self
                .cand2bot
                .entry(cand.clone())
                .or_insert_with(BTreeSet::default);
            c2b.insert(bot.to_string());
            if c2b.len() > self.limit {
                self.limit = (c2b.len() + self.limit) / 2;
            }
            self.bot2cand
                .entry(bot.to_string())
                .or_insert_with(BTreeSet::default)
                .insert(cand.clone());
        };
        for (bot, bd) in &self.bots {
            let mut prev_one = String::default();
            let mut prev_two = String::default();
            for cand in bd.desc.split_whitespace() {
                if cand.trim().is_empty() {
                    continue;
                }
                let cand = word_to_tag(cand);
                if cand.trim().is_empty() {
                    continue;
                }
                if prev_two.contains(' ') {
                    prev_two.push(' ');
                    prev_two.push_str(&cand);
                    add_cand(bot, &bd, &prev_two);
                    prev_two.clear();
                }
                if !prev_one.trim().is_empty() {
                    prev_one.push(' ');
                    prev_one.push_str(cand.trim());
                    add_cand(bot, &bd, &prev_one);
                    prev_two = prev_one;
                }
                prev_one = cand.to_uppercase();
                add_cand(bot, &bd, &cand);
            }
        }
        trace!("Collected all the words!");
    }
    pub fn add_existing_tags(&mut self, db: &Connection) -> Result {
        let mut ins = db.prepare("INSERT INTO tags (tag,slug) VALUES ( ?1 , ?2 )")?;
        for (bot, tag) in &self.to_add {
            ins.execute([tag, bot]).ok();
            if let Some(td) = self.tags.get_mut(tag) {
                td.bots.insert(bot.clone());
            }
            if let Some(bd) = self.bots.get_mut(bot) {
                bd.tags.insert(tag.clone());
            }
        }
        self.to_add.clear();
        Ok(())
    }
    pub fn commonify(&mut self, db: &Connection) -> Result {
        let mut stmt = db.prepare("INSERT INTO words (word) VALUES ( ?1 )")?;
        for (cand, bs) in &self.cand2bot {
            if bs.len() + 9 > self.limit {
                trace!("considering commonifying '{cand}' @ {}", bs.len());
                if self.permatag.contains(cand.trim()) {
                    debug!(
                        "Not considering {} to be a common word, because of its permatag status.",
                        &cand
                    );
                } else if stmt.execute([cand]).unwrap_or(0) > 0 {
                    warn!("commonified '{}'", &cand);
                    self.limit += 1;
                } else {
                    debug!("Couldn't commonify '{}'", &cand);
                }
            }
        }
        Ok(())
    }
    pub fn commonify_from_regular_dictionary(&mut self, db: &Connection) -> Result {
        let mut stmt = db.prepare("INSERT INTO words (word) VALUES ( ?1 )")?;
        let mut words = BTreeSet::default();
        let mut tryit = |tok: &str| {
            if self.common_words.contains(tok) {
                return false;
            }
            if !self.cand2bot.contains_key(tok) {
                return false;
            }
            if 1 == stmt.execute([&tok]).unwrap_or(0) {
                info!("Commonified dictionary word {}", &tok);
                let tok = tok.to_string();
                self.cand2bot.remove(&tok);
                self.common_words.insert(tok);
                true
            } else {
                false
            }
        };
        let mut rng = rand::thread_rng();
        let mut skips: u8 = rng.gen();
        let dict_it = read_dir("/usr/share/dict/")?
            .filter_map(|r| r.ok())
            .filter_map(|e| e.metadata().ok().map(|m| (m, e)))
            .filter(|(m, _)| m.file_type().is_file());
        for (meta, dict) in dict_it {
            let file_size = meta.len();
            if file_size < 646540 {
                info!("Reading {:?} ({} B)", &dict, &file_size);
            } else {
                info!("Skipping large dictionary {:?} ({} B)", &dict, &file_size);
                continue;
            }
            if let Ok(contents) = std::fs::read_to_string(dict.path()) {
                warn!("Read {} bytes from {:?}", contents.len(), &dict);
                for tok in contents.split_whitespace() {
                    if !tok.chars().all(|c| c.is_alphabetic()) {
                        continue;
                    }
                    if skips > 0 {
                        skips -= 1;
                        continue;
                    }
                    skips = rng.gen();
                    trace!("Read dictionary word {} (will skip {})", &tok, &skips);
                    if words.contains(tok) {
                        continue;
                    }
                    debug!("Consider dictionary word {}", &tok);
                    if tryit(tok) {
                        return Ok(());
                    }
                    let tag = word_to_tag(tok);
                    if tryit(&tag) {
                        return Ok(());
                    }
                    words.insert(tok.to_string());
                    words.insert(tag);
                }
            }
        }
        for v in words.iter().permutations(2) {//.chain(words.iter().permutations(3)) {
            let t2 = v.iter().map(|w| w.clone()).join(" ");
            debug!("Consider multi-token combo from regular dictionary: '{}'", &t2);
            if tryit(&t2) {
                return Ok(());
            }
        }
        Ok(())
    }
    pub fn retire_tags(&self, db: &Connection) -> Result {
        let n = self
            .tags
            .len()
            .checked_sub(self.cand2bot.len())
            .unwrap_or(0);
        if n == 0 {
            return Ok(());
        }
        let mut sel = db.prepare("SELECT tag FROM tags ORDER BY updated ASC")?;
        let mut del = db.prepare("DELETE FROM tags WHERE tag = ?1")?;
        let kill_list: Vec<String> = sel
            .query_map([], |r| r.get(0))?
            .filter_map(|r| r.ok())
            .unique()
            .take(n)
            .collect();
        info!("Delete old tags {:?}", &kill_list);
        for tag in kill_list {
            del.execute([tag])?;
        }
        Ok(())
    }
    pub fn apply_new_tags(&self, db: &Connection) -> Result {
        let mut stmt = db.prepare("INSERT INTO tags (tag, slug) VALUES ( ?1 , ?2 )")?;
        let mut lo = 0;
        let mut hi = self.limit * 3 / 4;
        let mut scan_for_taggable = |it: &mut dyn Iterator<Item=(&String, &BTreeSet<String>)>,
                                     context: String| {
            let count = it
                .filter(|(c, _)| !self.common_words.contains(c.as_str()))
                .filter(|(_, bs)| {
                    if bs.len() > lo && bs.len() < hi {
                        lo += 1;
                        hi -= 1;
                        true
                    } else {
                        false
                    }
                })
                .filter(|(c, bs)| {
                    bs.iter().all(|b| {
                        trace!("Applying discovered tag '{}' to '{}'", &c, &b);
                        stmt.execute([c, b]).is_ok()
                    })
                })
                .take(99)
                .count();
            if count > 0 {
                info!("Applied {} new tags {} between {} and {} in usage.", count, context, lo, hi );
            } else {
                debug!( "Found nothing to tag between {} and {} for {}", lo, hi, context );
            }
        };
        let mut min_rate = 99;
        for (b, bd) in self
            .bots
            .iter()
            .filter(|(_, bd)| bd.wc7)
            .chain(self.bots.iter())
        {
            if bd.tag_rate() < min_rate {
                continue;
            }
            min_rate = bd.tag_rate() + 1;
            if let Some(cs) = self.bot2cand.get(b) {
                scan_for_taggable(
                    &mut cs.iter().filter_map(|t| Some((t, self.cand2bot.get(t)?))),
                    format!(
                        "related to under-tagged bot {} (rate={}, wc7={} {} candidates)",
                        &b,
                        min_rate,
                        bd.wc7,
                        cs.len()
                    ),
                );
            }
        }
        scan_for_taggable(&mut self.cand2bot.iter(), "Tags in general...".to_string());
        Ok(())
    }

    pub fn neighborhood(
        &mut self,
        of: String,
        db: &Connection,
    ) -> Option<Vec<(f64, String, String, String, String, String)>> {
        let mut bot2cnt = BTreeMap::default();
        let of_tags = &self.get_tags4sure(&of, db)?;
        info!("In neighborhood({of}), of_tags={:?}", &of_tags);
        for tag in of_tags.iter().rev() {
            let others = match self.get_bots4sure(&tag, db) {
                Some(x) => x,
                None => {
                    error!("Couldn't find ANY bots for {tag}? Shouldn't we at least have {of} in the list?");
                    continue;
                }
            };
            let weight = self.get_weight(&tag);
            trace!("for '{tag}', others.len()={} weight={weight}", others.len());
            let mut inc = 2000 - (others.len().clamp(0, 2000) as u32);
            if weight > 1 {
                info!("{tag} is weighted {weight}");
                inc *= weight;
            }
            for bot in &others {
                debug!("Accounting for {} tagged {} adding {inc}", &bot, &tag);
                *bot2cnt.entry(bot.clone()).or_insert(0) += inc;
            }
        }
        for (bot, cnt) in &mut bot2cnt {
            trace!("Iterating through bot2cnt: ({},{})", &bot, cnt);
            if let Some(tags) = &self.get_tags4sure(&bot, db) {
                for miss_tag in tags.difference(&of_tags).chain(of_tags.difference(&tags)) {
                    let n = self
                        .get_bots4sure(miss_tag, db)
                        .map(|bots| bots.len() as u32)
                        .unwrap_or(0u32)
                        * self.get_weight(&miss_tag);
                    if n / 2 >= *cnt {
                        *cnt = 1;
                    } else {
                        *cnt -= n / 2
                    }
                }
            }
        }
        let mut result: Vec<(u32, String)> =
            bot2cnt.into_iter().map(|(s, i)| (i, s.clone())).collect();
        result.sort();
        result.reverse();
        let norm: f64 = result.iter().next()?.0.try_into().ok()?;
        let mut reported_tags: HashSet<String> = HashSet::default();
        let it = result.into_iter().map(|(n, b)| {
            let mut why_tags: Vec<(usize, String)> = self
                .bots
                .get(&b)
                .map(|bd| bd.tags.clone())
                .unwrap_or_else(BTreeSet::default)
                .intersection(&of_tags)
                .filter(|t| !reported_tags.contains(*t))
                .map(|t| {
                    (
                        self.get_tags4sure(t, &db).map(|bs| bs.len()).unwrap_or(0),
                        t.clone(),
                    )
                })
                .collect();
            why_tags.sort();
            for r in why_tags.iter().take(4) {
                reported_tags.insert(r.1.clone());
            }
            (
                (n as f64 / norm),
                b,
                why_tags
                    .get(0)
                    .map(|(_, t)| t.clone())
                    .unwrap_or_else(|| String::default()),
                why_tags
                    .get(1)
                    .map(|(_, t)| t.clone())
                    .unwrap_or_else(|| String::default()),
                why_tags
                    .get(2)
                    .map(|(_, t)| t.clone())
                    .unwrap_or_else(|| String::default()),
                why_tags
                    .get(3)
                    .map(|(_, t)| t.clone())
                    .unwrap_or_else(|| String::default()),
            )
        });
        Some(it.collect())
    }
    pub fn tag_together(&mut self, left: String, rite: String, db: &Connection) -> Vec<String> {
        let left_cand = self.bot2cand.get(&left).expect("left  bot missing");
        let rite_cand = self.bot2cand.get(&rite).expect("right bot missing");
        let common: Vec<String> = left_cand
            .intersection(rite_cand)
            .map(|w| w.clone())
            .collect();
        println!("{} and {} share {:?}", &left, &rite, &common);
        let mut result = Vec::default();
        let mut stmt = db
            .prepare("INSERT INTO tags (tag, slug) VALUES ( ?1 , ?2 )")
            .expect("Hard-coded portion of insert Statement invalid.");
        for word in common {
            info!("Looking at '{word}'");
            if self.tags.contains_key(&word) {
                //|| !self.get_bots4sure(word, db).map(|v| v.is_empty()).unwrap_or(true) {
                trace!("Skipping {word} which is already a tag.");
                continue;
            }
            let bots = self.cand2bot.get(&word);
            if bots.is_none() {
                error!("Sup {word}");
                continue;
            }
            if bots.unwrap().iter().all(|b| {
                trace!("Inserting tag {word} on bot {b}");
                1 == stmt.execute([&word, b]).unwrap_or(0)
            }) {
                result.push(word.clone());
            }
            if result.len() > 9 {
                break;
            }
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::*;

    #[tokio::test]
    async fn bots_for_tag_cold() {
        let mut t = Tagger::new();
        let c = db::Connection::create().await;
        let tag = "POUND".to_owned();
        let result = t.get_bots4sure(&tag, &c);
        assert!(result.is_some());
        assert_ne!(0, result.unwrap().len());
    }
}
