/*
use std::{
    borrow::BorrowMut,
    collections::{HashMap, HashSet},
    cell::Cell,
    str::FromStr,
};
use crate::traits::CompRef;
use crate::traits::lu;
use itertools::Itertools;

pub struct PermutingCompetitionLookup<Lower: lu::Competition> {
    lower: Lower,
    known: Cell<HashMap<String, CompRef>>,
}

impl<Lower: lu::Competition> PermutingCompetitionLookup<Lower> {
    pub fn new(lower: Lower) -> Self {
        Self {
            lower,
            known: Cell::new(HashMap::default()),
        }
    }
    fn descend(&self, name: &str, tries: &mut HashSet<String>) -> CompRef {
        let mut known = self.known.take();
        self.known.set(known.clone());
        //let mut result = None;
        if let Some(cref) = known.get(name) {
            return cref.clone();
        }
        if !tries.contains(name) {
            match self.lower.competition_lookup(name) {
                CompRef::Zero => (),
                hit => {
                    known.insert(name.to_string(), hit.clone());
                    self.known.set(known);
                    return hit;
                }
            }
        }
        tries.insert(name.to_string());
        //TODO
        known.insert(name.to_string(), CompRef::Zero);
        self.known.set(known);
        CompRef::Zero
    }
}

impl<Lower: lu::Competition> lu::Competition for PermutingCompetitionLookup<Lower> {
    fn competition_lookup(&self, name: &str) -> CompRef {
        todo!()
    }
}

*/
