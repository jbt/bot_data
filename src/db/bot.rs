use super::Connection;

use crate::{
    mdl::bot::Bot,
    wiki::bot::Participation,
};

use log::{debug,info,error};
use rusqlite::ErrorCode;

pub(crate) fn store(bot: &Bot, conn: &Connection) {
    let mut stmt = "INSERT INTO bots(slug,updated,name,desc,team,trivia) VALUES (?1,?2,?3,?4,?5,?6) ON CONFLICT(slug) DO UPDATE SET updated = MAX(updated,excluded.updated)".to_string();
    if !bot.name.is_empty() {
        stmt.push_str(", name = excluded.name");
    }
    if !bot.desc.is_empty() {
        stmt.push_str(", desc = excluded.desc");
    }
    while let Err(rusqlite::Error::SqliteFailure(e, l)) = conn.execute(
        stmt.as_str(),
        (
            &bot.slug,
            bot.updated,
            &bot.name,
            &bot.desc,
            &bot.team.slug,
            bot.trivia.join(" | "),
        ),
    ) {
        error!("Error inserting into bots: {:?} ({:?}) from({stmt})",&l,&e);
        if e.code == ErrorCode::DatabaseBusy {
            std::thread::sleep(std::time::Duration::from_secs(1));
        } else {
            break;
        }
    }
    let mut stmt = conn.prepare("INSERT INTO bot_stats (slug, key, val) VALUES ( ?1 , ?2 , ?3 ) ON CONFLICT(slug,key) DO UPDATE SET val = excluded.val").expect("stat prep");
    for (k, v) in &bot.stats {
        if let Err(e) = stmt.insert([&bot.slug, k, v]) {
            println!(
                "ERROR: failed to insert bot stat: {}/{}/{} : {:?}",
                bot.slug, k, v, e
            );
        }
    }
    let mut stmt = conn
        .prepare("INSERT INTO bot_cats (bot, cat) VALUES ( ?1 , ?2 )")
        .expect("bot cat ins prep");
    for c in &bot.cats {
        use rusqlite::*;
        match stmt.execute([&bot.slug, c]) {
            Ok(_) => (),
            Err(Error::SqliteFailure(
                    ffi::Error {
                        code: ffi::ErrorCode::ConstraintViolation,
                        ..
                    },
                    _,
                )) => (),
            Err(e) => error!("ERROR: failed to insert into bot_cats: {}", e),
        }
    }
    let mut ins_hon = conn
        .prepare("INSERT INTO honors (slug, name, comp) VALUES ( ?1 , ?2 , ?3 )")
        .expect("prep ins hon");
    for honor in &bot.honors {
        ins_hon
            .insert([&bot.slug, &honor.name, &honor.competition])
            .ok();
    }
    super::team::store(conn, &bot.team).ok(); //.expect("Why does this function not return a result?");
}

pub fn record_participation(pi: &Participation, c: &Connection, slug: &String) {
    let mut insert = c
        .prepare("INSERT INTO aliases(alias, canon_slug) VALUES( ?1 , ?2 )")
        .unwrap();
    for alias in &pi.aliases {
        if insert.execute([alias, slug]).is_ok() {
            debug!("{} is/was AKA '{}'", &slug, alias);
        }
    }
    let mut insert = c
        .prepare("INSERT INTO lookup(competition, name, bot) VALUES( ?1 , ?2 , ?3 )")
        .unwrap();
    for (competition, name) in &pi.season2name {
        let off = slug.replace("_", " ");
        if &off != name {
            info!("In {competition} {off}({slug}) was known as {name}.");
        }
        insert.execute([competition, name, slug]).ok();
    }
    super::matches::save(slug, pi.matches.clone().into_iter(), c).expect("Trouble saving matches");
}
