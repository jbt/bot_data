pub(crate) mod bot;
pub(crate) mod competition;
pub(crate) mod conn;
pub(crate) mod ddl;
pub(crate) mod matches;
mod permuting_competition_lookup;
pub mod prefetched_competition_lookup;
mod refresher;
mod steps;
mod tag;
pub mod tag_logic;
pub mod tagger;
pub mod team;
mod util;
mod vamp;
mod worker;
mod simple_bot_lookup;

pub(crate) use conn::Connection;
pub use worker::Worker;

pub type Bot = mdl::bot::Bot;

use crate::mdl;
pub use competition::Competition;
