use super::{
    conn::Connection,
    tagger,
};

use crate::mdl::tag_logic::*;

use log::{debug, error, info, trace};
use rusqlite::Result;

use std::time::{
    Instant,
};
use crate::db::tagger::Tagger;

fn row2rule(r: &rusqlite::Row) -> Result<Rule> {
    let op: i8 = r.get("op")?;
    let op = Op::try_from(op).expect("invalid operation");
    trace!("in lambda op={:?}", op);
    Ok(Rule::new(
        op.into(),
        r.get("lhs")?,
        r.get("rhs")?,
        r.get("result")?,
    ))
}

pub fn apply(db: &Connection, tagger: &mut tagger::Tagger) -> Result<()> {
    let func_start = Instant::now();
    let mut stmt = db.prepare("SELECT lhs, rhs, op, result FROM tag_logic")?;
    let mut curs = stmt.query_map([], row2rule)?;
    let mut get_bots = |tag: &String| tagger.get_bots4sure(tag, db);
    let mut ins = db.prepare("INSERT INTO tags (tag, slug) VALUES ( ?1 , ?2 )")?;
    let mut hits = 0;
    while let Some(Ok(rule)) = curs.next() {
        if let Some(targets) = rule.eval(&mut get_bots) {
            if targets.len() > 0 {
                info!(
                    " {:?} should apply to {} additional bots: {:?}",
                    &rule,
                    targets.len(),
                    &targets
                );
            }
            for bot in targets {
                use rusqlite::*;
                match ins.execute((rule.out_tag(), &bot)) {
                    Ok(1) => {
                        hits += 1;
                        info!(
                            "Applied {} to {} because {:?}. hits={}",
                            rule.out_tag(),
                            &bot,
                            rule,
                            hits,
                        );
                        if hits > 9 {
                            break;
                        }
                    }
                    Ok(x) => {
                        error!("How would this insert which is attempting 1 row successfully apply to {} rows?", x);
                    }
                    Err(Error::SqliteFailure(
                            ffi::Error {
                                code: ffi::ErrorCode::ConstraintViolation,
                                ..
                            },
                            _,
                        )) => {
                        trace!(
                            "Did not insert the dynamic tag {} to {} because it was already there.",
                            rule.out_tag(),
                            &bot
                        );
                    }
                    Err(x) => {
                        error!(
                            "ERROR inserting dynamic tag {} on bot {}: {:?}",
                            rule.out_tag(),
                            &bot,
                            x
                        );
                    }
                }
            }
        } else {
            error!("ERROR: could not evaluate {:?} rule", &rule);
        }
        if hits + func_start.elapsed().as_secs() > 99 {
            info!("Tag Logic has been going on too long ({}s). Stopping after applying {} dynamic tags", func_start.elapsed().as_secs(), &hits);
            break;
        }
    }
    Ok(())
}

pub fn permify(db: &Connection) -> Result<()> {
    let mut stmt = db.prepare("SELECT DISTINCT w FROM (SELECT lhs w FROM tag_logic UNION SELECT rhs w FROM tag_logic UNION SELECT result w FROM tag_logic) LEFT JOIN permatag ON w = word WHERE word IS NULL")?;
    for w in stmt
        .query_map([], |r| Ok(r.get::<usize, String>(0)?))?
        .filter_map(|r| r.ok())
    {
        if 1 == db
            .execute("INSERT INTO permatag (word) VALUES ( ?1 )", [&w])
            .unwrap_or(0)
        {
            info!("Adding {} to permatag due to its use in tag_logic.", &w);
            return Ok(());
        }
    }
    Ok(())
}

pub(crate) fn for_pattern(db: &Connection, t: &mut Tagger, pat: &String) -> Result<usize> {
    trace!("for_pattern(db,tagger,{pat})");
    let mut hits = 0;
    let mut get_bots = |tag: &String| {
        trace!("Calling out bots tagged with {tag}");
        let results = t.get_bots4sure(tag, db);
        info!("Found for {tag} -> {:?}", &results);
        results
    };
    let mut ins = db.prepare("INSERT INTO tags (tag, slug) VALUES ( ?1 , ?2 )")?;
    for match_against in ["result", "lhs", "rhs"] {
        let q = format!("SELECT lhs, rhs, op, result FROM tag_logic WHERE {match_against} like '%{pat}%'");
        debug!("Query={q}");
        let mut stmt = db.prepare(&q)?;
        let mut curs = stmt.query_map([], row2rule)?;
        while let Some(Ok(rule)) = curs.next() {
            debug!("Considering rule {:?}", &rule);
            let tag = rule.out_tag();
            if let Some(targets) = rule.eval(&mut get_bots) {
                if targets.len() > 1 {
                    info!("Rule {:?} evals to {} targets: {:?}", &rule, targets.len(), &targets);
                }
                for target in targets {
                    match ins.execute((tag, &target)) {
                        Ok(1) => {
                            hits += 1;
                            info!("Applied {tag} to {target} because {:?} which was selected by pattern {pat}. hits={hits}", &rule);
                            if hits > 9 {
                                break;
                            }
                        }
                        Ok(x) => error!("What? {x}"),
                        Err(rusqlite::Error::SqliteFailure(
                                rusqlite::ffi::Error {
                                    code: rusqlite::ffi::ErrorCode::ConstraintViolation,
                                    ..
                                },
                                _,
                            )) => {
                            trace!( "Did not insert your dynamic tag {tag} to {target} because it was already there." );
                        }
                        Err(e) => error!("ERROR: could not insert tag {tag} for {target}: {:?}",e),
                    }
                }
            }
        }
    }
    Ok(hits)
}
