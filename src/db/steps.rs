use super::util::*;
use super::worker::Worker;

use crate::db::competition::Competition;
use crate::mdl::PageType;
use crate::wiki;

use log::{debug, error, info, trace, warn};
use num_enum::*;
use rand::prelude::SliceRandom;

use std::{sync::Once, time::Instant};

#[derive(Debug, TryFromPrimitive, PartialEq, IntoPrimitive, Copy, Clone)]
#[repr(i8)]
pub(crate) enum Step {
    Begin,
    TaggerLoadBots,
    Prune,
    Competitions,
    Bots,
    Unfetched,
    Uncategorized,
    ParseBots,
    ParseCompetitions,
    DealiasMatches,
    TaggerLoadWords,
    TaggerLoadPermatag,
    TaggerWc7,
    TaggerLoadBotStatTags,
    TaggerLoadHonorTags,
    TaggerLoadTeamTags,
    TaggerCats,
    TaggerRetireCommonWord,
    TaggerTagLogic,
    TaggerCollectWords,
    TaggerCommonifyFromRealDict,
    TaggerCommonify,
    TaggerLoadTags,
    TaggerApplyExisting,
    TaggerRetireTags,
    TaggerApplyDiscoveredTags,
    Vamps,
    PermifyTagLogic,
    TaggerTagLogicAgain,
    Done,
}

static LOG_DONE: Once = Once::new();

impl Step {
    pub(crate) async fn check<'a>(w: &mut Worker) -> bool {
        let start_time = Instant::now();
        if w.plan != Step::Done {
            debug!("Starting Step: {:?}", &w.plan);
        }
        match w.plan {
            Step::Begin => {
                warn!("Starting up.");
            }
            Step::Prune => {
                if let Err(e) = prune(w) {
                    info!("Nothing removed by prune: {:?}", &e);
                }
            }
            Step::Competitions => {
                // //w.log_str("Check list of all competitions.", 7);
                for u in wiki::Page::competitions_page() {
                    w.enqueue(u);
                }
            }
            Step::Bots => {
                // //w.log_str("Check list of all bots.", 8);
                let mut pages = wiki::Page::bots_pages();
                pages.shuffle(&mut rand::thread_rng());
                pages.truncate(3);
                for u in pages {
                    w.enqueue(u);
                }
            }
            Step::Unfetched => {
                // //w.log_str("Looking at pages we know exist but have no HTML for", 6);
                query_unfetched(w).expect("eh maybe");
            }
            Step::Uncategorized => {
                // //w.log_str("Categorizing pages", 5);
                apply_cats(w).expect("Is this a problem?");
            }
            Step::ParseBots => {
                // //w.log_str("Populating bots", 0x10);
                if plug_in_missing_bots(w).is_err() {
                    info!("No missing or out-of-date bots found.");
                }
            }
            Step::ParseCompetitions => {
                // //w.log_str("Populating competitions", 3);
                plug_in_missing_competitions(w).expect("OK?");
            }
            Step::DealiasMatches => {
                if let Err(e) = super::matches::dealias_opp(&w.conn) {
                    error!("Error dealiasing matches: {:?}",&e);
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::Vamps => {
                // //w.log_str("Hunting for vampires.", 0xFA);
                super::vamp::list(w).expect("?");
            }
            Step::TaggerLoadWords => {
                if let Err(e) = w.tagger.load_common_words(&w.conn) {
                    error!("ERROR: tag::Ger : could not load the words table: {:?}", e);
                    w.conn
                        .execute("INSERT INTO words (word) VALUES ('A')", ())
                        .ok();
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerLoadPermatag => {
                if let Err(e) = w.tagger.load_permatag(&w.conn) {
                    let msg = format!(
                        "ERROR: tag::Ger : could not load the permatag table: {:?}",
                        e
                    );
                    println!("{}", &msg);
                    // //w.log(msg, 0xFF);
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerLoadBots => {
                if let Err(e) = w.tagger.load_bots(&w.conn) {
                    let msg = format!("ERROR: tag::Ger : could not load the bots table: {:?}", e);
                    println!("{}", &msg);
                    // //w.log(msg, 0xFF);
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerWc7 => {
                if let Err(e) = w.tagger.mark_wc7_bots(&w.conn) {
                    let msg = format!("ERROR: tag::Ger : could not load the lookup table: {:?}", e);
                    println!("{}", &msg);
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerLoadBotStatTags => {
                if let Err(e) = w.tagger.gen_tags_from_bot_stats(&w.conn) {
                    error!(
                        "ERROR: tag::Ger : could not gen tags from the bot_stats table: {:?}",
                        e
                    );
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerLoadHonorTags => {
                if let Err(e) = w.tagger.gen_tags_from_honors(&w.conn) {
                    error!("ERROR: tag::Ger : could not gen tags from honors: {:?}", e);
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerLoadTeamTags => {
                if let Err(e) = w.tagger.gen_tags_from_teams(&w.conn) {
                    error!("ERROR: tag::Ger : could not gen tags from honors: {:?}", e);
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerLoadTags => {
                match w
                    .tagger
                    .load_tags(&w.conn, std::time::Duration::from_secs(9))
                {
                    Ok(()) => (),
                    Err(super::tagger::Error::DataMissing(x)) => {
                        info!("... have more tags to load!: {}", x);
                        w.plan = Step::TaggerLoadTags;
                        return true;
                    }
                    Err(e) => {
                        error!("ERROR: tag::Ger : could not load the tags table: {:?}", e);
                        w.plan = Step::Done;
                        return false;
                    }
                }
            }
            Step::TaggerTagLogic | Step::TaggerTagLogicAgain => {
                w.tagger.log_stats();
                super::tag_logic::apply(&w.conn, &mut w.tagger).expect("TODO error handling");
                w.tagger.log_stats();
            }
            Step::TaggerRetireCommonWord => {
                if let Err(e) = w.tagger.retire_common_word(&w.conn) {
                    error!(
                        "ERROR: tag::Ger : could not delete an old entry from words table: {:?}",
                        e
                    );
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerCommonifyFromRealDict => {
                if let Err(e) = w.tagger.commonify_from_regular_dictionary(&w.conn) {
                    error!(
                        "ERROR: tag::Ger : could use real dictionary words for commonality: {:?}",
                        e
                    );
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerCommonify => {
                if let Err(e) = w.tagger.commonify(&w.conn) {
                    error!(
                        "ERROR: tag::Ger : could not insert into words table: {:?}",
                        e
                    );
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerCats => {
                if let Err(e) = w.tagger.load_cats(&w.conn) {
                    error!(
                        "ERROR: tag::Ger : could not load from bot_cats/categories: {:?}",
                        e
                    );
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerCollectWords => {
                trace!("Calling collect_words");
                w.tagger.collect_words();
                trace!("Calling log_stats");
                w.tagger.log_stats();
            }
            Step::TaggerApplyExisting => {
                trace!("Calling add_existing_tags");
                if let Err(e) = w.tagger.add_existing_tags(&w.conn) {
                    error!("ERROR: tag::Ger : could not add_existing_tags: {:?}", e);
                    w.plan = Step::Done;
                    return false;
                }
                trace!("Calling log_stats");
                w.tagger.log_stats();
            }
            Step::TaggerRetireTags => {
                if let Err(e) = w.tagger.retire_tags(&w.conn) {
                    let msg = format!(
                        "ERROR: tag::Ger : could not delete old entry from tags table: {:?}",
                        e
                    );
                    println!("{}", &msg);
                    //w.log(msg, 0xFF);
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::TaggerApplyDiscoveredTags => {
                if let Err(e) = w.tagger.apply_new_tags(&w.conn) {
                    error!(
                        "ERROR: tag::Ger : could not insert into tags table: {:?}",
                        e
                    );
                    w.plan = Step::Done;
                    return false;
                }
            }
            Step::PermifyTagLogic => {
                if let Err(e) = super::tag_logic::permify(&w.conn) {
                    error!("ERROR: Could not move tags from existing tag_logic rules into permatag for DB reaons: {:?}", &e);
                }
            }
            Step::Done => {
                LOG_DONE.call_once(|| {
                    //w.log_str("\n   ###   Started up.   ### \n", 0xFF);
                });
                return false;
            }
        }
        let taken = (Instant::now() - start_time).as_millis();
        if taken > 0 {
            warn!("{taken} ms taken for step={:?}", w.plan);
        }
        w.plan = Step::try_from(i8::from(w.plan) + 1).expect("did you ... run past Done?");
        true
    }
}

fn query_unfetched(w: &mut Worker) -> rusqlite::Result<usize> {
    let mut v = vec![];
    {
        let mut stmt = w.conn.prepare(
            "SELECT slug, page_type, updated FROM wiki_pages WHERE html = '' OR html IS NULL ORDER BY updated ASC",
        )?;
        let mut curs = stmt.query([])?;
        while let Ok(Some(row)) = curs.next() {
            let page = wiki::Page {
                slug: row.get("slug")?,
                typ: row.get("page_type")?,
                html: "".to_string(),
                updated: row.get("updated")?,
            };
            info!("Will fetch HTML for {page:?}.");
            v.push(page);
        }
    }
    for p in v {
        w.enqueue(p);
    }
    Ok(42)
}

fn apply_cats(w: &Worker) -> rusqlite::Result<usize> {
    assert_eq!(i8::from(PageType::Unknown), 0);
    let mut stmt = w
        .conn
        .prepare("SELECT slug, html, updated FROM wiki_pages WHERE page_type = 0 AND html != ''")?;
    let mut curs = stmt.query([])?;
    while let Ok(Some(row)) = curs.next() {
        let slug: String = row.get("slug")?;
        let html: String = row.get("html")?;
        let typ = crate::wiki::cat::infer_type(&html);
        if typ == PageType::Unknown {
            //w.log(format!("{} is of unknown type ", slug), 0xD0);
        } else {
            //w.log(format!("Assigning type {:?} to {}", typ, slug.as_str()), 0xE);
            w.conn.execute(
                "UPDATE wiki_pages SET page_type = ?1 WHERE slug = ?2",
                (typ, slug.as_str()),
            )?;
        }
    }
    Ok(42)
}

fn plug_in_missing_bots(w: &Worker) -> rusqlite::Result<usize> {
    assert_eq!(i8::from(PageType::Bot), 3);
    trace!("plug_in_missing_bots...");
    let mut stmt = w.conn.prepare("SELECT w.slug, w.html, w.updated FROM wiki_pages w LEFT JOIN bots b ON w.slug = b.slug WHERE page_type = 3 AND b.slug IS NULL")?;
    let mut rows = stmt.query([])?;
    let mut hits = 0;
    while let Ok(Some(row)) = rows.next() {
        trace!("plug in missing bot for {:?}", row.get::<usize, String>(0));
        let pg = row2page(row)?;
        if pg.html.len() > 0 {
            info!(
                "A bot page {} has html {}, but no corresponding bot. Parse it!",
                &pg.slug,
                pg.html.len()
            );
            if let Ok((bot, pi)) = wiki::bot::parse(&pg, &w.conn) {
                info!("Did a bot parse, now store : {}", &bot.slug);
                super::bot::store(&bot, &w.conn);
                info!("Stored the main bot, now for participation info: {:?}", &pi);
                super::bot::record_participation(&pi, &w.conn, &bot.slug);
                info!("Parsed out {} as a bot and stored it.", &pg);
                hits += 1;
                if hits > 9 {
                    break;
                }
            }
        }
    }
    Ok(hits)
}

fn plug_in_missing_competitions(w: &Worker) -> rusqlite::Result<usize> {
    assert_eq!(i8::from(PageType::Competition), 1);
    let mut stmt = w.conn.prepare("SELECT w.slug, w.html, w.updated FROM wiki_pages w LEFT JOIN competitions c ON w.slug = c.slug WHERE page_type = 1 AND c.slug IS NULL")?;
    let mut rows = stmt.query([])?;
    while let Ok(Some(row)) = rows.next() {
        let p = row2page(row)?;
        if !p.html.is_empty() {
            Competition::from_page(&p).store(&w.conn);
            //w.log(format!("Parsed competition {}", p.slug), 0x30);
        }
    }
    Ok(42)
}

fn prune(w: &Worker) -> rusqlite::Result<usize> {
    let mut pruned = 0;
    for x in ["matches", "bots", "competitions", "bot_stats", "honors"] {
        trace!("pruning {x}");
        let mut select = "SELECT x.slug FROM ".to_string();
        select.push_str(x);
        select.push_str(" x LEFT JOIN wiki_pages w  ON w.slug = x.slug WHERE w.slug IS NULL OR x.updated < w.updated");
        let mut stmt = w.conn.prepare(select.as_str())?;
        let mut rows = stmt.query([])?;
        let mut delstr = "DELETE FROM ".to_string();
        delstr.push_str(x);
        delstr.push_str(" WHERE slug = ?");
        let mut del = w.conn.prepare(delstr.as_str()).expect(delstr.trim());
        while let Ok(Some(row)) = rows.next() {
            let slug: String = row.get("slug")?;
            info!("Culling from {}: {} based on {}", x, slug, select);
            del.execute([&slug]).expect(delstr.as_str());
            pruned += 1;
            if pruned > 3 {
                break;
            }
        }
    }
    Ok(pruned)
}
