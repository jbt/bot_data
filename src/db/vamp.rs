//Vampires have no reflection

use super::util::row2page;

use crate::wiki;

use log::{debug, error, info};
use time::{Duration, OffsetDateTime};

pub(crate) fn list(db: &super::Worker) -> rusqlite::Result<usize> {
    let mut result = 0;
    let mut stmt = db.conn.prepare("SELECT DISTINCT b.* FROM matches a INNER JOIN wiki_pages b ON a.opp = b.slug WHERE b.html != '' AND NOT EXISTS (SELECT 1 FROM matches WHERE slug = a.opp)")?;
    for missing in stmt.query_map([], row2page)?.filter_map(|p| p.ok()) {
        if missing.html.is_empty() {
            continue;
        }
        if missing.updated - Duration::hours(48) > OffsetDateTime::now_utc() {
            continue;
        }
        debug!("Reparse {:?} due to apparent vampirism.", &missing);
        let (_bot, part) = wiki::bot::parse(&missing, &db.conn).unwrap();
        result += part.matches.len();
        if result > 9 {
            break;
        } else if part.matches.is_empty() {
            error!(
                "Seem to be having a hard time parsing out the matches of {}",
                &missing.slug
            );
        } else {
            info!(
                "{} now has {} matches in the database after reparse.",
                &missing.slug,
                part.matches.len()
            );
        }
    }
    Ok(result)
}
