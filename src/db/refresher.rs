use super::util::*;
use super::Connection;
use super::Worker;

use crate::consts;
use crate::wiki::Page;

use std::ops::Sub;
use std::time::{SystemTime, UNIX_EPOCH};

use log::{info, warn};
use time::{Duration, OffsetDateTime};

pub(crate) struct Refresher {
    last_fetch: SystemTime,
    hit_list: Vec<Page>,
    index: usize,
    rm_thresh: OffsetDateTime,
    count: usize,
}

impl Refresher {
    fn next(&self) -> &Page {
        return self.hit_list.get(self.index).unwrap();
    }
    pub(crate) fn refresh(&mut self, worker: &mut Worker) -> bool {
        if self.hit_list.is_empty() {
            self.select(&worker.conn).is_ok()
        } else if self.index >= self.hit_list.len() {
            false
        } else if self.should_delete() {
            warn!(
                "\n   ###   Dropping old page (may have been removed from wiki?): {}   ### \n",
                self.next().slug
            );
            let result = worker
                .conn
                .execute("DELETE FROM wiki_pages WHERE slug = ?", [&self.next().slug])
                .is_ok();
            self.index += 1;
            self.rm_thresh = self.next().updated.sub(Duration::DAY);
            result
        } else if let Ok(since) = SystemTime::now().duration_since(self.last_fetch) {
            if since > Duration::seconds(consts::REFRESH_SECONDS) {
                info!("Refreshing old: {}", &self.hit_list[self.index]);
                worker.enqueue(self.hit_list[self.index].clone());
                self.index += 1;
                self.count += 1;
                self.last_fetch = SystemTime::now();
                true
            } else {
                false
            }
        } else {
            false
        }
    }
    fn select(&mut self, conn: &Connection) -> rusqlite::Result<()> {
        let mut stmt =
            conn.prepare("SELECT * FROM wiki_pages WHERE updated ORDER BY updated ASC")?;
        let rows = stmt.query_map([], row2page)?;
        self.hit_list = rows.filter_map(Result::ok).collect();
        if self.hit_list.is_empty() {
            Err(rusqlite::Error::InvalidQuery)
        } else {
            Ok(())
        }
    }
    fn should_delete(&self) -> bool {
        return self.next().updated < self.rm_thresh;
    }
    pub fn requested(&self) -> usize {
        self.count
    }
}

impl Default for Refresher {
    fn default() -> Self {
        Self {
            last_fetch: UNIX_EPOCH,
            hit_list: Vec::default(),
            index: 0,
            rm_thresh: OffsetDateTime::now_utc().sub(Duration::HOUR * consts::MAX_AGE_HOURS),
            count: 0,
        }
    }
}
