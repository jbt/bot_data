use crate::{
    db,
    error::GenErr,
    mdl::{
        Match,
        Page,
        PageType,
    },
};

use log::{error, info, trace};

use std::collections::{BTreeSet, HashMap};


pub fn save<It: Iterator<Item=Match>>(
    from_slug: &str,
    matches: It,
    conn: &db::Connection,
) -> rusqlite::Result<usize> {
    let mut ins = conn.prepare("INSERT INTO matches(slug, opp, competition, segment, winner, note) VALUES ( ?1 , ?2 , ?3 , ?4 , ?5 , ?6 )")?;
    let mut count = 0;
    for m in matches {
        let mut opp = m
            .bots
            .iter()
            .filter_map(|s| {
                if s.contains(from_slug) {
                    None
                } else {
                    Some(s.to_string())
                }
            })
            .collect::<Vec<String>>();
        opp.sort();
        let opp = opp.join(" ");
        use rusqlite::*;
        match ins.execute((
            from_slug,
            &opp,
            m.competition.trim(),
            m.segment.trim(),
            m.winner.trim(),
            m.note.trim(),
        )) {
            Ok(n) => {
                info!(
                    "Saved {} match {:?} from {} against {}",
                    n, &m, &from_slug, &opp
                );
                count += n;
            }
            Err(Error::SqliteFailure(
                    ffi::Error {
                        code: ffi::ErrorCode::ConstraintViolation,
                        ..
                    },
                    _,
                )) => trace!("ConstraintViolation inserting {:?}", &m),
            Err(e) => error!("ERROR: Couldn't insert match {:?} because: {:?}", &m, e),
        }
    }
    Ok(count)
}

fn from_row(row: &rusqlite::Row) -> rusqlite::Result<Match> {
    let bots = vec![row.get("slug")?, row.get("opp")?];
    Ok(Match {
        competition: row.get("competition")?,
        segment: row.get("segment")?,
        bots,
        winner: row.get("winner")?,
        note: row.get("note")?,
        sequence: 0,
    })
}

pub type MatchLookup = HashMap<String, BTreeSet<Match>>;

pub fn get_1v1_wins(conn: &db::Connection) -> rusqlite::Result<MatchLookup> {
    let mut q = conn.prepare("SELECT * FROM matches WHERE slug = winner AND opp not like '% %'")?;
    let it = q
        .query_map([], from_row)?
        .flat_map(|m| m.ok().map(|m| (m.winner.clone(), m)));
    let mut result = HashMap::default();
    for (w, o) in it {
        result.entry(w).or_insert_with(BTreeSet::default).insert(o);
    }
    Ok(result)
}

#[allow(dead_code)]
pub enum Outcome {
    Win,
    Loss,
    Other,
}

#[derive(Clone)]
pub struct BotMatches {
    pub wins: MatchLookup,
    pub loss: MatchLookup,
}

impl BotMatches {
    #[allow(dead_code)]
    pub fn for_outcome(&self, o: Outcome) -> &MatchLookup {
        match o {
            Outcome::Win => &self.wins,
            Outcome::Loss => &self.loss,
            Outcome::Other => todo!(),
        }
    }
}

fn get_matches(bot: &str, db: &db::Connection) -> Result<BotMatches, GenErr> {
    let mut q = db.prepare("SELECT * FROM matches WHERE slug like ?1")?;
    let mut wins = MatchLookup::default();
    let mut loss = MatchLookup::default();
    for m in q.query_map([bot], from_row)?.filter_map(|m| m.ok()) {
        if m.winner == bot {
            if let Some(loser) = m.loser() {
                wins.entry(loser.clone()).or_default().insert(m);
            }
        } else {
            loss.entry(m.winner.clone()).or_default().insert(m);
        }
    }
    if wins.len() + loss.len() > 0 {
        return Ok(BotMatches { wins, loss });
    }
    let mut q = db.prepare("SELECT html FROM wiki_pages WHERE slug = ?1")?;
    let html: String = q.query_row([bot], |r| r.get(0))?;
    if html.is_empty() {
        return Err("We don't have matches or HTML for {bot}.".into());
    }
    let pg = Page::unfetched(bot.to_owned(), PageType::Bot).with_fetched_text(html);
    let (b, pi) = crate::wiki::bot::parse(&pg, &db).unwrap();
    db::bot::store(&b, db);
    db::bot::record_participation(&pi, db, &b.slug);
    for m in pi.matches {
        if m.winner == bot {
            let opp = m.loser().ok_or::<GenErr>("Match with no opponent?".into())?.clone();
            wins.entry(opp).or_default().insert(m);
        } else {
            loss.entry(m.winner.clone()).or_default().insert(m);
        }
    }
    Ok(BotMatches { wins, loss })
}

#[derive(Default)]
pub struct CachedLookup {
    cache: HashMap<String, BotMatches>,
}

impl CachedLookup {
    pub fn lookup(&mut self, bot: &String, db: &db::Connection) -> Option<BotMatches> {
        if let Some(x) = self.cache.get(bot) {
            return Some(x.clone());
        }
        match get_matches(bot, db) {
            Ok(data) => {
                self.cache.insert(bot.clone(), data);
                self.cache.get(bot).map(|x| x.clone())
            }
            Err(e) => {
                error!("Error fetching matches for {bot}: {:?}",e);
                None
            }
        }
    }
}

pub fn dealias_opp(db: &db::Connection) -> Result<usize, GenErr> {
    let mut q = db.prepare("SELECT m.opp as opp, m.competition as comp, l.bot as bot FROM (matches m INNER JOIN lookup l ON m.competition = l.competition AND m.opp = l.name ) WHERE l.name != l.bot")?;
    let mut c = q.query([])?;
    let mut upd = db.prepare("UPDATE matches SET opp = ?1 WHERE opp = ?2 AND competition = ?3")?;
    let mut hits = 0;
    while let Ok(Some(row)) = c.next() {
        let opp: String = row.get("opp")?;
        let comp = row.get("comp")?;
        let bot = row.get("bot")?;
        hits += upd.execute([&bot, &opp, &comp])?;
        info!("Repointed matches against {opp} in {comp} to be against {bot} due to lookup of real slug.");
    }
    let mut q = db.prepare("SELECT DISTINCT m.opp as opp, m.competition as comp, a.canon_slug AS bot FROM matches m inner join aliases a on a.alias = m.opp left join wiki_pages w on a.canon_slug = w.slug where a.canon_slug != a.alias and not exists (select 1 from wiki_pages where slug = m.opp)")?;
    let mut c = q.query([])?;
    while let Ok(Some(row)) = c.next() {
        let opp: String = row.get("opp")?;
        let comp = row.get("comp")?;
        let bot = row.get("bot")?;
        hits += upd.execute([&bot, &opp, &comp])?;
        info!("Repointed matches against {opp} in {comp} to be against {bot} due to existence of an alias hit.");
        if hits > 9 {
            break;
        }
    }
    Ok(hits)
}
