use std::io::BufRead;
use std::{cmp::Ordering, collections::*, fs, io, time::SystemTime};

#[derive(Clone, Copy)]
enum Direction {
    Bot2Tag,
    Tag2Bot,
}

fn get_path(name: &String, d: Direction) -> String {
    let dn = match d {
        Direction::Bot2Tag => "bot2tag",
        Direction::Tag2Bot => "tag2bot",
    };
    format!("data/{dn}/{name}.txt")
}

#[derive(Debug, PartialEq, Eq)]
struct Age {
    modtime: SystemTime,
    name: String,
}

impl Age {
    fn new(name: String, d: Direction) -> Self {
        let path = get_path(&name, d);
        let md = fs::metadata(path).ok();
        let modtime = md.and_then(|md| md.modified().ok());
        let modtime = modtime.unwrap_or_else(SystemTime::now);
        Self { name, modtime }
    }
}

impl PartialOrd for Age {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.modtime.cmp(&other.modtime).reverse())
    }
}

impl Ord for Age {
    fn cmp(&self, other: &Self) -> Ordering {
        self.modtime.cmp(&other.modtime).reverse()
    }
}

#[allow(dead_code)]
#[derive(Default)]
struct Tags {
    bot2tag: HashMap<String, BTreeSet<String>>,
    tag2bot: HashMap<String, BTreeSet<String>>,
    by_age: BinaryHeap<Age>,
    dirty: Vec<String>,
}

#[allow(dead_code)]
impl Tags {
    pub fn add(&mut self, bot: String, tag: String) {
        if !self.bot2tag.contains_key(&bot) {
            self.load_one(&bot, Direction::Bot2Tag);
        }
        if !self.tag2bot.contains_key(&tag) {
            self.load_one(&tag, Direction::Tag2Bot);
        }
        let bots = self.tag2bot.get_mut(&tag).unwrap();
        let tags = self.bot2tag.get_mut(&bot).unwrap();
        tags.insert(tag);
        bots.insert(bot);
    }

    fn load_one(&mut self, name: &String, d: Direction) {
        //-> &mut BTreeSet<String> {
        let p = get_path(name, d);
        let col = self.collection(d);
        col.insert(
            name.clone(),
            fs::File::open(p)
                .map(|f| {
                    io::BufReader::new(f)
                        .lines()
                        .filter_map(|r| r.ok())
                        .collect()
                })
                .unwrap_or_else(|_| BTreeSet::default()),
        );
        {
            self.by_age.push(Age::new(name.clone(), d.clone()));
        }
        // col.get_mut(name).unwrap()
    }
    fn collection(&mut self, d: Direction) -> &mut HashMap<String, BTreeSet<String>> {
        match d {
            Direction::Tag2Bot => &mut self.tag2bot,
            Direction::Bot2Tag => &mut self.bot2tag,
        }
    }
    fn hygiene(&mut self) {}
}
