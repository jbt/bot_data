use super::prefetched_competition_lookup::PrefetchedCompetitionLookup;

// use crate::ipfs;

use log::{debug, trace};
use rusqlite::Result;
use time::Instant;

use std::{
    boxed::Box,
    cell::RefCell,
    collections::*,
    mem,
    path::Path,
    // rc::Rc,
};

const PATH: &str = "bots.db";

#[allow(dead_code)]
const CID_PATH: &str = "data.cid";

pub struct Connection {
    inner: Box<Option<rusqlite::Connection>>,
    dirty: RefCell<Option<Instant>>,
}

impl Connection {
    //TODO raise abstraction
    pub fn prepare(&self, stmt: &str) -> Result<rusqlite::Statement> {
        trace!("db.prepare({})", &stmt);
        match &self.inner.as_ref() {
            Some(db) => db.prepare(stmt),
            None => panic!(),
        }
    }
    pub fn execute<B: rusqlite::Params + std::fmt::Debug>(
        &self,
        stmt: &str,
        binds: B,
    ) -> Result<usize> {
        debug!("db.execute({}, {:?})", &stmt, &binds);
        let result = match self.inner.as_ref() {
            Some(db) => {
                let result = db.execute(stmt, binds);
                db.flush_prepared_statement_cache();
                result
            }
            None => panic!(),
        };
        if let Ok(count) = result {
            let mut dirt = self.dirty.borrow_mut();
            if count > 0 {
                *dirt = Some(Instant::now())
            }
        }
        result
    }

    pub async fn create() -> Connection {
        let path = Path::new(PATH);
        if !path.is_file() {
            let cid = Path::new("data.cid");
            if cid.is_file() {
                crate::ipfs::read::down(
                    std::fs::read_to_string(cid)
                        .expect("failed to read data.cid")
                        .as_str(),
                    path,
                )
                    .await;
            } else {
                eprintln!("WARNING: neither bots.db nor data.cid are present. Don't want to start the database over FROM SCRATCH");
                std::process::exit(9);
            }
        }
        let db = rusqlite::Connection::open(PATH).expect("Was unable to open database.");
        for s in super::ddl::INIT {
            if db.execute(s, []).is_ok() {
                eprintln!("Executed {} while preparing DB.", s);
            }
        }
        //eprintln!("Got a DB connection!");
        {
            let mut stmt = db.prepare("SELECT file, line, ABS(prio) as prio FROM log_prio").expect("select log_prio");
            let curs = stmt
                .query_map([], |r| {
                    let val = ((r.get("file")?, r.get("line")?), r.get("prio")?);
                    Ok(val)
                })
                .expect("valid query")
                .filter_map(|o| match o {
                    Ok(g) => Some(g),
                    Err(e) => {
                        eprintln!("Error: {:?}", &e);
                        None
                    }
                });
            let prios: HashMap<(String, u32), i64> = curs.collect();
            //eprintln!("Loaded {} prios", prios.len());
            crate::prio_log::set_priorities(prios);
        }
        let created = Self {
            inner: Some(db).into(),
            dirty: RefCell::new(Some(Instant::now())),
        };
        created.load_log_priorities();
        created
    }
    pub(crate) fn load_log_priorities(&self) {
        // eprintln!("load_log_priorities");
        let mut stmt = match &self.inner.as_ref() {
            Some(db) => db.prepare("SELECT file, line, ABS(prio) as prio FROM log_prio").expect("select log_prio"),
            _ => panic!(),
        };
        let curs = stmt
            .query_map([], |r| {
                let val = ((r.get("file")?, r.get("line")?), r.get("prio")?);
                // println!("Loaded log statement priority: {:?}", &val);
                Ok(val)
            })
            .expect("valid query")
            .filter_map(|o| o.ok());
        crate::prio_log::set_priorities(curs.collect());
    }
    pub(crate) fn save_log_priorities(&self) {
        let (mut ins, mut neg) = match &self.inner.as_ref() {
            Some(db) => (
                db.prepare("INSERT INTO log_prio ( file, line, prio ) VALUES ( ?1 , ?2 , ?3 ) ON CONFLICT (file,line) DO UPDATE SET prio = excluded.prio").unwrap(),
                db.prepare("SELECT file, line, ABS(prio) as prio FROM log_prio WHERE prio < 0").expect("select updated log_prio"),
            ),
            _ => {
                return;
            }
        };
        let mut prios = crate::prio_log::get_priorities();
        let mut neg_curs = neg.query([]).expect("Query neg");
        while let Ok(Some(row)) = neg_curs.next() {
            let file: String = row.get("file").expect("file");
            let line: u32 = row.get("line").expect("line");
            let prio: i64 = row.get("prio").expect("prio");
            if let Some(pref) = prios.get_mut(&(file, line)) {
                *pref = prio;
            }
        }
        for ((f, l), p) in &mut prios {
            *p += 1;
            ins.execute((f, l, *p)).ok();
        }
        crate::prio_log::set_priorities(prios);
    }

    pub(crate) async fn save_db_snapshot(&mut self) -> bool {
        self.save_log_priorities();
        let result = match self.inner.as_ref() {
            Some(db) => db.execute("DELETE FROM log_prio WHERE prio > 4100", []),
            None => panic!(),
        };
        match result {
            Ok(n) => eprintln!(
                "Removed logging meta records for {} unused log statements.",
                n
            ),
            Err(e) => eprintln!(
                "ERROR: unable to retire unused log statement meta records: {:?}",
                e
            ),
        }
        eprintln!("Now to close the DB...");
        {
            mem::take(&mut self.inner)
                .expect("Someone is still using the DB?!")
                .close()
                .expect("Failed to close the database?!");
        }
        if !self.is_dirty() {
            println!("Not dirty");
            return false;
        }
        *self.dirty.borrow_mut() = None;
        /*
        println!("Now, to read the database as just a bunch of bytes...");
        if let Ok(data) = std::fs::read(PATH) {
            println!("Now attempting to upload the DB to IFPS...");
            match ipfs::write::add(data, false).await {
                Ok(hash) => {
                    if std::fs::write(CID_PATH, hash.clone()).is_ok() {
                        println!("Database state uploaded: {}", hash);
                        // return self.inner.execute("INSERT INTO history(cid) VALUES( ?1 )", [&hash]).is_ok();
                        return true;
                    } else {
                        println!("Failed to write data.cid")
                    }
                }
                Err(err) => println!("Failed to add&pin the db state: {:?}", err)
            }
        } else {
            println!("Failed to read database file.");
        }

         */
        false
    }
    pub(crate) fn is_dirty(&self) -> bool {
        self.dirty.borrow().is_some()
    }
    pub fn competitions(&self) -> Option<PrefetchedCompetitionLookup> {
        match super::Competition::names(self) {
            Ok(a) => Some(PrefetchedCompetitionLookup::new(a)),
            Err(e) => {
                println!("Failed to get competition names: {}", &e);
                None
            }
        }
    }
    pub(crate) fn bot_lookup(&self) -> super::simple_bot_lookup::SimpleBotLookup {
        super::simple_bot_lookup::SimpleBotLookup::new(self)
    }
}
