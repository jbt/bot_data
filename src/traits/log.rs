pub trait Logger {
    fn log(&self, line: String, priority: u8);
    fn log_str(&self, line: &str, priority: u8);
}

pub trait MutatingLogger {
    fn log(&mut self, line: String, priority: u8);
    fn log_str(&mut self, line: &str, priority: u8);
}

impl<T: Logger> MutatingLogger for T {
    fn log(&mut self, line: String, priority: u8) {
        Logger::log(self, line, priority)
    }

    fn log_str(&mut self, line: &str, priority: u8) {
        Logger::log_str(self, line, priority)
    }
}

