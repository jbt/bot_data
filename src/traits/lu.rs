pub trait Competition {
    fn competition_lookup(&self, name: &str) -> CompRef;
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, Clone)]
pub enum CompRef {
    Zero,
    One(String),
    More(String, String),
}

pub trait Bots {
    fn is_bot(&self, slug: &str) -> bool;
}
