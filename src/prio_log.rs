use log::*;
use priority_queue::PriorityQueue;
use std::cell::*;
use std::collections::*;
use std::time::*;

pub static LOGGER: PrioLog = PrioLog {};

type Prio = i64;
type SrcLoc = (String, u32);
type State = HashMap<SrcLoc, i64>;

fn start_from_level(lev: Level) -> Prio {
    match lev {
        Level::Error => 2048,
        Level::Warn => 1024,
        Level::Info => 512,
        Level::Debug => 256,
        Level::Trace => 128,
    }
}

thread_local! {
    static INTERNAL: RefCell<Internal> = RefCell::new(Internal::default());
}

pub struct PrioLog {}

impl log::Log for PrioLog {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        self.check()
    }
    fn log(&self, record: &Record) {
        INTERNAL.with_borrow_mut(|int| int.add_log_line(record.clone()));
    }
    fn flush(&self) {
        ()
    }
}

impl PrioLog {
    pub fn check(&self) -> bool {
        INTERNAL.with_borrow_mut(|int| int.check())
    }
    pub fn speed_up(&self) {
        INTERNAL.with_borrow_mut(|int| int.speed_up())
    }
}

pub fn get_priorities() -> State {
    INTERNAL.with_borrow(|int| int.srcloc2prio.clone())
}

pub fn set_priorities(state: State) {
    INTERNAL.with_borrow_mut(|int| int.srcloc2prio = state);
}

struct Internal {
    srcloc2prio: State,
    q: PriorityQueue<(String, SrcLoc, i64), i64>,
    last: Instant,
    slow_ms: u64,
}

impl Default for Internal {
    fn default() -> Self {
        Self {
            srcloc2prio: HashMap::default(),
            q: PriorityQueue::default(),
            last: Instant::now(),
            slow_ms: 4096,
        }
    }
}

fn decrement(record: &Record) -> i64 {
    let dec = match record.level() {
        Level::Error => 1,
        Level::Warn => 2,
        Level::Info => 3,
        Level::Debug => 4,
        Level::Trace => 5,
    };
    if record
        .module_path()
        .map(|m| m.contains("bot_data"))
        .unwrap_or(false)
    {
        dec
    } else {
        dec + 9
    }
}

impl Internal {
    fn add_log_line(&mut self, record: Record) {
        let t = chrono::Local::now();
        let mut file = record.file().unwrap_or("");
        if file.starts_with("src/") {
            file = &file[4..];
        }
        let line = record.line().unwrap_or(0);
        let srcloc = (file.to_string(), line);
        let prio = if let Some(prio) = self.srcloc2prio.get(&srcloc) {
            prio.clone()
        } else {
            let prio = start_from_level(record.level());
            self.srcloc2prio.insert(srcloc.clone(), prio);
            prio
        };
        if prio + 9 >= self.q.len().try_into().unwrap_or(0) {
            let mut m = format!(
                "{} {:55} {:#04X} {}:{}",
                t.format("%H:%M:%S"),
                record.args(),
                prio,
                file,
                line
            );
            if let Some((max_idx, _)) = m.char_indices().nth(256) {
                m.truncate(max_idx);
                let ending = format!("... {:#04X} {}:{}", prio, file, line);
                m.push_str(ending.trim());
            }
            self.q.push((m, srcloc, decrement(&record)), prio);
        } else {
            self.check();
        }
    }
    fn check(&mut self) -> bool {
        if let Some((_, p)) = self.q.peek() {
            let next = self.last
                + Duration::from_millis(
                self.slow_ms - p.clone().clamp(0, self.slow_ms as i64) as u64,
            );
            let now = Instant::now();
            if next > now {
                // if *p > 9 {
                //     std::thread::sleep(Duration::from_millis(1));
                // }
                return *p > 1234;
            }
            self.last = now;
        } else {
            return false;
        }
        if let Some(((m, loc, dec), p)) = self.q.pop() {
            if let Some(sav) = self.srcloc2prio.get_mut(&loc) {
                if p - *sav <= dec * 9 {
                    eprintln!("{}", &m);
                    if *sav <= dec {
                        *sav = 0;
                    } else {
                        *sav -= dec;
                    }
                } else {
                    self.q.push((m, loc, dec), *sav);
                }
            }
            p > 99
        } else {
            false
        }
    }
    fn speed_up(&mut self) {
        if self.slow_ms > 1 {
            self.slow_ms -= 1;
        }
    }
}
